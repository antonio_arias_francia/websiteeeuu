﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="PreRegistroSocio.aspx.cs" ClientIDMode="Static" Inherits="SantaNaturaNetworkV3.PreRegistroSocio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/Banner de Store Template/animate.css">
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />
    <link href="css/estilosDetalleCompra.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/proyecto2/eskju.jquery.scrollflow.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.7/dist/sweetalert2.all.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
        .anchoBotonGuardar {
            width: 40% !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdn_Idioma" runat="server" />    
    <input type="hidden" class="form-control" id="hdn_Idioma_online" /> 

    <asp:ScriptManager runat="server">
        <Scripts>
            <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
            <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
            <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
            <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
            <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
            <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
            <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
            <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />

        </Scripts>
    </asp:ScriptManager>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <div id="ContenidoFluido" class="container-fluid" style="width: 92%; margin-top: 120px">
        <div class="row justify-content-md-center">
            <!--REGISTRO AFILIACION-->
            <div id="MostrarRegistroCliente" class="col-md-8">
                <div class="row form-group colorlib-form">
                    <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto">
                        <h1 id="Registro_h1_1" runat="server">REGISTRATION</h1>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label id="Registro_label_1" runat="server" style="font-weight: bold; font-size: 16px">ACCOUNT DATA</label>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label id="Registro_label_2" class="label" runat="server" style="font-weight: bold">User</label>
                                <asp:TextBox ID="txtUl" title="Se necesita un nombre de Usuario" MaxLength="8" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label id="Registro_label_3" class="label" runat="server" style="font-weight: bold">Password</label>
                                <asp:TextBox ID="TxtCl" runat="server" CssClass="form-control text-uppercase marginTop" MaxLength="12"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity" id="btnCargaImagen" runat="server">
                                <label id="Registro_label_4" class="label" runat="server" style="font-weight: bold">Profile picture</label>
                                <label id="Registro_label_5" class="file-upload btn btn-success form-control marginTop" style="font-size: 15px">
                                     <%=change_foto %> 
                                    <asp:FileUpload CssClass="form-control imagen" ID="fileUpload" runat="server" />
                                </label>
                            </div>
                        </div>

                        <div class="row col-md-12" id="imagenMostrada" runat="server">
                            <div class="form-group col-md-4 scrollflow -opacity" style="margin-left: auto; margin-right: auto">
                                <label id="Registro_label_6" runat="server">My profile picture</label>
                                <div id="imagePreview" class="center-block align-content-center">
                                    <img src="img/usuario1.png" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 13px">
                        <label id="Registro_label_7" runat="server" style="font-weight: bold; font-size: 16px">Personal information</label>

                        <div class="row col-md-12">
                              <div class="form-group col-md-3 scrollflow -opacity">
                                <label id="Registro_label_8" runat="server" class="label" style="font-weight: bold">Country (*)</label>
                                <asp:DropDownList ID="cboPais" runat="server" CssClass="form-control marginTop" Height="33px" />
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label id="Registro_label_9" runat="server" class="label" style="font-weight: bold">UpLine (*)</label>
                                <asp:DropDownList ID="CboUpLine" runat="server" CssClass="form-control btn-lg marginTop" Height="33px" />
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" id="Registro_label_10" runat="server" style="font-weight: bold">Sponsor</label>
                                <asp:TextBox ID="txtPatrocinador" runat="server" CssClass="form-control text-uppercase marginTop" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity" style="display:none;">
                                <label class="label" id="Registro_label_11" runat="server" style="font-weight: bold">Package (*)</label>
                                <asp:DropDownList ID="ddlPaquete" runat="server" CssClass="form-control marginTop" onchange="paqueteCliente(this);" Height="33px">
                                    <asp:ListItem Value="0">Select:</asp:ListItem>
                                    <asp:ListItem Value="01">Basic Pack</asp:ListItem>
                                    <asp:ListItem Value="02">Professional Pack</asp:ListItem>
                                    <asp:ListItem Value="03">Business Pack</asp:ListItem>
                                    <asp:ListItem Value="04">Millionaire Pack</asp:ListItem>
                                    <asp:ListItem Value="23">Imperial Pack</asp:ListItem>
                                    <asp:ListItem Value="05">Consultant Pack</asp:ListItem>
                                    <asp:ListItem Value="06">Intelligent Consumer</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity" style="display: none;">
                                <label class="label" id="Registro_label_12" runat="server"  style="font-weight: bold">Store Country</label>
                                <asp:DropDownList ID="cboPaisStore" runat="server" CssClass="form-control marginTop" Height="33px" />
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity" style="display: none;">
                                <label class="label" id="Registro_label_13" runat="server" style="font-weight: bold">Yachai Wasi Default</label>
                                <asp:DropDownList ID="cboTipoEstablecimiento" runat="server" CssClass="form-control btn-lg marginTop" Height="33px" />
                            </div>
                        </div>
                        
                        <div class="row col-md-12">
                           
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" id="Registro_label_14" runat="server" style="font-weight: bold">Names (*)</label>
                                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                    onkeyup="validarLetras(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" id="Registro_label_15" runat="server" style="font-weight: bold">Last Name (*)</label>
                                <asp:TextBox ID="txtApPaterno" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                    onkeyup="validarLetras(event)"></asp:TextBox>
                            </div>
                            <div id="divApeMat" class="form-group col-md-3 scrollflow -opacity" style="display: none;">
                                <label class="label" id="Registro_label_16" runat="server" style="font-weight: bold">Second surname</label>
                                <asp:TextBox ID="txtApMaterno" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                    onkeyup="validarLetras(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-3 scrollflow -opacity">
                                <label class="label" id="Registro_label_17" runat="server" style="font-weight: bold">Birth (*)</label>
                                <input type="text" id="datepicker" class="form-control text-uppercase marginTop" readonly runat="server" />
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_18" runat="server" style="font-weight: bold">Gender (*)</label>
                                <select id="ComboSexo" runat="server" class="form-control marginTop" style="height:33px;">
                                </select>
                                <%--<asp:DropDownList ID="ComboSexo2" runat="server" CssClass="form-control marginTop" Height="33px">
                                    <asp:ListItem Value="0">------</asp:ListItem>
                                    <asp:ListItem Value="1">MALE</asp:ListItem>
                                    <asp:ListItem Value="2">FEMALE</asp:ListItem>
                                    <asp:ListItem Value="3">NOT SPECIFIC</asp:ListItem>
                                </asp:DropDownList>--%>
                            </div>

                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_19" runat="server" style="font-weight: bold">Document type (*)</label>
                                <select id="ComboTipoDocumento" runat="server" class="form-control marginTop" style="height:33px;">
                                </select>
                                <%--<asp:DropDownList ID="ComboTipoDocumento2" runat="server" CssClass="form-control marginTop" Height="33px">
                                    <asp:ListItem Value="0">--------</asp:ListItem>
                                    <asp:ListItem Value="1">SSN (Social Security Number)</asp:ListItem>
                                    <asp:ListItem Value="2">PASSPORT</asp:ListItem>
                                    <asp:ListItem Value="3">ITIN</asp:ListItem>
                                </asp:DropDownList>--%>
                            </div>

                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_20" runat="server" style="font-weight: bold">Document No. (*)</label>
                                <asp:TextBox ID="txtNumDocumento" runat="server" CssClass="form-control text-uppercase marginTop solo-numero" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>

                        </div>
                        <div class="row col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_21" runat="server" style="font-weight: bold">Choose lenguage notifications (*)</label>
                                <select id="cbo_idioma" runat="server" class="form-control marginTop" style="height:33px">
                                </select>
                                    <%--<asp:DropDownList ID="cbo_idioma2" runat="server" CssClass="form-control marginTop" Height="33px">
                                        <asp:ListItem Value="0">--------</asp:ListItem>
                                        <asp:ListItem Value="1">Spanish</asp:ListItem>
                                        <asp:ListItem Value="2">English</asp:ListItem>
                                    </asp:DropDownList>--%>
                            </div>
                            
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_22" runat="server" style="font-weight: bold">Email (*)</label>
                                <asp:TextBox ID="TxtCorreo" runat="server" CssClass="form-control marginTop" TextMode="Email"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row col-md-12">

                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_23" runat="server" style="font-weight: bold">Phone</label>
                                <asp:TextBox ID="TxtTelefono" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-control text-uppercase marginTop solo-numero"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_24" runat="server" style="font-weight: bold">Mobile (*)</label>
                                <asp:TextBox ID="TxtCelular" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-control text-uppercase marginTop solo-numero"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row col-md-12">
                           
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_25" runat="server" style="font-weight: bold">Address (*)</label>
                                <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_26" runat="server" style="font-weight: bold">Address reference</label>
                                <asp:TextBox ID="TxtReferencia" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold" id="lblDepart" runat="server">State (*)</label>
                                <asp:DropDownList ID="cboDepartamento" runat="server" CssClass="form-control marginTop" Height="33px" />
                            </div>
                            <div id="ZipCode" class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold" id="lblZipCode" runat="server">Zip Code (*)</label>
                                <asp:DropDownList ID="cboZipCode" runat="server" CssClass="form-control marginTop" Height="33px" />
                            </div>
                            <div id="divProvincia" class="form-group col-md-4 scrollflow -opacity" style="display: none;">
                                <label class="label" style="font-weight: bold" id="lblProv" runat="server">Province</label>
                                <asp:DropDownList ID="cboProvincia" runat="server" CssClass="form-control marginTop" Height="33px" />
                            </div>
                            <div id="divDistrito" class="form-group col-md-4 scrollflow -opacity" style="display: none;">
                                <label class="label" style="font-weight: bold" id="Registro_label_27" runat="server">District</label>
                                <asp:DropDownList ID="cboDistrito" runat="server" CssClass="form-control marginTop" Height="33px" />
                                <br />
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity" style="display: none;">
                                <label class="label" style="font-weight: bold" id="Registro_label_28" runat="server">** Postal Code</label>
                                <asp:TextBox ID="txtCodigoPostal" runat="server" CssClass="form-control text-uppercase marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity" style="display: none;">
                                <label class="label" style="font-weight: bold" id="Registro_label_29" runat="server">Yachai Wasi of gifts (*)</label>
                                <asp:DropDownList ID="cboPremio" runat="server" CssClass="form-control btn-lg marginTop" Height="33px" />
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold" id="Registro_label_30" runat="server">(*) Required fields</label>
                            </div>
                        </div>

                    </div>
                    <div class="form-group" style="display: none;">
                        <label id="Registro_label_31" runat="server" style="font-weight: bold; font-size: 16px">BANK DATA</label>
                        <div class="row col-md-12">
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_32" runat="server" style="font-weight: bold">SSN/RUC</label>
                                <asp:TextBox ID="TxtRUC" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" id="Registro_label_33" runat="server" style="font-weight: bold">Bank</label>
                                <asp:TextBox ID="TxtBanco" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                    onkeyup="validarLetras(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-4 scrollflow -opacity">
                                <label class="label" style="font-weight: bold" id="Registro_label_34" runat="server">Deposit account No.</label>
                                <asp:TextBox ID="TxtNumCuenDeposito" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-6 scrollflow -opacity">
                                <label class="label" style="font-weight: bold" id="Registro_label_35" runat="server">Withdrawals account No.</label>
                                <asp:TextBox ID="TxtNumCuenDetracciones" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-6 scrollflow -opacity">
                                <label class="label" style="font-weight: bold" id="Registro_label_36" runat="server">Interbank account No.</label>
                                <asp:TextBox ID="TxtNumCuenInterbancaria" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="form-group row text-center">
                    <div class="col-lg-12">
                        <button class="btn-lg btn1 btn-success form-controlEPP anchoBotonGuardar" type="button" id="btnRegistrar" runat="server">SAVE</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="js/file-uploadv1.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/PreRegistroSocios.js?v22"></script>
    <script type="text/javascript">
        function pageLoad() {
            $('.file-upload').file_upload();
            $('.solo-numero').numeric();

            function filePreview(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagePreview').html("<img src='" + e.target.result + "' style='height:200px' />");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $('.imagen').change(function () {
                filePreview(this);
            });
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });

            function validarLetras(e) {
                var keyCode = (e.keyCode ? e.keyCode : e.which);
                if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
                    e.preventDefault();
                }
            }

            function validarNumeros(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            var idioma = $('#<%=hdn_Idioma.ClientID%>').val();
            $('#hdn_Idioma_online').val(idioma);
            //var x = $('#hdn_Idioma_online').val();
            //alert("x: " + x);
            
        }
    </script>

</asp:Content>
