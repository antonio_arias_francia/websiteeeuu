﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Zoom.aspx.cs" Inherits="SantaNaturaNetwork.Zoom" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!---->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />
    <style>      
        .contenedor{
        position: relative;
        display: inline-block;
        text-align: center;
        }
        .texto-encima{
            position: absolute;
            top: 10px;
            left: 10px;
        }
        .centrado{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        .datepicker {
            position: absolute !important;
            background-color: white !important;
        }
         .dataTables_filter {
        display: none;
        }
        
          #Progress
          {
            position: fixed;
            border-radius :7px;
            background: #f5f5f5;
            background-color: #f5f5f5;
            top: 40%;
            left: 35%;
            height: 30%;
            width: 30%;
            z-index: 100001;
            background-image: url('Imagenes/COLAGENO_FUERZA.PNG');
            background-repeat: no-repeat;
            background-position: center;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="css/estilosTablaMisCompras2-v1.css?v1" rel="stylesheet" />
    <link rel="stylesheet" href="css/bootstrapv2.min.css">
    <link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />

    <!---->
    <link rel="stylesheet" href="assets/Estilos/alertify.core.css" />
    <link rel="stylesheet" href="assets/Estilos/alertify.default.css" id="toggleCSS" />
    <script src="assets/Estilos/alertify.min.js" type="text/javascript"></script>


    <asp:HiddenField ID="hf_IdCliente" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hf_Establecimiento" ClientIDMode="Static" runat="server" />
    <input id="hdnTicket" name="hdnTicket" type="hidden">
    <asp:HiddenField ID="HiddenTicket" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenTienda" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenMondoPagar" ClientIDMode="Static" runat="server" />

    <asp:HiddenField ID="HiddenField_NombreCliente" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_idTipoCompra" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_FechaPago" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_TipoCompra" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_NotaDelivery" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_Ruc" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenRuta" ClientIDMode="Static" runat="server" />
   
    <asp:HiddenField ID="Ticket" runat="server" />
    <div style="padding-top: 90px; margin-bottom: 20px">
            <h1 style="text-align: center; font-family: Abril Fatface; font-size: 34px; font-style: normal; font-variant: normal; font-weight: 700; line-height: 26.4px; color: forestgreen">MEETING
            </h1>
        </div>
    <div class="container-tablaMisCompras2" style="margin-top: 80px;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
               <form>
                  <div class="form-group">
                    <label for="inputAddress" runat="server">Tema</label>
                    <input type="text" class="form-control" id="id_tema" runat="server" placeholder="Mi Reunión">
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-2">
                      <label for="inputCity">Fecha</label>
                      <input  type="date" class="form-control" runat="server" id="id_fecha">
                    </div>
                    <div class="form-group col-md-1">
                      <label for="inputZip">Hora</label>
                      <input type="time" class="form-control" runat="server" id="id_hora">
                    </div>  
                     <div class="form-group col-md-1">
                      <label for="inputZip">Duración</label>
                       <select id="cboduracion" runat="server" class="form-control" style="height:34px">
                                    <option value="0">Select:</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                        </select>
                    </div>  
                  </div>
               
                  <%--<button type="submit" class="btn btn-primary" OnClick="guardar();">Guardar</button>--%>
                  <%-- <asp:Button ID="id_guardar" runat="server" CssClass="btn btn-primary" Text="Guardar" Visible="true" OnClientClick="Button1_Click" />--%>
                   <asp:Button ID="id_guardar" runat="server" Text="Guardar" Visible="true" OnClick="Button1_Click" />
                </form>
            </div>
        </div>
    </div>
    

    <div class="container-tablaMisCompras2" style="margin-top: 80px;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="responsiveTbl table-responsive">
                                <table id="tableReuniones" style="text-align:center;width:100%" class="table table-hover table-condensed table-bordered w-auto table2">
                                             <thead class="table-success">
                                                <tr class="text-center" style="color: white;">
                                                    <th>ID</th>
                                                    <th>FECHA Y HORA</th>
                                                    <th>TEMA</th>
                                                    <th>ANFITRION</th>
                                                     <th>CLIENTE</th>
                                                </tr>
                                            </thead>                  
                                            <tfoot>
                                                <tr>
                                                </tr>
                                            </tfoot>
                                        </table>
                            </div>

                <%--<div class="responsiveTbl table-responsive">
                              <table class="table table-hover table-condensed table-bordered w-auto table2" style="text-align:center;width:100%" id="tableReuniones">
                                <thead class="table-success">
                                    <tr class="text-center" style="color: white">
                                        <th>ID</th>
                                        <th>FECHA Y HORA</th>
                                        <th>TEMA</th>
                                        <th>ENLACE</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                 <tfoot>
                                      <tr>
                                      </tr>
                                  </tfoot>
                            </table>
                        
                        </div>--%>
                </div>
            </div>
    </div>
    

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/bootstrap4.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>


    <script src="js/file-uploadv1.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <!--Nuevo DatePicker (calendario)-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <!--Nuevo DatePicker (calendario) en español-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.es.min.js"></script>

   
  <%--  <script>
        window.onload = function () {
            document.getElementById("idMenuTienda").style.color = 'white';
            document.getElementById("idMenuTienda").style.borderBottom = '3px solid white';
            document.getElementById("idSubMenuHistorialCompra").style.color = 'white';
            document.getElementById("idSubMenuHistorialCompra").style.borderBottom = '3px solid white';
        }

        $('#datetimepicker2').datepicker({
            weekStart: 0,
            todayBtn: "linked",
            language: "es",
            orientation: "bottom auto",
            keyboardNavigation: false,
            autoclose: true
        });
    </script>--%>

    <script>
        $("[id$=datepicker]").datepicker({
            uiLibrary: 'bootstrap4',
            format: 'dd/mm/yyyy',
            endDate: new Date()
        });               

        function validarLetras(e) {
            var keyCode = (e.keyCode ? e.keyCode : e.which);
            if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
                e.preventDefault();
            }
        }

        function validarNumeros(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
    <script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>    
    <link href="assets/css/dataTables.bootstrap.css" rel="stylesheet" />
     <script>
         $(document).ready(function ($) {
             listar_reuniones();
         });

         function listar_reuniones() {   
              var usuario = document.getElementById('hf_IdCliente').value;  
               var ndata; var table;
                $.ajax({
                   type: 'POST',
                   url: 'Zoom.aspx/ListaReuniones',
                   contentType: 'application/json; charset=utf-8',
                   dataType: 'json',
                   cache: false,
                   data: '{usuario: "' + usuario + '"}',
                    success: function (data) {
                        
                       ndata = data.d;
                       table = $('#tableReuniones').DataTable({
                           data: ndata,                      
                           columns: [                              
                                { data: 'IdReunion' },
                                { data: 'FechaReunion' },
                                { data: 'Tema' },
                                {  
                                    "data": {'Host_url':'Host_url'},
                                    "render": function (data) {
                                        var btnHost_url = '';
                                        //btnVoucher = ' <button type="button" title="Voucher" class="btn btn-primary btn-lg btn-xs" onclick="abrir_zoom(' + "'" + data.Host_url + "'" + ',);" ><i class="glyphicon glyphicon-list-alt"></i> Ir a Meeting</button>';
                                         btnHost_url = '<div class="contenedor"> <img src="Imagenes/zoom_logo_icon.png" style="cursor:pointer;" onclick="abrir_zoom(' + "'" + data.Host_url + "'" + ',);" /><div class="centrado"></div></div>';
                                        return btnHost_url;
                                    }
                               },
                                {  
                                    "data": {'Join_url':'Join_url'},
                                    "render": function (data) {
                                        var btnJoin_url = '';
                                        //btnVoucher = ' <button type="button" title="Voucher" class="btn btn-primary btn-lg btn-xs" onclick="abrir_zoom(' + "'" + data.Host_url + "'" + ',);" ><i class="glyphicon glyphicon-list-alt"></i> Ir a Meeting</button>';
                                         btnJoin_url = '<div class="contenedor"> <img src="Imagenes/zoom_logo_icon.png" style="cursor:pointer;" onclick="abrir_zoom(' + "'" + data.Join_url + "'" + ',);" /><div class="centrado"></div></div>';
                                        return btnJoin_url;
                                    }
                                }
                           ],
                           language: {
                               "decimal": "",
                               "emptyTable": "There is no information",
                               "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                               "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                               "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                               "infoPostFix": "",
                               "thousands": ",",
                               "lengthMenu": "Mostrar _MENU_ Entradas",
                               "loadingRecords": "Cargando...",
                               "processing": "Procesando...",
                               "search": "Buscar: ",
                               "zeroRecords": "Sin resultados encontrados",
                               "paginate": {
                                   "first": "First",
                                   "last": "Latest",
                                   "next": "Next",
                                   "previous": "Previous"
                               }
                           },
                           "scrollY": false,
                           "scrollX": true,
                           "scrollCollapse": true,
                           "ordering": true,
                           "bInfo": false,
                           "bLengthChange": false,
                           "paging": false,
                           "responsive": true,
                           "select": false,
                           "bDestroy": true,
                           "autoWidth":true
                       });                  
                   }
               })
         }
         function guardar() {
              $("#id_guardar").trigger("click");
         }
         function guardar_reunion() {
             var usuario = document.getElementById('hf_IdCliente').value;          
             var tema = document.getElementById('id_tema').value;
             var fecha = document.getElementById('id_fecha').value;
             var hora = document.getElementById('id_hora').value;
             var fecha_hora = fecha + " " + hora + ":00"; 

             if (tema == "") {
                 alert("Ingresar Tema");
                 return;
             }
              if (fecha == "") {
                  alert("Ingresar Fecha");
                  return;
             }
              if (hora == "") {
                  alert("Ingresar Hora");
                  return;
             }

            $.ajax({
                    type: 'POST',
                    url: 'Zoom.aspx/GuardarReunion',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    cache: false,
                    data: '{usuario: "' + usuario + '",tema: "' + tema + '",fecha: "' + fecha_hora + '"}',
                    success: function (data) {
                        ndata = data.d;
                        //var element_mensaje_nueva_clave = document.getElementById("mensaje_nueva_clave");
                        if (ndata == 'ok') {
                            //$('#ModalPreguntasSeguridad').modal('hide');
                            alert("correcto");
                        } else {
                            alert("Incorrecto");
                        }

                    }
                });
         }
         
         function abrir_zoom(enlace) {
            window.open(enlace, '_blank');
         }  
           
    </script>
</asp:Content>