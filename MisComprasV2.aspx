﻿<%@ Page Title="" Async="true" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="MisComprasV2.aspx.cs" Inherits="SantaNaturaNetwork.MisCompras2" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!---->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />
    <style>
        #tablaDetalleComprasRealizadasEfectivo.dataTable tbody tr:hover {
            background-color: gainsboro;
        }

            #tablaDetalleComprasRealizadasEfectivo.dataTable tbody tr:hover > .sorting_1 {
                background-color: gainsboro;
            }

        .datepicker {
            position: absolute !important;
            background-color: white !important;
        }

        .dataTables_filter {
            display: none;
        }

        #BtnAgregarVaucher {
            background-color: White;
            border-color: White;
            font-size: 0px;
            border: white;
        }

        #Progress {
            position: fixed;
            border-radius: 7px;
            background: #f5f5f5;
            background-color: #f5f5f5;
            top: 40%;
            left: 35%;
            height: 30%;
            width: 30%;
            z-index: 100001;
            background-image: url('Imagenes/COLAGENO_FUERZA.PNG');
            background-repeat: no-repeat;
            background-position: center;
        }

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(img/loadingPageSantanatura.gif) center no-repeat #fff;
        }
    </style>
    <script src="https://www.paypal.com/sdk/js?client-id=ARsiy6qtq4wNw8zEpuuxlngjV7uw83Xp3KenZ4eT2jC2ii2Vvtb5KUPe7CR9JY2ZGRyQS4pvtB9qvYP_"></script>
    <%--<script src="https://www.paypal.com/sdk/js?client-id=AfCQzWDi9TYauxQdJXJSt86JJ7OVF3tUuACWLN4SP1NOJ4qEzTuw5A5G3uRYCMl0WJbBiI1YBqYva6LK"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="css/estilosTablaMisCompras2-v1.css?v1" rel="stylesheet" />
    <link rel="stylesheet" href="css/bootstrapv2.min.css">
    <link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />

    <!---->
    <link rel="stylesheet" href="assets/Estilos/alertify.core.css" />
    <link rel="stylesheet" href="assets/Estilos/alertify.default.css" id="toggleCSS" />
    <script src="assets/Estilos/alertify.min.js" type="text/javascript"></script>


    <asp:HiddenField ID="hf_IdCliente" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hf_Establecimiento" ClientIDMode="Static" runat="server" />
    <input id="hdnTicket" name="hdnTicket" type="hidden">
    <asp:HiddenField ID="HiddenTicket" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenTienda" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenMondoPagar" ClientIDMode="Static" runat="server" />

    <asp:HiddenField ID="HiddenField_NombreCliente" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_idTipoCompra" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_FechaPago" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_TipoCompra" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_NotaDelivery" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenField_Ruc" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HiddenRuta" ClientIDMode="Static" runat="server" />
    <div id="page_loader" style="display: none" class="se-pre-con"></div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row justify-content-center">
                            <div>
                                <div id="paypal-button-container"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCancelM" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->

    <div class="container-tablaMisCompras2" style="margin-top: 80px;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab2" role="tablist">
                        <a class="nav-item nav-link active" id="nav-eeuu-tab" data-toggle="tab" href="#nav-pendiente-eeuu" role="tab" aria-controls="nav-pendiente-eeuu" aria-selected="false" onclick="TabPendingPurchasesCashEEUU();"><i class="glyphicon glyphicon-chevron-right" onclick="TabPendingPurchasesCash();"></i><%=nav_eeuu_tab %></a>
                        <a class="nav-item nav-link" id="nav-comp-tab" data-toggle="tab" href="#nav-complete-eeuu" role="tab" aria-controls="nav-complete-eeuu" aria-selected="false" onclick="TabPendingPurchasesMadeCashEEUU();"><i class="glyphicon glyphicon-chevron-right"></i><%=nav_comp_tab %></a>
                    </div>
                </nav>

                <div class="tab-content" id="nav-tabContent-eeuu">
                    <!-- PENDING PURCHASES CASH-->
                    <div class="tab-pane fade show active" id="nav-pendiente-eeuu" role="tabpanel" aria-labelledby="nav-eeuu-tab">
                        <div class="responsiveTbl table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto table2" style="text-align: center; width: 100%" id="tablePendingPurchasesCashEEUU">
                                <thead class="table-success">
                                    <tr class="text-center" style="color: white">
                                        <th id="MisCompras_th_70" runat="server">TICKET</th>
                                        <th id="MisCompras_th_71" runat="server">DATE ORDER</th>
                                        <th id="MisCompras_th_72" runat="server">AMOUNT</th>
                                        <th id="MisCompras_th_73" runat="server">TOTAL ORDER AMOUNT</th>
                                        <th id="MisCompras_th_74" runat="server">TOTAL NET AMOUNT</th>
                                        <th id="MisCompras_th_75" runat="server">REAL POINTS</th>
                                        <th id="MisCompras_th_76" runat="server">PROMOTION POINTS</th>
                                        <th id="MisCompras_th_77" runat="server">Y.W.</th>
                                        <th id="MisCompras_th_78" runat="server">CONDITION</th>
                                        <th></th>
                                        <th id="txPaypal" runat="server">PAY NOW</th>
                                        <th id="txtEliminar" runat="server">ELIMINAR</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>

                    <!--  PURCHASES MADE CASH -->
                    <div class="tab-pane fade" id="nav-complete-eeuu" role="tabpanel" aria-labelledby="nav-comp-tab">
                        <div class="responsiveTbl table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto table2" style="text-align: center; width: 100%" id="TablePurchasesMadeCashEEUU">
                                <thead class="table-success">
                                    <tr class="text-center tr2" style="color: white">
                                        <th id="MisCompras_th_79" runat="server" class="th2">TICKET</th>
                                        <th id="MisCompras_th_80" runat="server" class="th2">DATE ORDER</th>
                                        <th id="MisCompras_th_81" runat="server" class="th2">AMOUNT</th>
                                        <th id="MisCompras_th_82" runat="server" class="th2">TOTAL ORDER AMOUNT</th>
                                        <th id="MisCompras_th_83" runat="server" class="th2">TOTAL NET AMOUNT</th>
                                        <th id="MisCompras_th_84" runat="server" class="th2">REAL POINTS</th>
                                        <th id="MisCompras_th_85" runat="server" class="th2">PROMOTION POINTS</th>
                                        <th id="MisCompras_th_86" runat="server" class="th2">OFFICE</th>
                                        <th id="MisCompras_th_87" runat="server" class="th2">CONDITION</th>
                                        <th id="txTipoPago" runat="server" class="th2">PAYMENT METHOD</th>
                                        <th class="th2"></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Pendiente Efectivo -->
    <div class="modal" id="comprasPendientesEfectivoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="MisCompras_h5_6" runat="server">DETAIL OF MY PURCHASE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
                            <div class="responsiveTbl table-responsive">
                                <table id="tablaDetalleComprasRealizadasEfectivo" class="table table-hover table-condensed table-bordered w-auto table2" style="text-align: center; width: 100%" id="tablePendingPurchasesCashEEUU">
                                    <thead class="table-success">
                                        <tr class="text-center" style="color: white">
                                            <th id="MisCompras_th_97" runat="server">SUPER FOOD</th>
                                            <th id="MisCompras_th_98" runat="server">NAME</th>
                                            <th id="MisCompras_th_99" runat="server">QUANTITY</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>

                    <%--  <div class="row">
                         <div class="col-12 col-sm-12 col-md-12">
                            
                            <div class="table-responsive">
                             
                                <table id="tablaDetalleComprasRealizadasEfectivo" class="table table-bordred table-striped">
                                     <thead class="table-success">
                                        <tr style="color: white;">
                                            <th>SUPER FOOD</th>
                                            <th>NAME</th>
                                            <th>QUANTITY</th>
                                        </tr>
                                    </thead>                  
                                    <tfoot>
                                        <tr>
                                        </tr>
                                    </tfoot>
                                </table> 
                                </div>
                            
                             </div>
                        
                    </div>--%>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="Ticket" runat="server" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/bootstrap4.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>


    <script src="js/file-uploadv1.js"></script>
    <script src="js/HistorialCompras.js?v17"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10" type="text/javascript"></script>

    <script>
        window.onload = function () {
            document.getElementById("idMenuTienda").style.color = 'white';
            document.getElementById("idMenuTienda").style.borderBottom = '3px solid white';
            document.getElementById("idSubMenuHistorialCompra").style.color = 'white';
            document.getElementById("idSubMenuHistorialCompra").style.borderBottom = '3px solid white';
        }
        TabPendingPurchasesMadeCashEEUU();
        $('.solo-numero').numeric();

        function validarLetras(e) {
            var keyCode = (e.keyCode ? e.keyCode : e.which);
            if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
                e.preventDefault();
            }
        }

        function validarNumeros(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
    <script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>
    <link href="assets/css/dataTables.bootstrap.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {

            $(document).mouseup(function (e) {

                var buttons = $("#nav-pendiente-eeuu,#nav-complete-eeuu");

                var btn = e.target.id;
                if (btn == 'nav-eeuu-tab') {
                    // alert('PENDING PURCHASES');
                    $('.active').removeClass('active');
                }

                if (btn == 'nav-comp-tab') {
                    // alert('PENDING PURCHASES');
                    $('.active').removeClass('active');
                }

            });

            var IdCliente = document.getElementById('hf_IdCliente').value;
            LlenarDatatablePendingPurchasesCashEEUU(IdCliente);
        });

        function LlenarDatatableComprasRealizadasEfectivo(Ticket) {

            var ndata; var table;
            $.ajax({
                type: 'POST',
                url: 'MisComprasV2.aspx/DetalleComprasRealizadasEfectivo',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                data: '{Ticket: "' + Ticket + '"}',
                success: function (data) {

                    ndata = data.d;
                    table = $('#tablaDetalleComprasRealizadasEfectivo').DataTable({
                        data: ndata,
                        columns: [
                            {
                                "data": { 'Foto': 'Foto', 'Nombre': 'Nombre' },
                                "render": function (data) {
                                    var btnFoto = '<div class="col-9 col-sm-9 col-md-9 center-block" id="div-image" onclick="openProducto(' + "'" + data.Foto + "'" + ',' + "'" + data.Nombre + "'" + ');"><img src="products/' + data.Foto + '" class="img-responsive" style="cursor:pointer;width:100px; height:100px;" title="click here" /></div>'
                                    //   var btnFoto = "hola";//'<img src="products/' + data.Foto + '" class="img-responsive" style="cursor:pointer;width:50%;height:50%;";title="click here" />'
                                    return btnFoto;
                                }
                            },
                            { data: 'Nombre' },
                            { data: 'Cantidad' }
                        ],
                        language: {
                            "decimal": "",
                            "emptyTable": "There is no information",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar: ",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "First",
                                "last": "Latest",
                                "next": "Next",
                                "previous": "Previous"
                            }
                        },
                        "scrollY": false,
                        "scrollX": true,
                        "scrollCollapse": true,
                        "ordering": true,
                        "bInfo": false,
                        "bLengthChange": false,
                        "paging": false,
                        "responsive": true,
                        "select": false,
                        "bDestroy": true,
                        "autoWidth": true
                    });
                }
            })
        }

        function TabPendingPurchasesCashEEUU() {

            var IdCliente = document.getElementById('hf_IdCliente').value;
            LlenarDatatablePendingPurchasesCashEEUU(IdCliente);
        }

        function TabPendingPurchasesMadeCashEEUU() {

            var IdCliente = document.getElementById('hf_IdCliente').value;
            LlenarDatatablePurchaseMadeCashEEUU(IdCliente);
        }

        function LlenarDatatablePendingPurchasesCashEEUU(IdCliente) {
            var ndata; var table;
            $.ajax({
                type: 'POST',
                url: 'MisComprasV2.aspx/DetallePendingPurchasesCashEEUU',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                data: '{IdCliente: "' + IdCliente + '"}',
                success: function (data) {
                    ndata = data.d;
                    table = $('#tablePendingPurchasesCashEEUU').DataTable({
                        order: [[0, "desc"]],
                        data: ndata,
                        columns: [
                            { data: 'Ticket' },
                            { data: 'FechaPago2' },
                            { data: 'Cantidad' },
                            { data: 'MontoAPagar' },
                            { data: 'MontoNeto' },
                            { data: 'PuntosTotal' },
                            { data: 'PuntosTotalPromo' },
                            { data: 'Despacho' },
                            {
                                "data": 'Ticket',
                                "render": function (Ticket) {
                                    var label1 = '<label for="Name">WAITING FOR PAYMENT</label>';
                                    return label1;
                                }
                            },
                            {
                                "data": { 'Ticket': 'Ticket' },
                                "render": function (data) {
                                    var btnDetalle = '';
                                    btnDetalle = ' <button type="button" class="btn btn-success btn-lg btn-xs" onclick="VerDetalle(' + "'" + data.Ticket + "'" + ',);" ><i class="glyphicon glyphicon-chevron-right"></i> See Detail</button>';
                                    return btnDetalle;
                                }
                            },
                            {
                                "data": { 'Ticket': 'Ticket', 'ValBtnPay': 'ValBtnPay' },
                                "render": function (data) {
                                    var btnPaypal = '';
                                    var Sty = (data.ValBtnPay == "1") ? "" : "none";
                                    btnPaypal = ' <button type="button" style="display:' + Sty + '" class="btn btn-info btn-lg btn-xs" onclick="AbrilModalPay(' + "'" + data.Ticket + "'" + ',);" ><i class="glyphicon glyphicon-chevron-right"></i> PAYPAL</button>';
                                    return btnPaypal;
                                }
                            },
                            {
                                data: { 'Ticket': 'Ticket' },
                                "render": function (data) {
                                    var btnDetalle = '';
                                    btnDetalle = ' <button type="button" class="btn btn-danger btn-lg btn-xs" onclick="EliminarCompra(' + "'" + data.Ticket + "'" + ',);" ><i class="glyphicon glyphicon-chevron-right"></i> Eliminar</button>';
                                    return btnDetalle;
                                }
                            },
                        ],
                        language: {
                            "decimal": "",
                            "emptyTable": "There is no information",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar: ",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "First",
                                "last": "Latest",
                                "next": "Next",
                                "previous": "Previous"
                            }
                        },
                        "scrollY": false,
                        "scrollX": true,
                        "scrollCollapse": true,
                        "ordering": true,
                        "bInfo": false,
                        "bLengthChange": false,
                        "paging": true,
                        "responsive": true,
                        "select": false,
                        "bDestroy": true,
                        "autoWidth": true

                    });
                }
            })
        }

        function LlenarDatatablePurchaseMadeCashEEUU(IdCliente) {

            var ndata; var table;
            $.ajax({
                type: 'POST',
                url: 'MisComprasV2.aspx/DetallePendingPurchasesMadeCashEEUU',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                data: '{IdCliente: "' + IdCliente + '"}',
                success: function (data) {

                    ndata = data.d;
                    table = $('#TablePurchasesMadeCashEEUU').DataTable({
                        order: [[0, "desc"]],
                        data: ndata,
                        columns: [
                            { data: 'Ticket' },
                            { data: 'FechaPago2' },
                            { data: 'Cantidad' },
                            { data: 'MontoAPagar' },
                            { data: 'MontoNeto' },
                            { data: 'PuntosTotal' },
                            { data: 'PuntosTotalPromo' },
                            { data: 'Despacho' },
                            { data: 'TipoPago' },
                            {
                                "data": 'Ticket',
                                "render": function (Ticket) {
                                    var label1 = '<label for="Name">COMPLETED</label>';
                                    return label1;
                                }
                            },
                            {
                                "data": { 'Ticket': 'Ticket' },
                                "render": function (data) {
                                    var btnDetalle = '';
                                    btnDetalle = ' <button type="button" title="See Detail" class="btn btn-success btn-lg btn-xs" onclick="VerDetalle(' + "'" + data.Ticket + "'" + ',);" ><i class="glyphicon glyphicon-chevron-right"></i> See Detail</button>';
                                    return btnDetalle;
                                }
                            },
                        ],
                        language: {
                            "decimal": "",
                            "emptyTable": "There is no information",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar: ",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "First",
                                "last": "Latest",
                                "next": "Next",
                                "previous": "Previous"
                            }
                        },
                        "scrollY": false,
                        "scrollX": true,
                        "scrollCollapse": true,
                        "ordering": true,
                        "bInfo": false,
                        "bLengthChange": false,
                        "paging": true,
                        "responsive": true,
                        "select": false,
                        "bDestroy": true,
                        "autoWidth": true

                    });
                }
            })
        }

        function RemoverPendingPurchases(Ticket) {
            $('#confirm-delete').modal('show');
            hdnTicket.value = Ticket
        }

        function RemoverSi() {

            var Ticket = hdnTicket.value;
            var ndata; var table;
            $.ajax({
                type: 'POST',
                url: 'MisComprasV2.aspx/EliminarCompraCliente',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                data: '{Ticket: "' + Ticket + '"}',
                success: function (data) {

                    ndata = data.d;

                    var IdCliente = document.getElementById('hf_IdCliente').value;
                    LlenarDatatablePendingPurchases(IdCliente);
                }
            });
        }

        function VerDetalle(Ticket) {
            $('#comprasPendientesEfectivoModal').modal('show');
            LlenarDatatableComprasRealizadasEfectivo(Ticket);
        }

        function confirmar() {
            //un confirm
            alertify.confirm("<p>Aquí confirmamos algo.<br><br><b>ENTER</b> y <b>ESC</b> corresponden a <b>Aceptar</b> o <b>Cancelar</b></p>", function (e) {
                if (e) {
                    alertify.success("Has pulsado '" + alertify.labels.ok + "'");
                } else {
                    alertify.error("Has pulsado '" + alertify.labels.cancel + "'");
                }
            });
            return false
        }

        function datos() {
            //un prompt
            alertify.prompt("Esto es un <b>prompt</b>, introduce un valor:", function (e, str) {
                if (e) {
                    alertify.success("Has pulsado '" + alertify.labels.ok + "'' e introducido: " + str);
                } else {
                    alertify.error("Has pulsado '" + alertify.labels.cancel + "'");
                }
            });
            return false;
        }

        function notificacion() {
            //una notificación normal
            alertify.log("Esto es una notificación cualquiera.");
            return false;
        }

        function ok() {
            //una notificación correcta
            alertify.success("Visita nuestro <a href=\"https://blog.reaccionestudio.com/\" style=\"color:white;\" target=\"_blank\"><b>BLOG.</b></a>");
            return false;
        }

        function openProducto(Foto, Nombre) {
            Foto = "products/" + Foto;
            document.getElementById('img_result').setAttribute('src', Foto);
            document.getElementById('TituloProducto').innerHTML = Nombre;
            $('#ModalProducto').modal('show');
        }

        function EliminarCompra(Ticket) {
            Swal.fire({
                title: 'Está seguro de eliminar la compra?',
                text: "Esto no puede revertirse!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    EliminarCompraExpiradaEfectivo(Ticket);
                }
            });
            
        }

        function EliminarCompraExpiradaEfectivo(Ticket) {
            $.ajax({
                type: 'POST',
                url: 'MisComprasV2.aspx/EliminarCompraExpirada',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                data: '{Ticket: "' + Ticket + '"}',
                success: function (data) {
                    Swal.fire({
                        title: 'Perfecto!',
                        text: 'La compra ha sido eliminada',
                        type: "success"
                    }).then(function () {
                        TabPendingPurchasesCashEEUU();
                    });
                }
            })
        }

    </script>
</asp:Content>
