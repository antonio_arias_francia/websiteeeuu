﻿<%@ Page Language="C#" Async="true"  AutoEventWireup="true" CodeBehind="QuickbooksPage.aspx.cs" Inherits="SantaNaturaNetworkV3.QuickbooksPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title></title>
        <% if (SantaNaturaNetworkV3.Quickbooks.QBOLogic.dictionary.ContainsKey("accessToken"))
            {
                Response.Write("<script> window.opener.location.reload();window.close(); </script>");
            }
        %> 
    </head>
    <body>
        <form id="form2" runat="server">
            <div>
    
                 <p>&nbsp;</p>
            </div>
            <div id="connect" runat="server">
                <asp:ImageButton id="btnOAuth" runat="server" AlternateText="OAuth"
                   ImageAlign="left"
                   ImageUrl="~/imagenes/C2QB_white_btn_lg_default.png"
                    CssClass="font-size:14px; border: 1px solid grey; padding: 10px; color: red" Height="40px" Width="200px" OnClick="btnOAuth_Click"/>
                <br /><br /><br />
            </div>
            <div id="revoke" runat="server" visible ="false">
                <p>
                <asp:label runat="server" id="lblConnected" visible="false">"La aplicación se encuentra conectada a Quickbooks."</asp:label>
                </p>  

                <br />  

                                <p><asp:label runat="server" id="lblQBOCall" visible="false"></asp:label></p>
                <asp:Button ID="btnRefreshToken" runat="server" OnClick="btnRefreshToken_Click" Text="RefreshToken" />
                <br /><br /><br />
            </div>
            <p>

                &nbsp;</p>
            <label><%= SantaNaturaNetworkV3.Quickbooks.QBOLogic.getExpirationDate() %></label>
            <br />

            <label><%= SantaNaturaNetworkV3.Quickbooks.QBOLogic.getError() %></label>

            <br />

            <label><%= SantaNaturaNetworkV3.Quickbooks.QBOLogic.getTrama() %></label>
            <br />

            <label><%= SantaNaturaNetworkV3.Quickbooks.QBOLogic.getTramaQB() %></label>

        </form>
    </body>
</html>