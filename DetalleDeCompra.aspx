﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="DetalleDeCompra.aspx.cs" Inherits="SantaNaturaNetwork.DetalleDeCompra" ClientIDMode="Static" %>

<%@ OutputCache Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/Banner de Store Template/animate.css">
    <!--Para que el banner salga muy bien diseñado-->
    <link rel="stylesheet" href="css/proyecto2/estilosDetalleDeCompra-Banner.css">
    <link href="css/proyecto2/vendors/elegant-icon/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="icon" href="img/Natural_Food_icon.png" type="image/x-icon" />

    <!--Para el boton ingresar foto de perfil-->
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />
    <link href="css/carritoDeCompra.css" rel="stylesheet" />
    <link href="css/estilosDetalleCompra.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/proyecto2/eskju.jquery.scrollflow.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.7/dist/sweetalert2.all.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        .parallax2 {
            /* The image used */
            background: url("img/detalledecompra-1355x210.png");
            /*width:100vw;*/
            /* Set a specific height */
            /* Create the parallax scrolling effect */
            background-attachment: fixed;
            background-position: bottom;
            background-repeat: no-repeat
        }

        .modal-body {
            height: initial;
            overflow: hidden;
            text-align: justify;
        }

        /*.modal-body1 {
            text-align:justify;
            height: 470px;
            overflow: auto;
        }*/

        /*.modal-body1:hover {
                overflow-y: auto;
            }*/

        .btn-visa-upload {
            height: 49px;
            border-radius: 8px;
            background: #3850A1;
            font-size: 15px;
            color: white;
            line-height: 46px;
            border: 1px solid transparent;
            margin: 10px 0px 10px 0px;
        }

            .btn-visa-upload:hover {
                background-color: #D6D9D9;
                color: #3850A1;
            }


        #txtNombreDatosCompra, #txtDNIDatosCompra, #txtCelularDatosCompra, #txtDireccionDatosCompra, #txtTransporteDatosCompra, #txtDirecTransporteDatosCompra, #txtProvinciaDatosCompra, #txtDirecProvinciaDatosCompra {
            height: 24.5px
        }

        .btnValidar{
            margin-top: 10px;
            height: calc(2.25rem + 2px);
        }

        .txtRUC{
            height: calc(2.25rem + 2px);
        }

        .lblValidarFacturacion{
            justify-content: center;
        }
    </style>

    <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see https://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <%--<asp:ScriptReference Name="MsAjaxBundle" />--%>
                <%--<asp:ScriptReference Name="jquery" />--%>
                <%--<asp:ScriptReference Name="bootstrap" />--%>
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <%--<asp:ScriptReference Name="WebFormsBundle" />--%>
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
    <%--    <!--================Categories Banner Area =================-->
    <!--================End Categories Banner Area =================-->--%>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <aside id="colorlib-hero" class="breadcrumbs" style="margin-top: 50px">
        <div class="flexslider">
            <ul class="slides">
                <li <%--class="parallax2" style="background-size:100% 500px"--%>>
                    <img src="img/detalledecompra.png" style="width: 100vw; height: 300px; background-attachment: fixed" />
                    <div class="overlay">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                    <div class="slider-text-inner text-center" style="z-index: 1">
                                        <h1>Carrito </h1>
                                        <h2 class="bread">
                                            <span><a id="inicio" href="TiendaSN.aspx" style="color: green; font-weight: bold; transition: color 0.5s ease;">Tienda</a></span>
                                            <span style="font-size: 10px"><a style="padding-top: 5px; color: green" class="glyphicon glyphicon-chevron-right"></a></span>
                                            <span><a>Carrito</a></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>



    <div id="ContenidoFluido" class="container-fluid" style="width: 92%; margin-top: 20px">

        <div class="row row-pb-md" style="padding-top: 30px; display: block;">
            <div class="col-md-10 col-md-offset-1">
                <div class="process-wrap">
                    <div id="circle01" class="process text-center active">
                        <p><span>01</span></p>
                        <h3 id="carritoDeCompras" class="active">CARRITO DE COMPRAS</h3>
                    </div>
                    <div id="circle02" class="process text-center">
                        <p><span style="transition: all 1s; ">02</span></p>
                        <h3 id="afiliacion" style="transition: all 1s; ">AFILIACION</h3>
                    </div>
                    <div id="circle03" class="process text-center">
                        <p><span id="circulito" style="transition: all 1s; ">03</span></p>
                        <h3 id="ordenCompletada" style="transition: all 1s; ">ORDEN COMPLETADA</h3>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <hr />
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background: #898e8bb5;">
            <div class="container modal-dialog" id="modalTamano" role="document">
                <div class="modal-content" style="position: absolute; width: auto;">
                    <div class="modal-header">
                        <h4 style="text-align: center; font-weight: bold" class="modal-title text-center" id="exampleModalLabel">Terminos y Condiciones</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body1">
                            <span>
                                <br />
                                El presente documento contiene los Términos y Condiciones Generales aplicables al presente portal web, así como las condiciones de uso y venta que rigen las compras en el presente sitio web, incluyendo las limitaciones y exclusiones correspondientes. La utilización del Sitio y/o de sus servicios constituye la aceptación de las condiciones.
                        <br />
                                <br />
                                1. <strong style="text-decoration: underline">TÉRMINOS Y CONDICIONES </strong>
                                <br>
                                <br>
                                <span class="scrollflow -opacity">Este documento describe los términos y condiciones generales (en adelante, los "Términos y Condiciones Generales aplicables al acceso y uso de los servicios ofrecidos por ANDINA NATURAL Y DISTRIBUCION E.I.R.L.  dentro del presente portal web, sus subdominios y/u otros dominios (urls) relacionados (en adelante “el Sitio”) en donde éstos Términos y Condiciones Generales se encuentren. Cualquier persona que desee acceder y/o suscribirse y/o usar el Sitio o los Servicios podrá hacerlo sujetándose a los Términos y Condiciones Generales, junto con todas las demás políticas y principios que rigen el Sitio y que son incorporados al presente directamente o por referencia o que son explicados y/o detallados en otras secciones del Sitio. En consecuencia, todas las visitas y todos los contratos y transacciones que se realicen en este Sitio, así como sus efectos jurídicos, quedarán regidos por estas reglas y sometidos a la legislación aplicable en Perú.  Los Términos y Condiciones Generales contenidos en este instrumento se aplicarán y se entenderán como parte integral de todos los actos y contratos que se ejecuten o celebren mediante los sistemas de oferta y comercialización comprendidos en este sitio entre los usuarios de este Sitio y ANDINA NATURAL Y DISTRIBUCION E.I.R.L. ( “la Empresa”), y por cualquiera de las otras sociedades o empresas que sean filiales o vinculadas a ella, y que hagan uso de este sitio, a las cuales se las denominará en adelante también en forma indistinta como las "Empresas", o bien la "Empresa Oferente", el "Proveedor" o la "Empresa Proveedora", según convenga al sentido del texto. Cualquier persona que no acepte estos Términos y Condiciones Generales, los cuales tienen un carácter obligatorio y vinculante, deberá abstenerse de utilizar el Sitio y/o los Servicios.  El Usuario debe leer, entender y aceptar todas las condiciones establecidas en los Términos y Condiciones Generales de LA EMPRESA , así como en los demás documentos incorporados a los mismos por referencia, previo a su registro como Usuario del Sitio y/o a la adquisición de productos y/o entrega de cualquier dato, quedando sujetos a lo señalado y dispuesto en los Términos y Condiciones Generales. Cuando usted visita el presente Sitio se está comunicando con LA EMPRESA de manera electrónica. En ese sentido, usted brinda su consentimiento para recibir comunicaciones de LA EMPRESA por correo electrónico o mediante la publicación de avisos en su portal.  1. CAPACIDAD LEGAL Los Servicios sólo están disponibles para personas que tengan capacidad legal para contratar. No podrán utilizar los servicios las personas que no tengan dicha capacidad, como Usuarios del Sitio que hayan sido suspendidos temporalmente o inhabilitados definitivamente en razón de lo dispuesto en la sección 2 “Registro y Uso del Sitio”. Los menores de edad no podrán actuar en el Sitio, y si lo hicieran deben contar con la autorización expresa de sus padres o tutores. Ellos serán responsables de los actos realizados por los menores de edad en este Sitio, en ejercicio de la representación legal con la que cuentan.  </span>
                                <br>
                                <br>
                                <b class="scrollflow -opacity">2. <strong style="text-decoration: underline">REGISTRO Y USO DEL SITIO</strong></b>
                                <br>
                                <br>
                                <span class="scrollflow -opacity">Es obligatorio llenar el formulario de registro en todos sus campos con datos válidos y verdaderos para convertirse en Usuario autorizado del Sitio (en adelante, el "Usuario"), de esta manera, podrá acceder a las promociones, y a la adquisición de productos y/o servicios ofrecidos en este Sitio. El futuro Usuario deberá completar el formulario de registro con su información personal de manera exacta, precisa y verdadera ("Datos Personales") y asume el compromiso de actualizarlos conforme resulte necesario.  LA EMPRESA podrá utilizar diversos medios para identificar a sus Usuarios, pero no se responsabiliza por la certeza de los Datos Personales provistos por estos. Los Usuarios garantizan y responden, en cualquier caso, de la exactitud, veracidad, vigencia y autenticidad de los Datos Personales ingresados. En ese sentido, la declaración realizada por los Usuarios al momento de registrarse se entenderá como una Declaración Jurada. Cada Usuario sólo podrá ser titular de una (1) cuenta en el Sitio web, no pudiendo acceder a más de una (1) cuenta con distintas direcciones de correo electrónico o falseando, modificando y/o alterando sus datos personales de cualquier manera posible. En caso se detecte esta infracción, LA EMPRESA se comunicará con el cliente informándole que todas sus cuentas serán agrupadas en una sola cuenta anulándose todas sus demás cuentas. Ello se informará al Usuario mediante el correo electrónico indicado por él mismo, o el último registrado en LA EMPRESA, sin perjuicio de lo anterior, LA EMPRESA se reserva el derecho de iniciar las acciones legales que le correspondan. Si se verifica o sospecha algún uso fraudulento y/o malintencionado y/o contrario a estos Términos y Condiciones y/o contrarios a la buena fe, LA EMPRESA tendrá el derecho inapelable de dar por terminados los créditos, no hacer efectiva las promociones, cancelar las transacciones en curso, dar de baja las cuentas y hasta de perseguir judicialmente a los infractores. LA EMPRESA podrá realizar los controles que crea convenientes para verificar la veracidad de la información dada por el Usuario. En ese sentido, se reserva el derecho de solicitar algún comprobante y/o dato adicional a efectos de corroborar los Datos Personales, así como de suspender temporal o definitivamente a aquellos Usuarios cuyos datos no hayan podido ser confirmados. En caso de suspensión temporal LA EMPRESA comunicará al cliente informando el tiempo de suspensión de la cuenta. En casos de inhabilitación, LA EMPRESA podrá dar de baja la compra efectuada, sin que ello genere derecho alguno a resarcimiento, pago y/o indemnización. El Usuario, una vez registrado, dispondrá en su dirección de email una clave secreta (en adelante la "Clave") que le permitirá el acceso personalizado, confidencial y seguro. En caso de olvidar y/o extraviar su Clave, el Usuario tendrá la posibilidad de solicitar una nueva Clave de acceso para lo cual deberá sujetarse al procedimiento establecido en el sitio respectivo. El Usuario se obliga a mantener la confidencialidad de su Clave de acceso, asumiendo totalmente la responsabilidad por el mantenimiento de la confidencialidad de su Clave secreta registrada en este sitio web, la cual le permite efectuar compras, solicitar servicios y obtener información (la “Cuenta”). Dicha Clave es de uso personal, y su entrega a terceros no involucra responsabilidad de LA EMPRESA o de las empresas en caso de utilización indebida, negligente y/o incorrecta. El Usuario será responsable por todas las operaciones efectuadas en y desde su Cuenta, pues el acceso a la misma está restringido al ingreso y uso de una Clave secreta, de uso y conocimiento exclusivo del Usuario. El Usuario, además, se compromete a notificar a LA EMPRESA en forma inmediata y por medio idóneo y fehaciente, cualquier uso indebido o no autorizado de su Cuenta y/o Clave, así como el ingreso por terceros no autorizados a la misma. Se 
                        aclara que está prohibida la venta, cesión, préstamo o transferencia de la Clave y/o Cuenta bajo ningún título.  LA EMPRESA se reserva el derecho de rechazar cualquier solicitud de registro o de cancelar un registro previamente aceptado, sin que esté obligado a comunicar o exponer las razones de su decisión y sin que ello genere algún derecho a indemnización o resarcimiento. El registro del Usuario es personal y no se puede transferir por ningún motivo a terceras personas. En ese sentido, ningún usuario podrá vender, intentar vender, ceder o transferir un usuario o contraseña. Por lo dicho, LA EMPRESA podrá suspender o cancelar definitivamente una cuenta en el caso de una venta, ofrecimiento de venta, cesión o transferencia, en infracción de lo dispuesto en el presente párrafo. </span>
                                <br>
                                <br>
                                <span class="scrollflow -opacity">3. <strong style="text-decoration: underline">MODIFICACIONES</strong>
                                    <br>
                                    <br>
                                    LA EMPRESA podrá modificar los Términos y Condiciones Generales en cualquier momento y sin previo aviso con el solo hecho de hacerlos públicos en el Sitio WEB. Los términos modificados entrarán en vigencia de forma automática después de su publicación, y surtirán efectos para todas las compras que se realicen desde dicha fecha. 
                        <br>
                                    <br>
                                    4. <strong style="text-decoration: underline">MEDIOS DE PAGO QUE SE PODRÁN UTILIZAR EN EL SITIO </strong>
                                    <br>
                                    <br>
                                    Los productos ofrecidos en el Sitio, salvo que se señale una forma diferente para casos particulares u ofertas de determinados bienes, sólo pueden ser pagados con los medios que en cada caso específicamente se indiquen. El uso de tarjetas de crédito o débito se sujetará a lo establecido en estos Términos y Condiciones Generales y, en relación con su emisor, y a lo pactado en los respectivos contratos suscritos con el banco emisor del medio de pago. En caso de contradicción, predominará lo expresado en ese último instrumento. Tratándose de tarjetas bancarias aceptadas en el Sitio, los aspectos relativos a éstas, tales como la fecha de emisión, caducidad, bloqueos, cobros de comisiones, interés de compra en cuotas etc., se regirán por los respectivos contratos suscritos con el banco emisor del medio de pago, de tal forma que la Empresa no tendrán responsabilidad por cualquiera de los aspectos señalados. El Sitio podrá indicar determinadas condiciones de compra según el medio de pago que se utilice por el usuario. Al utilizar una tarjeta de crédito o débito, si el nombre del titular de dicha tarjeta no coincide con el nombre utilizado al registrarse en el portal de LA EMPRESA, el usuario será responsable del uso de esa tarjeta y en el caso de fraude el usuario será responsable de las implicaciones civiles y penales que genere el uso indebido. 
                        <br>
                                    <br>
                                    5. <strong style="text-decoration: underline">PROMOCIONES </strong>
                                    <br>
                                    <br>
                                    Cuando el Sitio ofrezca promociones que consistan en la entrega gratuita o rebajada de un producto por la compra de otro, el despacho del bien que se entregue gratuitamente o a precio rebajado, se hará en el mismo lugar en el cual se despacha el producto comprado. El Sitio somete sus promociones y actividades promocionales al cumplimiento de las normas vigentes. Además de los Términos y Condiciones Generales establecidos en este documento, cuando LA EMPRESA realice promociones en radio, televisión, revistas u otros medios publicitarios, aplican adicionalmente los Términos y Condiciones específicos de la promoción.  </span>
                                <br>
                                <br>
                                <b class="scrollflow -opacity">6. <strong style="text-decoration: underline">DESPACHO DE LOS PRODUCTOS</strong></b>
                                <br>
                                <br>
                                <span class="scrollflow -opacity">Los productos adquiridos a través de la página web se sujetarán a las condiciones de despacho y entrega y disponibles en el Sitio, entre ellas, las siguientes:
                    <br>
                                    <br />
                                    • La información del lugar de envío es de exclusiva responsabilidad del cliente. Por lo que será de su responsabilidad la exactitud de los datos indicados para realizar una correcta y oportuna entrega de los productos a su domicilio o dirección de envío. Si hubiera algún error en la dirección, el producto podría no llegar en la fecha indicada.  
                        <br>
                                    • Los plazos elegidos para el despacho y entrega, se cuentan desde que LA EMPRESA valida la orden de compra y el medio de pago utilizado, considerándose días hábiles para el cumplimiento de dicho plazo.  
                        <br>
                                    • El cambio de dirección solo se podrá hacer antes de recibir la confirmación de LA EMPRESA.  
                        <br>
                                    • LA EMPRESA realizará hasta dos intentos de visita al domicilio indicado por el cliente.  
                        <br />
                                    • El siguiente día útil de efectuada la primera visita, el transportista realizará un último intento de entrega del pedido. Si en esta segunda entrega, al cliente se le vuelve a encontrar ausente, el pedido será retornado, en este caso, el cliente deberá recoger su pedido en las oficinas de LA EMPRESA o solicitar y pagar un nuevo flete.  
                        <br>
                                    • Posteriormente le llegará un correo electrónico al cliente sobre la anulación del pedido. En caso el cliente aún quiera el pedido, deberá solicitar un nuevo delivery a su costo o recogerlo de las oficinas de LA EMPRESA.  
                        <br>
                                    • LA EMPRESA cuenta con cobertura de despachos a nivel de Lima y Provincias, sin embargo, hay destinos rurales o de difícil acceso en los cuales no podrá efectuar despachos y esto será identificado por el cliente al momento de realizar su compra. En caso la ubicación del domicilio del cliente no pueda atenderse porque está en una calle o zona de difícil acceso, LA EMPRESA se comunicará con el cliente para gestionar un cambio de domicilio y poder entregar el producto adquirido.  
                        <br>
                                    • Cuando el cliente recibe un producto, deberá validar que la caja o bolsa que contenga el producto, está sellada y no tenga signos de apertura previa. En caso contrario, no deberá recibir el producto y deberá ponerse en contacto inmediatamente con LA EMPRESA a través de nuestro Servicio de Atención al Cliente. En caso que el producto fuera recibido en buenas condiciones y completo, el cliente firmará la guía de entrega correspondiente, dejando así conformidad de la entrega. Luego de la aceptación del producto y firma de la guía, el sólo podrá presentar reclamos por temas de garantía o cualquiera descrito dentro de la Política de Devolución y Cambios en los tiempos establecidos en estos Términos y Condiciones Generales.  
                        
                        <br />
                                    <br />
                                </span>
                                <span class="scrollflow -opacity">7. <strong style="text-decoration: underline">POLÍTICA DE DEVOLUCIONES Y CAMBIOS </strong>
                                    <br>
                                    <br>
                                    Para solicitar un cambio o devolución se debe tener en cuenta la Política de Cambios y Devoluciones publicada en el Sitio.  
                        <br>
                                    <br>
                                    8. <strong style="text-decoration: underline">COMPROBANTE DE PAGO </strong>
                                    <br>
                                    <br>
                                    LA EMPRESA emitirá a los Usuarios boletas o facturas de venta electrónicas, las mismas que constituyen el comprobante de pago de la adquisición de productos efectuada en el Sitio.  
                        <br>
                                    <br>
                                    9. <strong style="text-decoration: underline">PROPIEDAD INTELECTUAL </strong>
                                    <br>
                                    <br>
                                    Todo el contenido incluido o puesto a disposición del Usuario en el Sitio, es de propiedad de LA EMPRESA o ha sido licenciada a ésta por terceros. La compilación del Contenido es propiedad exclusiva de LA EMPRESA y, en tal sentido, el Usuario debe abstenerse de extraer y/o reutilizar partes del Contenido sin el consentimiento previo y expreso de la Empresa. Además del Contenido, las marcas, denominativas o figurativas y cualquier otro elemento de propiedad intelectual que haga parte del Contenido, son de propiedad de LA EMPRESA y, por tal razón, están protegidas por las leyes y los tratados internacionales de derecho de autor, marcas, patentes, modelos y diseños industriales.  El uso indebido y la reproducción total o parcial de dichos contenidos quedan prohibidos, salvo autorización expresa y por escrito de LA EMPRESA . Asimismo, no pueden ser usadas por los Usuarios en conexión con cualquier producto o servicio que no sea provisto por LA EMPRESA . En el mismo sentido, la Propiedad Industrial no 
podrá ser usada por los Usuarios en conexión con cualquier producto y servicio que no sea de aquellos que comercializa u ofrece LA EMPRESA o de forma que produzca confusión con sus clientes o que desacredite a la Empresa o a las Empresas Proveedoras.  
                        <br>
                                    <br>
                                    10. <strong style="text-decoration: underline">RESPONSABILIDAD DE LA EMPRESA </strong>
                                    <br>
                                    <br>
                                    LA EMPRESA hará lo posible dentro de sus capacidades para que la transmisión del Sitio sea ininterrumpida y libre de errores. Sin embargo, dada la naturaleza de la Internet, dichas condiciones no pueden ser garantizadas. En el mismo sentido, el acceso del Usuario a la Cuenta puede ser ocasionalmente restringido o suspendido con el objeto de efectuar reparaciones, mantenimiento o introducir nuevos Servicios. LA EMPRESA no será responsable por pérdidas (i) que no hayan sido causadas por el incumplimiento de sus obligaciones; (ii) lucro cesante o pérdidas de oportunidades comerciales; (iii) cualquier daño indirecto.  </span>
                                <br>
                                <br>
                                <b class="scrollflow -opacity">11. <strong style="text-decoration: underline">INDEMNIZACIÓN </strong></b>
                                <br>
                                <br>
                                <span class="scrollflow -opacity">El Usuario indemnizará y mantendrá indemne a LA EMPRESA, directivos, administradores, representantes y empleados, por su incumplimiento en los Términos y Condiciones Generales y demás Políticas que se entienden incorporadas al presente o por la violación de cualesquiera leyes o derechos de terceros, incluyendo los honorarios de abogados correspondientes.  </span>
                                <br>
                                <br>
                                <b class="scrollflow -opacity">12. <strong style="text-decoration: underline">TÉRMINOS DE LEY </strong></b>
                                <br>
                                <br>
                                <span class="scrollflow -opacity">Estos Términos y Condiciones Generales y su aplicación serán interpretados de acuerdo con las leyes de Perú, sin dar efecto a cualquier principio de conflictos de ley. Si alguna disposición de estos Términos y Condiciones Generales es declarada ilegal, o presenta un vacío, o por cualquier razón resulta inaplicable, la misma deberá ser interpretada dentro del marco del mismo y en cualquier caso no afectará la validez y la aplicabilidad de las provisiones restantes.  </span>
                                <br>
                                <br>
                                <b class="scrollflow -opacity">13. <strong style="text-decoration: underline">NOTIFICACIONES </strong></b>
                                <br>
                                <br>
                                <span class="scrollflow -opacity">Cualquier comentario, inquietud o reclamación respecto de los anteriores Términos y Condiciones Generales, o la ejecución de éstos, deberá ser notificada por escrito a LA EMPRESA a la siguiente dirección: Av. Antúnez de Mayolo 891, los Olivos. JURISDICCIÓN Y LEY APLICABLE Este acuerdo estará regido en todos sus puntos por las leyes vigentes en la República del Perú. Cualquier controversia derivada del presente acuerdo, su existencia, validez, interpretación, alcance o cumplimiento, será sometida a los Tribunales competentes de la ciudad de Lima, Perú.</span>
                                <br>
                                <br>
                            </span>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btnId" type="reset" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <!-- Modal -->

        <div class="row">
            <div class="col-md-12" id="01carritoDeCompras" style="padding-left: 0px; padding-bottom: 40px">
                <!--TABLA DETALLE DE COMPRA-->
                <div class="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-xs-12 scrollflow -slide-top" style="padding-top: 30px; z-index: 3">
                    <div id="tablaCompra" class="container">
                        <div class="box-body table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto">
                                <thead class="table-info">
                                    <tr class="text-center">
                                        <th style="width: 150px"></th>
                                        <th>Producto</th>
                                        <th>Precio</th>
                                        <th>Puntos</th>
                                        <th>Cantidad</th>
                                        <th>Subtotal neto</th>
                                        <th>Subtotal puntos</th>
                                        <th>ACCIONES</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <% if (productosCarrito != null)
                                        {
                                            foreach (var productoCarrito in productosCarritoPaquete)
                                            {%>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-7 col-sm-7 col-md-7 center-block">
                                                    <img id="ImgFotoo" src="products/<%=productoCarrito.Foto %>" class="img-responsive" />
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center">

                                            <%=productoCarrito.NombreProducto %>
                                           
                                        </td>
                                        <td data-th="Price" class="text-center">S/. <%=productoCarrito.PrecioUnitario.ToString("N2").Replace(",", ".") %></td>
                                        <td class="text-center">
                                            <%=productoCarrito.Puntos.ToString("N2").Replace(",", ".") %>
                                        </td>
                                        <td data-th="Quantity">
                                            <input type="number" id="<%=productoCarrito.Codigo %>" class="form-control text-center" value="<%=productoCarrito.Cantidad %>">
                                        </td>
                                        <td class="text-center">
                                            <%=productoCarrito.SubTotalNeto.ToString("N2").Replace(",", ".") %>
                                        </td>
                                        <td class="text-center">
                                            <%=productoCarrito.SubTotalPuntos.ToString("N2").Replace(",", ".") %>
                                        </td>
                                        <td class="actions" data-th="">
                                            <a href="#" onclick="ActualizarProducto('<%=productoCarrito.Codigo %>')" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></a>
                                            <%--                                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>--%>
                                            <a href="#" onclick="EliminarProducto('<%=productoCarrito.Codigo %>')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>

                                </tbody>

                                <%}
                                    } %>
                            </table>

                        </div>
                    </div>

                </div>
                <!--DATOS DE COMPRA-->
                <div id="datosDeLaCompra" class="col-xl-3 col-lg-6 col-md-8 col-sm-8 scrollflow -slide-left -opacity" style="padding-top: 10px; z-index: 2">
                    <div id="resumenDeLaCompra">

                        <div id="bloqueDatosDeLaCompra" class="form-group" style="margin-top: -20px">
                            <div class="col-md-12 text-center">
                                <label style="font-size: 17px; font-weight: bolder">DATOS DE LA COMPRA</label>
                            </div>

                            <div class=" row form-group" id="MostrarSoloLogueado" style="display: block;">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra">Tipo de compra: </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <select id="STipoCompra" enableviewstate="true" onchange="MostrarRegistroCliente(this.value)" runat="server" class="form-control btn-lg marginTop">
                                        </select>
                                    </div>
                                </div>
                                <div class="row col-md-12" style="display:none;">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra">Corazones:</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbCorazones" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra">Puntos para Rango:</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbPuntosRango" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra">Puntos de compra:</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbPuntosCompra" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra">Precio total:</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbPrecioTotal" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra">Precio a pagar:</label>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbPrecioPagar" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px; display: none">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra">Tipo de entrega: </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <select id="STipoEntrega" onchange="MostrarComboTiendaAndDatosDelivery(this.value)" runat="server" class="form-control btn-lg marginTop">
                                            <option hidden value="0">Seleccione</option>
                                            <option value="1">Recojo en tienda</option>
                                            <option value="2">Delivery</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="MostrarDatosParaDelivery" style="display: none">
                            </div>
                            <div id="MostrarComboTiena" class="row form-group" style="margin-top: -10px; display: none;">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="Label2" runat="server" CssClass="labelResuCompra">Tienda: </asp:Label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:DropDownList ID="ComboTienda" AutoPostBack="true" OnSelectedIndexChanged="ComboTienda_SelectedIndexChanged" CssClass="form-control btn-lg marginTop" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="Label7" runat="server" CssClass="labelResuCompra">Medio de pago: </asp:Label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:DropDownList AutoPostBack="true" ID="SMedioPago" OnSelectedIndexChanged="SMedioPago_SelectedIndexChanged" CssClass="form-control btn-lg marginTop" runat="server">
                                            <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px; display:none;">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra">Activar promoción: </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:DropDownList AutoPostBack="true" runat="server" ID="myListDropDown" OnSelectedIndexChanged="myListDropDown_SelectedIndexChanged" CssClass="form-control btn-lg marginTop">
                                            <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Activar" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Desactivar" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group" style="margin-top: -10px;" id="DatosComboComprobante" runat="server">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelComprobante">Comprobante: </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:DropDownList AutoPostBack="true" runat="server" ID="cboComprobante" OnSelectedIndexChanged="cboComprobante_SelectedIndexChanged" CssClass="form-control btn-lg marginTop">
                                            <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Boleta" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Factura" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group" style="margin-top: -10px; display:none;" id="MostrarBotonValidar" runat="server">
                                <div class="row col-md-12 lblValidarFacturacion">
                                    <label>VALIDACIÓN PARA FACTURACIÓN</label>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelComprobante">A nombre de: </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:DropDownList AutoPostBack="true" runat="server" ID="ddlTitularRUC" OnSelectedIndexChanged="ddlTitularRUC_SelectedIndexChanged" CssClass="form-control btn-lg marginTop">
                                            <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div runat="server" id="ValidacionRucOP" style="display:none">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelRUC">RUC: </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:TextBox ID="TextBoxRUC" runat="server" CssClass="form-control marginTop txtRUC"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <button ID="ButtonValida" class="btn form-control btnValidar" style="background-color:#2a2929; color:white">Validar</button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                    </div>
                                </div>

                                </div>
                            </div>  

                            <div id="chkCondiciones" class="row form-group" style="display: block; margin-top: -10px" runat="server">
                                <div class="col-12 col-sm-12 col-md-12 col-xs-12">
                                    <asp:CheckBox ID="chkTerminos" runat="server" />
                                    <span style="font-size: 13.5px;">Declaro que he leído y aceptado los <a id="clicTerminosCondiciones" href="#" data-toggle="modal" data-target="#exampleModal">Términos y Condiciones.</a> </span>
                                </div>
                            </div>
                            <div class="row form-group" style="display: none" runat="server" id="datosDelivery">
                                <div class="row col-md-12" style="margin-bottom: 5px">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="labelResuCompra" style="font-weight: bold">Nombre del Destinatario</label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtNombreDatosCompra" title="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="labelResuCompra" style="font-weight: bold">DNI del Destinatario</label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtDNIDatosCompra" title="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="labelResuCompra" style="font-weight: bold">Celular del Destinatario</label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtCelularDatosCompra" title="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row col-md-12" style="margin-bottom: 5px">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="labelResuCompra" style="font-weight: bold">Dirección del Destinatario</label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtDireccionDatosCompra" title="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row col-md-12" style="margin-bottom: 5px">
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                        <label class="labelResuCompra" style="font-weight: bold">SERVICIO DE DELIVERY A PROVINCIAS</label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="labelResuCompra" style="font-weight: bold">*Es responsabilidad del socio verificar previamente que la empresa de transporte trabaje con Pago a Destino.</label>
                                    </div>
                                </div>
                                <div class="row col-md-12" style="margin-bottom: 5px">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="labelResuCompra" style="font-weight: bold">Nombre de la Empresa de transporte</label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtTransporteDatosCompra" title="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row col-md-12" style="margin-bottom: 5px">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="labelResuCompra" style="font-weight: bold">Direccion de la Empresa de transporte-Lima</label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtDirecTransporteDatosCompra" title="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row col-md-12" style="margin-bottom: 5px">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="labelResuCompra" style="font-weight: bold">Provincia destino</label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtProvinciaDatosCompra" title="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row col-md-12" style="margin-bottom: 5px">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="labelResuCompra" style="font-weight: bold">Direccion de la Empresa de transporte-Provincia(Destino)</label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:TextBox ID="txtDirecProvinciaDatosCompra" title="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="btnRegistrarAfiliado" class="row col-md-12" runat="server">
                            <div class="col-sm-8 col-sm-offset-2">
                                <a class="btn btn-success form-control tablaSiguiente" runat="server" style="font-size: 12px" href="javascript:void('');">Siguiente <span class="glyphicon glyphicon-arrow-right"></span></a>
                            </div>
                        </div>
                        <div class="row col-md-12" id="CompraNormal" runat="server">
                            <div class="col-sm-8 col-sm-offset-2">
                                <asp:Button ID="BntComprar" OnClientClick="return Comprar()" OnClick="BntComprar_Click" CssClass="btn btn-success form-control" runat="server" Text="Comprar" />
                            </div>
                        </div>
                        <div class="row col-md-12" id="CompraPE" runat="server">
                            <div class="col-sm-8 col-sm-offset-2">
                                <asp:Button ID="btnCompraPE" OnClientClick="return ComprarPE()" OnClick="btnCompraPE_Click" BackColor="#FFCC00" ForeColor="#000000" Font-Bold="true" Font-Size="Smaller" CssClass="btn form-control" runat="server" Text="COMPRAR" />
                            </div>
                        </div>
                        <div id="botonVisa" class="row col-md-12" style="display: block;" runat="server">
                            <div class="col-sm-8 col-sm-offset-2">
                                <button class="btn-visa-upload form-control" type="button" runat="server" id="btnV" onclick="Delivery();" onserverclick="pagarOrden">
                                    PAGA CON &nbsp;&nbsp;
                            <img id="logovisa" src="img/credit/visa_logo.png" style="width: 45px; margin-top: -4px;" /></button>
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div id="btn_pago">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="02afiliacion">
                <div style="width: 165px; padding-bottom: 20px">
                    <a class="btn btn-danger form-control retroceder 01" style="font-size: 13px" href="javascript:void('01');"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp RETROCEDER</a>
                </div>
                <!--REGISTRO AFILIACION-->
                <div id="MostrarRegistroCliente" class="col-md-8">
                    <div class="row form-group colorlib-form">
                        <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto">
                            <h1>REGISTRO DE AFILIACIÓN</h1>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 16px">DATOS DE LA CUENTA</label>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Usuario</label>
                                    <asp:TextBox ID="txtUl" title="Se necesita un nombre de Usuario" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Clave</label>
                                    <asp:TextBox ID="TxtCl" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity" id="btnCargaImagen" runat="server">
                                    <label class="label" style="font-weight: bold">Foto de Perfil</label>
                                    <label class="file-upload btn btn-success form-control marginTop" style="font-size: 15px">
                                        Ingresa tu foto
                                    <input type="file" class="imagen form-control" id="imagen" name="MiImagen" accept="image/x-png,image/jpeg" runat="server" style="margin-top: 4px" />
                                    </label>
                                </div>
                            </div>

                            <div class="row col-md-12" id="imagenMostrada" runat="server">
                                <div class="form-group col-md-4 scrollflow -opacity" style="margin-left: auto; margin-right: auto">
                                    <label>Mi foto de perfil</label>
                                    <div id="imagePreview" class="center-block align-content-center">
                                        <img src="img/usuario1.png" class="img-fluid" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="margin-top: 13px">
                            <label style="font-weight: bold; font-size: 16px">Datos Personales</label>


                            <div class="row col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">UpLine</label>
                                    <asp:DropDownList ID="CboUpLine" runat="server" CssClass="form-control btn-lg marginTop" />

                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Tipo de cliente</label>
                                    <asp:DropDownList ID="cboTipoCliente" runat="server" CssClass="form-control btn-lg marginTop" />
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">CDR Preferido</label>
                                    <asp:DropDownList ID="cboTipoEstablecimiento" runat="server" CssClass="form-control btn-lg marginTop" />
                                </div>
                            </div>

                            <div class="row col-md-12">
                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Nombres</label>
                                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Ap. Paterno</label>
                                    <asp:TextBox ID="txtApPaterno" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Ap. Materno</label>
                                    <asp:TextBox ID="txtApMaterno" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Patrocinador</label>
                                    <asp:TextBox ID="txtPatrocinador" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row col-md-12">

                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Nacimiento</label>
                                    <input type="text" id="datepicker" class="form-control text-uppercase marginTop" readonly runat="server" />
                                    <asp:HiddenField ID="FechaNaci" runat="server" />
                                    <asp:HiddenField ID="DivTipCompraAndPuntos" runat="server" />
                                </div>

                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Sexo</label>
                                    <asp:DropDownList ID="ComboSexo" runat="server" CssClass="form-control marginTop">
                                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                                        <asp:ListItem Value="1">MASCULINO</asp:ListItem>
                                        <asp:ListItem Value="2">FEMENINO</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Tipo documento</label>
                                    <asp:DropDownList ID="ComboTipoDocumento" runat="server" CssClass="form-control marginTop">
                                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                                        <asp:ListItem Value="1">DNI</asp:ListItem>
                                        <asp:ListItem Value="2">PASAPORTE</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">N° Documento</label>
                                    <asp:TextBox ID="txtNumDocumento" runat="server" CssClass="form-control text-uppercase marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>

                            </div>

                            <div class="row col-md-12">

                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Correo electronico</label>
                                    <asp:TextBox ID="TxtCorreo" runat="server" CssClass="form-control text-uppercase marginTop" TextMode="Email"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Teléfono</label>
                                    <asp:TextBox ID="TxtTelefono" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Celular</label>
                                    <asp:TextBox ID="TxtCelular" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="row col-md-12">
                                        <div class="form-group col-md-4 scrollflow -opacity">
                                            <label class="label" style="font-weight: bold">Pais de operaciones</label>
                                            <asp:DropDownList ID="cboPais" runat="server" AutoPostBack="True" OnSelectedIndexChanged="PaisSeleccionado" CssClass="form-control marginTop" />
                                        </div>
                                        <div class="form-group col-md-4 scrollflow -opacity">
                                            <label class="label" style="font-weight: bold">Dirección</label>
                                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-md-4 scrollflow -opacity">
                                            <label class="label" style="font-weight: bold">Referencia</label>
                                            <asp:TextBox ID="TxtReferencia" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="row col-md-12">
                                        <div class="form-group col-md-4 scrollflow -opacity">
                                            <label class="label" style="font-weight: bold">Departamento</label>
                                            <asp:DropDownList ID="cboDepartamento" AutoPostBack="True" OnSelectedIndexChanged="DepartamentoSeleccionado" runat="server" CssClass="form-control marginTop" />
                                        </div>
                                        <div class="form-group col-md-4 scrollflow -opacity">
                                            <label class="label" style="font-weight: bold">Provincia</label>
                                            <asp:DropDownList ID="cboProvincia" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ProvinciaSeleccionada" CssClass="form-control marginTop" />
                                        </div>
                                        <div class="form-group col-md-4 scrollflow -opacity">
                                            <label class="label" style="font-weight: bold">Distrito</label>
                                            <asp:DropDownList ID="cboDistrito" AutoPostBack="True" runat="server" CssClass="form-control marginTop" />
                                            <br />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="row col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">CDR Premio</label>
                                    <asp:DropDownList ID="cboPremio" runat="server" CssClass="form-control btn-lg marginTop" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label style="font-weight: bold; font-size: 16px">DATOS BANCARIOS</label>


                            <div class="row col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">RUC</label>
                                    <asp:TextBox ID="TxtRUC" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">Banco</label>
                                    <asp:TextBox ID="TxtBanco" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">N° Cuenta depósito</label>
                                    <asp:TextBox ID="TxtNumCuenDeposito" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row col-md-12">
                                <div class="form-group col-md-6 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">N° Cuenta detracciones</label>
                                    <asp:TextBox ID="TxtNumCuenDetracciones" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold">N° Cuenta interbancaria</label>
                                    <asp:TextBox ID="TxtNumCuenInterbancaria" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--RESUMEN FINAL DE LA COMPRA-->
                <div class="col-md-4">
                    <div id="resumenDeLaCompra2">

                        <div class="form-group colorlib-form">
                            <div class="col-md-12 text-center">
                                <label style="font-size: 17px; font-weight: bolder">RESUMEN DE LA COMPRA</label>
                            </div>


                            <div class=" row form-group" id="MostrarSoloLogueado2" style="display: block;">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra">Tipo de compra: </label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra"><%=tipoCompraPublica %></label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra">Puntos para Rango:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label><%=puntosRangoCompraPublica %> pts.</label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra">Puntos de compra:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label><%=puntosCompraPublica %> pts.</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra">Precio total:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label id="lblPrecioTotal2">S/. <%=totalCompraPublica %></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra">Precio a pagar:</label>
                                    </div>

                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label id="lblPrecioPagar2">S/. <%=pagoCompraPublica %></label>
                                    </div>
                                </div>
                            </div>

                            <div id="MostrarDatosParaDelivery2" style="display: none">
                            </div>
                            <div id="MostrarComboTienda2" class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra">Tienda:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra"><%=tiendaCompraPublica %></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra">Medio de pago:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra"><%=medioPCompraPublica %></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br />
                        <div class="row col-md-12" id="chkCondiciones2" runat="server">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-7">
                                <asp:CheckBox ID="chkTerminos2" runat="server" />
                                <span style="font-size: 13.5px;">Declaro que he leído y aceptado los <a href="#" data-toggle="modal" data-target="#exampleModal">Términos y Condiciones.</a> </span>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                        <br />

                        <div class="row col-md-12" id="CompraNormal2" runat="server">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-7">
                                <asp:Button ID="btnCompra2" OnClientClick="return Comprar2()" OnClick="btnCompra2_Click" CssClass="btn btn-success form-control" runat="server" Text="Comprar" />
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                        <div class="row col-md-12" id="CompraPE2" runat="server">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-7">
                                <asp:Button ID="btnCompraPE2" OnClientClick="return ComprarPE2()" style="" OnClick="btnCompraPE2_Click" BackColor="#FFCC00" ForeColor="#000000" Font-Bold="true" Font-Size="Smaller" CssClass="btn form-control" runat="server" Text="Comprar" />
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                        <div class="row col-md-12" id="botonVisa2" runat="server">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-7">
                                <button class="btn-visa-upload form-control" type="button" runat="server" onserverclick="pagarOrden2">
                                    PAGA CON &nbsp;&nbsp;
                            <img id="logovisa2" src="img/credit/visa_logo.png" style="width: 45px; margin-top: -4px;" /></button>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-7">
                                <div id="btn_pago2">
                                </div>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12" id="03ordenCompletada">
                <div class="row" id="complet" runat="server">
                    <div class="col-md-10 col-md-offset-1 text-center">
                        <img src="img/shoppingcart_accept_compra_12832.png" />
                        <br />
                        <br />
                        <br />
                        <h2>Gracias por comprar, su pedido está realizado</h2>
                        <br />
                        <p>
                            <a href="Index.aspx" class="btn btn-primary" style="width: 60px; height: 30px">Inicio</a>
                            <a href="TiendaSN.aspx" class="btn btn-primary btn-outline" style="height: 30px">Seguir comprando</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <br />

    </div>
    <asp:HiddenField ID="PuntosEmprendedor" runat="server" />
    <asp:HiddenField ID="PuntosEmpresarial" runat="server" />
    <asp:HiddenField ID="PuntosProfesional" runat="server" />
    <asp:HiddenField ID="PuntosMillonario" runat="server" />
    <asp:HiddenField ID="CompraDeTodosModos" runat="server" />

    <asp:HiddenField ID="NombreDelivery" runat="server" />
    <asp:HiddenField ID="DNIDelivery" runat="server" />
    <asp:HiddenField ID="CelularDelivery" runat="server" />
    <asp:HiddenField ID="DireccionDelivery" runat="server" />
    <asp:HiddenField ID="TransporteDelivery" runat="server" />
    <asp:HiddenField ID="DirecTransporteDelivery" runat="server" />
    <asp:HiddenField ID="ProvinciaDelivery" runat="server" />
    <asp:HiddenField ID="DirecProvinciaDelivery" runat="server" />

    <asp:HiddenField ID="Nombress" runat="server" />
    <asp:HiddenField ID="ApPaterno" runat="server" />
    <asp:HiddenField ID="ApMaterno" runat="server" />
    <asp:HiddenField ID="TipoDocumento" runat="server" />
    <asp:HiddenField ID="NumDocumento" runat="server" />
    <asp:HiddenField ID="Telefono" runat="server" />
    <asp:HiddenField ID="Direccion" runat="server" />
    <asp:HiddenField ID="Usuario" runat="server" />
    <asp:HiddenField ID="Clave" runat="server" />
    <asp:HiddenField ID="UpLine" runat="server" />
    <asp:HiddenField ID="Patrocinador" runat="server" />

    <asp:HiddenField ID="Correo" runat="server" />
    <asp:HiddenField ID="Celular" runat="server" />
    <asp:HiddenField ID="Referencia" runat="server" />
    <asp:HiddenField ID="RUC" runat="server" />
    <asp:HiddenField ID="Banco" runat="server" />
    <asp:HiddenField ID="NumCuenDeposio" runat="server" />
    <asp:HiddenField ID="NumCuenDetrac" runat="server" />
    <asp:HiddenField ID="NumCuenInterbancaria" runat="server" />
    <asp:HiddenField ID="Provincia" runat="server" />
    <asp:HiddenField ID="Departamento" runat="server" />
    <asp:HiddenField ID="Pais" runat="server" />
    <asp:HiddenField ID="Distrito" runat="server" />
    <asp:HiddenField ID="Sexo" runat="server" />

    <asp:HiddenField ID="TipoCliente" runat="server" />
    <asp:HiddenField ID="TipoEstablecimiento" runat="server" />
    <asp:HiddenField ID="CDRPremio" runat="server" />
    <asp:HiddenField ID="Foto" runat="server" />

    <script src="js/48531.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <script src="js/carritoDeCompra5.js?v2"></script>

    <script src="js/file-uploadv1.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            var completada = "<%=Session["mostrarCompraTerminada"]%>";

            if (completada == 0) {
                $("#02afiliacion").hide();
                $("#03ordenCompletada").hide();
            }

            /*RETROCEDER DEL PASO 2 AL PASO 1*/
            $(".01").click(function (e) {
                $("#tablaCompra").fadeIn(300);
                $("#resumenDeLaCompra").fadeIn(300);
                $("#02afiliacion").slideUp(0);
                var elemento = document.getElementById("circle02");
                elemento.classList.remove('active');
                //$("#resumenDeLaCompra2").slideUp(0);
            });

            /*IR DEL PASO 2 AL PASO 3*/
            $(".registrarPedido").click(function (e) {
                $("#03ordenCompletada").fadeIn(300);
                $("#02afiliacion").slideUp(0);
                $("01carritoDeCompras").slideUp(0);
                var ordenCompletada = document.getElementById("ordenCompletada");
                var elemento = document.getElementById("circle03");
                elemento.className += " active"
                ordenCompletada.className += " active"
                //$("#resumenDeLaCompra2").slideUp(0);
            });

            /**/
            $(".02").click(function (e) {
                $("#02afiliacion").fadeIn(300);
                $("#03ordenCompletada").slideUp(0);
                var elemento = document.getElementById("circle03");
                elemento.classList.remove('active');
                //$("#resumenDeLaCompra2").slideUp(0);
            });

            $('.tablaSiguiente').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 100);
                return false;
            });

            var tipoComS = $("#STipoCompra").val();

            if (tipoComS == "07" | tipoComS == "08" | tipoComS == "09" | tipoComS == "10" |
                tipoComS == "11" | tipoComS == "12" | tipoComS == "13") {
                $("#circle02").hide();
                $("#circulito").text("02");
            } else {
                $("#circle02").show();
                $("#circulito").text("03");
            }



        });

    </script>
    <script>
        function ErrorPaqueteProducto() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Le pedimos las disculpas del caso, ocurrió un error en el producto en promoción, porfavor vuelva a realizar la compra y verifique los puntos y el monto a pagar',
            })
        }
        function ErrorRUCPerfil() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'El RUC de su perfil es incorrecto',
            })
        }
        function ErrorServicioSunat() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Le pedimos las disculpas del caso, el servicio de validación de RUC de SUNAT está presentando demoras, vuelva a intentarlo en 15 minutos',
            })
        }

        function IngreseRUCValido() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'El RUC ingresado es inválido',
            })
        }

         function FaltaRUCPerfil() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'El RUC de su perfil se encuentra vacio',
            })
        }

        function FaltaComprobante() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Seleccione tipo de comprobante',
            })
        }

        function FaltaTitular() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Seleccione a nombre de que persona se emitirá la factura',
            })
        }

        function FaltaValidarRUC() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'El RUC ingresado debe ser válido',
            })
        }

        function Delivery() {
            var nombreD = $("#txtNombreDatosCompra").val();
            var DNID = $("#txtDNIDatosCompra").val();
            var CelularD = $("#txtCelularDatosCompra").val();
            var DireccionD = $("#txtDireccionDatosCompra").val();
            var NomTransporteD = $("#txtTransporteDatosCompra").val();
            var DirecTransporteD = $("#txtDirecTransporteDatosCompra").val();
            var ProvinciaD = $("#txtProvinciaDatosCompra").val();
            var DirecProvinciaD = $("#txtDirecProvinciaDatosCompra").val();
            var NombreS = document.getElementById('<%=NombreDelivery.ClientID%>');
            var DNIS = document.getElementById('<%=DNIDelivery.ClientID%>');
            var CelularS = document.getElementById('<%=CelularDelivery.ClientID%>');
            var DireccionS = document.getElementById('<%=DireccionDelivery.ClientID%>');
            var NomTransporteS = document.getElementById('<%=TransporteDelivery.ClientID%>');
            var DirecTransporteS = document.getElementById('<%=DirecTransporteDelivery.ClientID%>');
            var ProvinciaS = document.getElementById('<%=ProvinciaDelivery.ClientID%>');
            var DirecProvinciaS = document.getElementById('<%=DirecProvinciaDelivery.ClientID%>');
            NombreS.value = nombreD;
            DNIS.value = DNID;
            CelularS.value = CelularD;
            DireccionS.value = DireccionD;
            NomTransporteS.value = NomTransporteD;
            DirecTransporteS.value = DirecTransporteD;
            ProvinciaS.value = ProvinciaD;
            DirecProvinciaS.value = DirecProvinciaD;
            
        }

        function ComprarPE() {
            var comprar = true;
            GuardarDatosDelivery();
            var esVisibleTipoCompraAndPuntos = $("#MostrarSoloLogueado").is(":visible");
            var fechaNacimiento = document.getElementById('<%= FechaNaci.ClientID %>');
            fechaNacimiento.value = $("[id$=datepicker]").val();
            var fechaNacFormateada = FormatearFecha(fechaNacimiento.value);
            fechaNacimiento.value = fechaNacFormateada;

            var divTipCompraAndPuntos = document.getElementById('<%= DivTipCompraAndPuntos.ClientID %>');
            divTipCompraAndPuntos.value = true;

            var comprarDeTodosModos = true;
            CompraDeTodosModos.value = "SI";

            if (comprar == true) {
                comprarDeTodosModos.value = "SI";
            }
            return comprar;
        }

        function ComprarPE2() {
            var comprar = true;
            var esVisibleTipoCompraAndPuntos = $("#MostrarSoloLogueado").is(":visible");
            var fechaNacimiento = document.getElementById('<%= FechaNaci.ClientID %>');
            fechaNacimiento.value = $("[id$=datepicker]").val();
            var fechaNacFormateada = FormatearFecha(fechaNacimiento.value);
            fechaNacimiento.value = fechaNacFormateada;

            var divTipCompraAndPuntos = document.getElementById('<%= DivTipCompraAndPuntos.ClientID %>');
            divTipCompraAndPuntos.value = true;

            var comprarDeTodosModos = true;
            CompraDeTodosModos.value = "SI";

            if (comprar == true) {
                comprarDeTodosModos.value = "SI";
            }
            return comprar;
        }

        $("[id*=ComboDepart] option[value='0']").hide()
        $("[id*=ComboProvincia] option[value='0']").hide()
        $("[id*=ComboDistrito] option[value='0']").hide()
        $("[id*=ComboTienda] option[value='0']").hide()
        $("[id*=STipoCompra] option[value='']").hide()
        $("[id*=myListDropDown] option[value='0']").hide()

        function abrirModalPasarela() {
            var correo = "<%= Session["Correo"] %>";
            var nombres = "<%= Session["Nombres"] %>";
            var apellidos = "<%= Session["Apellidos_Full"] %>";
            var monto = "<%=Session["MontoAPagar"]%>";
            var tokenID = "<%= Session["tokenVISA"] %>";
            var sessionID = "<%= Session["SessiontokenVISA"] %>";
            var idPagoCliente = "<%= Session["IdPagoCliente"] %>";
            pagar(correo, nombres, apellidos, monto, tokenID, sessionID, idPagoCliente);
        }

        function abrirModalPasarela2() {
            var correo = "<%= Session["Correo"] %>";
            var nombres = "<%= Session["Nombres"] %>";
            var apellidos = "<%= Session["Apellidos_Full"] %>";
            var monto = "<%=Session["MontoAPagar"]%>";
            var tokenID = "<%= Session["tokenVISA"] %>";
            var sessionID = "<%= Session["SessiontokenVISA"] %>";
            var idPagoCliente = "<%= Session["IdPagoCliente"] %>";
            pagar2(correo, nombres, apellidos, monto, tokenID, sessionID, idPagoCliente);
        }


        function IrCompraTerminada() {
            var ordenCompletada = document.getElementById("ordenCompletada");
            var elemento = document.getElementById("circle03");
            $("#03ordenCompletada").fadeIn(300);
            $("#02afiliacion").slideUp(0);
            $("#01carritoDeCompras").slideUp(0);
            $("#resumenDeLaCompra").slideUp(0);
            $("#tablaCompra").slideUp(0);
            elemento.className += " active"
            ordenCompletada.className += " active"
        }

        function formatDate(date) {
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + '/' + monthIndex + '/' + year;
        }

        function FormatearFecha(fecha) {
            var ms = Date.parse(fecha);
            var fecha = new Date(ms);

            var dd = fecha.getDate();
            var mm = fecha.getMonth() + 1;
            var yyyy = fecha.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            return dd + '/' + mm + '/' + yyyy;
        }

        function Comprar2() {
            var comprar = true;

            var esVisibleTipoCompraAndPuntos = $("#MostrarSoloLogueado").is(":visible");
            var fechaNacimiento = document.getElementById('<%= FechaNaci.ClientID %>');
            fechaNacimiento.value = $("[id$=datepicker]").val();
            var fechaNacFormateada = FormatearFecha(fechaNacimiento.value);
            fechaNacimiento.value = fechaNacFormateada;


            var divTipCompraAndPuntos = document.getElementById('<%= DivTipCompraAndPuntos.ClientID %>');
            divTipCompraAndPuntos.value = true;

            //Validar formulario      
            var usu = $("[id$=txtUl]").val();
            var cla = $("[id$=TxtCl]").val();
            var nombre = $("[id$=txtNombre]").val();
            var apPaterno = $("[id$=txtApPaterno]").val();
            var apMaterno = $("[id$=txtApMaterno]").val();
            var tipoDocumento = $("[id$=ComboTipoDocumento]").val();
            var numDocumento = $("[id$=txtNumDocumento]").val();
            var usuario = $("[id$=TxtUsuario]").val();
            var fechaNaci = $("[id$=datepicker]").val();
            var clave = $("[id$=TxtClave]").val();
            var correo = $("[id$=txtCorreo]").val();
            var celular = $("[id$=TxtCelular]").val();

            var comboTipoCliente = $("[id$=cboTipoCliente]").val();
            var comboSexo = $("[id$=ComboSexo]").val();

            var comprarDeTodosModos = true;
            CompraDeTodosModos.value = "SI";
            var puntosTotal = $("[id$=LbPuntosCompra]").html();
            var puntosTot = parseFloat(puntosTotal);

            var montoNeto = $("[id$=LbPrecioPagar]").html();
            var formatMontoNeto = montoNeto.replace("S/.", "");
            var montoNetito = parseFloat(formatMontoNeto);

            var tipoCompra = $("[id$=STipoCompra]").val();

            //var puntosProfesional = parseInt(puntosProfesional.value, 10);
            if (tipoCompra == "") {
                compra = true;
            } else if (tipoCompra == "07") {
                compra = true;
            } else if (tipoCompra == "01" || tipoCompra == "02" || tipoCompra == "03" || tipoCompra == "04" || tipoCompra == "05" || tipoCompra == "06") {
                if (usuario == "") {
                    compra = false;
                } else if (clave == "") {
                    compra = false;
                } else if (comboTipoCliente == "") {
                    compra = false;
                } else if (nombre == "") {
                    compra = false;
                } else if (apPaterno == "") {
                    compra = false;
                } else if (apMaterno == "") {
                    compra = false;
                } else if (fechaNaci == "") {
                    compra = false;
                } else if (comboSexo == "") {
                    compra = false;
                } else if (tipoDocumento == "") {
                    compra = false;
                } else if (numDocumento == "") {
                    compra = false;
                } else if (correo == "") {
                    compra = false;
                } else if (celular == "") {
                    compra = false;
                } else if (usu == "") {
                    compra = false;
                } else if (cla == "") {
                    compra = false;
                }
                else if (tipoCompra == "01") { //Emprendedor
                    if (puntosTot < emprendedorPuntos) {
                        AlertPuntosInsuficientesEmprendedor();
                        comprar = false;
                    } else if (puntosTot >= emprendedorPuntos && puntosTot < profesionalPuntos) {
                        comprar = true;
                    } else if (puntosTot >= profesionalPuntos && puntosTot < empresarialPuntos) {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Profesional. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    } else if (puntosTot >= empresarialPuntos && puntosTot < millonarioPuntos) {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Empresarial. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    } else /*(puntosTot >= millonarioPuntos) */ {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Millonario. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    }
                } else if (tipoCompra == "02") { //Profesional
                    if (puntosTot < profesionalPuntos) {
                        AlertPuntosInsuficientesProfesional();
                        comprar = false;
                    } else if (puntosTot >= profesionalPuntos && puntosTot < empresarialPuntos) {
                        comprar = true;
                    } else if (puntosTot >= empresarialPuntos && puntosTot < millonarioPuntos) {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Empresarial. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    } else /*(puntosTot >= millonarioPuntos) */ {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Millonario. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    }
                } else if (tipoCompra == "03") { //Empresarial
                    if (puntosTot < empresarialPuntos) {
                        AlertPuntosInsuficientesEmpresarial();
                        comprar = false;
                    } else if (puntosTot >= empresarialPuntos && puntosTot < millonarioPuntos) {
                        comprar = true;
                    } else /*(puntosTot >= millonarioPuntos)*/ {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Millonario. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    }
                } else if (tipoCompra == "04") { //Millonario

                } else if (tipoCompra == "05") { //Consultor

                } else { //C. Inteligente

                }
            } else if (tipoCompra == "08" || tipoCompra == "09" || tipoCompra == "10" || tipoCompra == "11" || tipoCompra == "12" || tipoCompra == "13") { //UPGRADE

            } else { //MIGRACIÓN - VALIDACIÓN PUNTOS

            }
            if (comprar == true) {
                comprarDeTodosModos.value = "SI";
            }
            return comprar;
        }
        

        function Comprar() {
            GuardarDatosDelivery();
            var comprar = true;

            var esVisibleTipoCompraAndPuntos = $("#MostrarSoloLogueado").is(":visible");
            var fechaNacimiento = document.getElementById('<%= FechaNaci.ClientID %>');
            fechaNacimiento.value = $("[id$=datepicker]").val();
            var fechaNacFormateada = FormatearFecha(fechaNacimiento.value);
            fechaNacimiento.value = fechaNacFormateada;


            var divTipCompraAndPuntos = document.getElementById('<%= DivTipCompraAndPuntos.ClientID %>');
            divTipCompraAndPuntos.value = esVisibleTipoCompraAndPuntos;

            //Validar formulario      
            var usu = $("[id$=txtUl]").val();
            var cla = $("[id$=TxtCl]").val();
            var nombre = $("[id$=txtNombre]").val();
            var apPaterno = $("[id$=txtApPaterno]").val();
            var apMaterno = $("[id$=txtApMaterno]").val();
            var tipoDocumento = $("[id$=ComboTipoDocumento]").val();
            var numDocumento = $("[id$=txtNumDocumento]").val();
            var usuario = $("[id$=TxtUsuario]").val();
            var fechaNaci = $("[id$=datepicker]").val();
            var clave = $("[id$=TxtClave]").val();
            var correo = $("[id$=txtCorreo]").val();
            var celular = $("[id$=TxtCelular]").val();

            var comboTipoCliente = $("[id$=cboTipoCliente]").val();
            var comboSexo = $("[id$=ComboSexo]").val();

            var comprarDeTodosModos = document.getElementById('<%= CompraDeTodosModos.ClientID %>');
            var puntosTotal = $("[id$=LbPuntosCompra]").html();
            var puntosTot = parseFloat(puntosTotal);

            var montoNeto = $("[id$=LbPrecioPagar]").html();
            var formatMontoNeto = montoNeto.replace("S/.", "");
            var montoNetito = parseFloat(formatMontoNeto);

            var emprendedorPuntos = <%=emprendedorPuntos%>;
            var profesionalPuntos = <%=profesionalPuntos%>;
            var empresarialPuntos = <%=empresarialPuntos%>;
            var millonarioPuntos = <%=millonarioPuntos%>;
            var consultorMontoNetoMinimo = <%=consultorMontoNetoMinimo%>;
            var clieInteligenteMontoNetoMinimo = <%=clieInteligenteMontoNetoMinimo%>;

            var UPG_P1aP2 = <%=UPG_P1aP2%>;
            var UPG_P1aP3 = <%=UPG_P1aP3%>;
            var UPG_P1aP4 = <%=UPG_P1aP4%>;
            var UPG_P2aP3 = <%=UPG_P2aP3%>;
            var UPG_P2aP4 = <%=UPG_P2aP4%>;
            var UPG_P3aP4 = <%=UPG_P3aP4%>;

            var MIG_P1aP3 = <%=MIG_P1aP3%>;
            var MIG_P1aP4 = <%=MIG_P1aP4%>;
            var MIG_P1aP4_B = <%=MIG_P1aP4_B%>;
            var MIG_P2aP3 = <%=MIG_P2aP3%>;
            var MIG_P2aP4 = <%=MIG_P2aP4%>;
            var MIG_P2aP4_B = <%=MIG_P2aP4_B%>;
            var MIG_P3aP4 = <%=MIG_P3aP4%>;
            var MIG_P3aP4_B = <%=MIG_P3aP4_B%>;

            var tipoCompra = $("[id$=STipoCompra]").val();

            //var puntosProfesional = parseInt(puntosProfesional.value, 10);
            if (tipoCompra == "") {
                compra = true;
            } else if (tipoCompra == "07") {
                compra = true;
            } else if (tipoCompra == "01" || tipoCompra == "02" || tipoCompra == "03" || tipoCompra == "04" || tipoCompra == "05" || tipoCompra == "06") {
                if (usuario == "") {
                    compra = false;
                } else if (clave == "") {
                    compra = false;
                } else if (comboTipoCliente == "") {
                    compra = false;
                } else if (nombre == "") {
                    compra = false;
                } else if (apPaterno == "") {
                    compra = false;
                } else if (apMaterno == "") {
                    compra = false;
                } else if (fechaNaci == "") {
                    compra = false;
                } else if (comboSexo == "") {
                    compra = false;
                } else if (tipoDocumento == "") {
                    compra = false;
                } else if (numDocumento == "") {
                    compra = false;
                } else if (correo == "") {
                    compra = false;
                } else if (celular == "") {
                    compra = false;
                } else if (usu == "") {
                    compra = false;
                } else if (cla == "") {
                    compra = false;
                }
                else if (tipoCompra == "01") { //Emprendedor
                    if (puntosTot < emprendedorPuntos) {
                        AlertPuntosInsuficientesEmprendedor();
                        comprar = false;
                    } else if (puntosTot >= emprendedorPuntos && puntosTot < profesionalPuntos) {
                        comprar = true;
                    } else if (puntosTot >= profesionalPuntos && puntosTot < empresarialPuntos) {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Profesional. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    } else if (puntosTot >= empresarialPuntos && puntosTot < millonarioPuntos) {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Empresarial. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    } else /*(puntosTot >= millonarioPuntos) */ {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Millonario. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    }
                } else if (tipoCompra == "02") { //Profesional
                    if (puntosTot < profesionalPuntos) {
                        AlertPuntosInsuficientesProfesional();
                        comprar = false;
                    } else if (puntosTot >= profesionalPuntos && puntosTot < empresarialPuntos) {
                        comprar = true;
                    } else if (puntosTot >= empresarialPuntos && puntosTot < millonarioPuntos) {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Empresarial. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    } else /*(puntosTot >= millonarioPuntos) */ {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Millonario. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    }
                } else if (tipoCompra == "03") { //Empresarial
                    if (puntosTot < empresarialPuntos) {
                        AlertPuntosInsuficientesEmpresarial();
                        comprar = false;
                    } else if (puntosTot >= empresarialPuntos && puntosTot < millonarioPuntos) {
                        comprar = true;
                    } else /*(puntosTot >= millonarioPuntos)*/ {
                        var bool = confirm("Con esta cantidad de puntos puedes hacer una afiliación Millonario. ¿Quiere continuar con la afiliación?");
                        if (bool) {
                            alert("Usted aceptó la afiliación");
                            comprar = true;
                        } else {
                            alert("Usted canceló la afiliación. Puede cambiar el tipo de afiliación");
                            comprar = false;
                        }
                    }
                } else if (tipoCompra == "04") { //Millonario

                } else if (tipoCompra == "05") { //Consultor

                } else { //C. Inteligente

                }
            } else if (tipoCompra == "08" || tipoCompra == "09" || tipoCompra == "10" || tipoCompra == "11" || tipoCompra == "12" || tipoCompra == "13") { //UPGRADE

            } else { //MIGRACIÓN - VALIDACIÓN PUNTOS

            }
            if (comprar == true) {
                comprarDeTodosModos.value = "SI";
            }
            return comprar;
        }

        function toDate(dateStr) {
            var parts = dateStr.split("/")
            return new Date(parts[2], parts[1] - 1, parts[0])
        }


        function AlertPuntosInsuficientesMIG_P1aP3() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta Migración tiene que hacer una compra mínima de ' +<%=MIG_P1aP3%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesMIG_P1aP4() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta Migración tiene que hacer una compra mínima de ' +<%=MIG_P1aP4%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesMIG_P1aP4_B() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta Migración tiene que hacer una compra mínima de ' +<%=MIG_P1aP4_B%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesMIG_P2aP3() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta Migración tiene que hacer una compra mínima de ' +<%=MIG_P2aP3%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesMIG_P2aP4() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta Migración tiene que hacer una compra mínima de ' +<%=MIG_P2aP4%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesMIG_P2aP4_B() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta Migración tiene que hacer una compra mínima de ' +<%=MIG_P2aP4_B%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesMIG_P3aP4() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta Migración tiene que hacer una compra mínima de ' +<%=MIG_P3aP4%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesMIG_P3aP4_B() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta Migración tiene que hacer una compra mínima de ' +<%=MIG_P3aP4_B%>+' puntos',
            })
        }


        function AlertPuntosInsuficientesUPG_P1aP2() { ///
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar este Upgrade tiene que hacer una compra mínima de ' +<%=UPG_P1aP2%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesUPG_P1aP3() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar este Upgrade tiene que hacer una compra mínima de ' +<%=UPG_P1aP3%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesUPG_P1aP4() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar este Upgrade tiene que hacer una compra mínima de ' +<%=UPG_P1aP4%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesUPG_P2aP3() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar este Upgrade tiene que hacer una compra mínima de ' +<%=UPG_P2aP3%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesUPG_P2aP4() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar este Upgrade tiene que hacer una compra mínima de ' +<%=UPG_P2aP4%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesUPG_P3aP4() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar este Upgrade tiene que hacer una compra mínima de ' +<%=UPG_P3aP4%>+' puntos',
            })
        }

        function AlertMontoNetoInsuficienteConsultor() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta afiliación tiene que hacer una compra mínima de ' +<%=consultorMontoNetoMinimo%>+' soles',
            })
        }

        function AlertMontoNetoInsuficienteCInteligente() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta afiliación tiene que hacer una compra mínima de ' +<%=clieInteligenteMontoNetoMinimo%>+' soles',
            })
        }

        function AlertPuntosInsuficientesEmprendedor() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta afiliación tiene que hacer una compra mínima de ' +<%=emprendedorPuntos%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesProfesional() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta afiliación tiene que hacer una compra mínima de ' +<%=profesionalPuntos%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesEmpresarial() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta afiliación tiene que hacer una compra mínima de ' +<%=empresarialPuntos%>+' puntos',
            })
        }

        function AlertPuntosInsuficientesMillonario() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Para realizar esta afiliación tiene que hacer una compra mínima de ' +<%=millonarioPuntos%>+' puntos',
            })
        }

        function AlertUsuarioVacio() {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Usted debe llenar el campo Usuario',
            })
        }

        function ConfirmarAfiliacion() {
            var comprarDeTodosModos = document.getElementById('<%= CompraDeTodosModos.ClientID %>');

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false,
            })

            swalWithBootstrapButtons.fire({
                title: 'Con esta cantidad de puntos puedes hacer una afiliación Profesional. ¿Quiere continuar con la afiliación?',
                text: "Si le das a sí, está aceptando hacer una afiliacón emprendedor",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'SÍ',
                cancelButtonText: 'NO',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    comprarDeTodosModos.value = "SI";
                    location.href = ("DetalleDeCompra.aspx");
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Recuerde!',
                        'Puede cambiar el tipo de afiliación',
                        'error'
                    )
                    comprarDeTodosModos.value = "NO";
                }

            })

        }


        function MostrarRegistroClienteAndTienda() {
            var divRegistroCliente = document.getElementById('MostrarRegistroCliente');

            divRegistroCliente.style.display = "block";
            fijarCampoRequeridoRegistrarCliete();

            var divComboTienda = document.getElementById('MostrarComboTiena');
            var divDatosDelivery = document.getElementById('MostrarDatosParaDelivery');

            divComboTienda.style.display = "block";
            divDatosDelivery.style.display = "none";

            MostrarTipoCopraAndPuntos();
        }

        function MostrarRegistroClienteAndDelivery() {
            var divRegistroCliente = document.getElementById('MostrarRegistroCliente');

            divRegistroCliente.style.display = "block";
            fijarCampoRequeridoRegistrarCliete();

            MostrarDelivery();
            MostrarTipoCopraAndPuntos();

        }

        function MostrarTiendaAndMostrarTipoCopraAndPuntos() {
            MostrarTipoCopraAndPuntos();
            MostrarTienda();
        }

        function MostrarDeliveryAndMostrarTipoCopraAndPuntos() {
            MostrarTipoCopraAndPuntos();
            MostrarDelivery();
        }

        function MostrarTienda() {
            var divComboTienda = document.getElementById('MostrarComboTiena');
            var divDatosDelivery = document.getElementById('MostrarDatosParaDelivery');

            divComboTienda.style.display = "block";
            divDatosDelivery.style.display = "none";
        }

        function MostrarDelivery() {
            var divComboTienda = document.getElementById('MostrarComboTiena');
            var divDatosDelivery = document.getElementById('MostrarDatosParaDelivery');

            divComboTienda.style.display = "none";
            divDatosDelivery.style.display = "block"
        }

        function AlertTipoCliente() {
            Swal.fire('Seleccione tipo cliente')
        }

        function AlertFechaNacimiento() {
            Swal.fire('Seleccione fecha de nacimiento')
        }

        function AlertSexo() {
            Swal.fire('Seleccione sexo')
        }

        function AlertPais() {
            Swal.fire('Seleccione país')
        }

        function AlertDepartamento() {
            Swal.fire('Seleccione departamento')
        }

        function AlertCompraExitosa() {
            Swal.fire('Compra exitosa! :D')
        }

        function AlertProvincia() {
            Swal.fire('Seleccione provincia')
        }

        function AlertLogueate() {
            Swal.fire('Logueate antes de comprar!')
        }

        function AlertDistrito() {
            Swal.fire('Seleccione distrito')
        }

        function AlertTienda() {
            Swal.fire('Seleccione tienda')
        }

        function AlertTipoCompra() {
            Swal.fire('Seleccione el tipo de compra')
        }

        function MostrarTipoCopraAndPuntos() {
            var divTipoCompraAndPuntos = document.getElementById('MostrarSoloLogueado');

            divTipoCompraAndPuntos.style.display = "block";
        }

        function AlertTipoEntrega() {
            Swal.fire('Seleccione el tipo de entrega')
        }

        function AlertMedioPago() {
            Swal.fire('Seleccione el medio de pago')
        }

        function MostrarValor() {
            var valor = $("[id$=P0001]").val();
            alert(valor);
        }

        function ActualizarProducto(codigo) {
            GuardarDatosDelivery();
            var nuevaCantidad = $("[id$=" + codigo + "]").val();
            location.href = "DetalleDeCompra.aspx?cantidad=" + nuevaCantidad + "&codPro=" + codigo;
        }

        function GuardarDatosDelivery() {

            var nombreD = $("#txtNombreDatosCompra").val();
            var DNID = $("#txtDNIDatosCompra").val();
            var CelularD = $("#txtCelularDatosCompra").val();
            var DireccionD = $("#txtDireccionDatosCompra").val();
            var NomTransporteD = $("#txtTransporteDatosCompra").val();
            var DirecTransporteD = $("#txtDirecTransporteDatosCompra").val();
            var ProvinciaD = $("#txtProvinciaDatosCompra").val();
            var DirecProvinciaD = $("#txtDirecProvinciaDatosCompra").val();
            var obj2 = JSON.stringify({
                Nombre: nombreD, DNI: DNID, Celular: CelularD, Direccion: DireccionD,
                Transporte: NomTransporteD, DirTransporte: DirecTransporteD, Provincia: ProvinciaD, DirProvincia: DirecProvinciaD
            });

            $.ajax({
                type: "POST",
                url: "DetalleDeCompra.aspx/GuardarDatosDelivery",
                data: obj2,
                contentType: 'application/json; charset=utf-8',
                error: function (xhr, ajaxOptions, throwError) {
                    console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
                },
                success: function (data2) {
                    console.log(data2.d);
                }
            });
        }

        function EliminarProducto(codigo) {
            GuardarDatosDelivery();
            location.href = "DetalleDeCompra.aspx?codPro=" + codigo;
        }

        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });

        function pageLoad() {
            $('.file-upload').file_upload();

            function filePreview(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagePreview').html("<img src='" + e.target.result + "' style='height:200px' />");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $('.imagen').change(function () {
                filePreview(this);
            });
        }

    </script>
    <script>
        //Para que el menu del navbar se quede de un color cuando esté seleccionado
        window.onload = function () {
            document.getElementById("idMenuTienda").style.color = 'white';
            document.getElementById("idMenuTienda").style.borderBottom = '3px solid white';
            document.getElementById("idSubMenuTienda").style.color = 'white';
            document.getElementById("idSubMenuTienda").style.borderBottom = '3px solid white';
        }
    </script>

    <script src="js/Banner de Store Template/jquery.flexslider-min.js"></script>
    <!-- Magnific Popup -->
    <script src="js/Banner de Store Template/jquery.magnific-popup.min.js"></script>
    <!-- Stellar Parallax -->
    <script src="js/Banner de Store Template/jquery.stellar.min.js"></script>
    <!-- Main -->
    <script src="js/Banner de Store Template/main.js"></script>


    <%--    <script src="js/main.js"></script>--%>
</asp:Content>
