﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="SantaNaturaNetworkV3.ShoppingCart" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/Banner de Store Template/animate.css">
    <!--Para que el banner salga muy bien diseñado-->
    <link rel="stylesheet" href="css/proyecto2/estilosDetalleDeCompra-Banner.css">
    <link href="css/proyecto2/vendors/elegant-icon/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="icon" href="img/Natural_Food_icon.png" type="image/x-icon" />

    <!--Para el boton ingresar foto de perfil-->
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />
    <link href="css/carritoDeCompra.css" rel="stylesheet" />
    <link href="css/estilosDetalleCompra.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/proyecto2/eskju.jquery.scrollflow.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.7/dist/sweetalert2.all.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://www.paypal.com/sdk/js?client-id=ARsiy6qtq4wNw8zEpuuxlngjV7uw83Xp3KenZ4eT2jC2ii2Vvtb5KUPe7CR9JY2ZGRyQS4pvtB9qvYP_"></script>
    <%--<script src="https://www.paypal.com/sdk/js?client-id=AfCQzWDi9TYauxQdJXJSt86JJ7OVF3tUuACWLN4SP1NOJ4qEzTuw5A5G3uRYCMl0WJbBiI1YBqYva6LK"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .parallax2 {
            background: url("img/detalledecompra-1355x210.png");
            background-attachment: fixed;
            background-position: bottom;
            background-repeat: no-repeat
        }

        .modal-body {
            height: initial;
            overflow: hidden;
            text-align: justify;
        }

        .btn-visa-upload {
            height: 49px;
            border-radius: 8px;
            background: #3850A1;
            font-size: 15px;
            color: white;
            line-height: 46px;
            border: 1px solid transparent;
            margin: 10px 0px 10px 0px;
        }

            .btn-visa-upload:hover {
                background-color: #D6D9D9;
                color: #3850A1;
            }


        #txtNombreDatosCompra, #txtDNIDatosCompra, #txtCelularDatosCompra, #txtDireccionDatosCompra, #txtTransporteDatosCompra, #txtDirecTransporteDatosCompra, #txtProvinciaDatosCompra, #txtDirecProvinciaDatosCompra {
            height: 24.5px
        }

        .btnValidar {
            margin-top: 10px;
            height: calc(2.25rem + 2px);
        }

        .txtRUC {
            height: calc(2.25rem + 2px);
        }

        .lblValidarFacturacion {
            justify-content: center;
        }
        @media screen and (max-width: 400px) {
            #paypal-button-container {
                width: 100%;
            }
        }
        
        /* Media query for desktop viewport */
        @media screen and (min-width: 400px) {
            #paypal-button-container {
                width: 250px;
            }
        }

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(img/loadingPageSantanatura.gif) center no-repeat #fff;
        }
    </style>
    <div id="page_loader" style="display: none" class="se-pre-con"></div>
    <asp:ScriptManager runat="server">
        <Scripts>
            <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
            <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
            <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
            <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
            <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
            <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
            <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
            <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
        </Scripts>
    </asp:ScriptManager>
    <%--    <!--================Categories Banner Area =================-->
    <!--================End Categories Banner Area =================-->--%>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <aside id="colorlib-hero" class="breadcrumbs" style="margin-top: 50px">
        <div class="flexslider">
            <ul class="slides">
                <li <%--class="parallax2" style="background-size:100% 500px"--%>>
                    <img src="img/detalledecompra.png" style="width: 100vw; height: 300px; background-attachment: fixed" />
                    <div class="overlay">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                    <div class="slider-text-inner text-center" style="z-index: 1">
                                        <h1 id="Shopping_h1_1" runat="server">Shopping Cart</h1>
                                        <h2 class="bread">
                                            <span><a id="inicio" runat="server" href="Store.aspx" style="color: green; font-weight: bold; transition: color 0.5s ease;">Store</a></span>
                                            <span style="font-size: 10px"><a style="padding-top: 5px; color: green" class="glyphicon glyphicon-chevron-right"></a></span>
                                            <span><a id="Shopping_a_1" runat="server">Shopping Cart</a></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row justify-content-center">
                            <div class="col-xs-8">
                                <button id="btnComPendiente" runat="server" class="btn btn-success form-control open-modal-pay" type="button">Submit Pendient Order</button>
                            </div>
                        </div>
                        <br />
                        <div class="row justify-content-center">
                            <div>
                                <div id="paypal-button-container"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCancelM" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <%--    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="container modal-dialog modal-sm" id="modalTamano" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="text-align: center; font-weight: bold" class="modal-title" id="exampleModalLabel">PAYPAL</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <button id="btnComPendiente" runat="server" class="btn btn-success form-control open-modal-pay" type="button">Submit Pendient Order</button>
                        </div>
                        <div class="col-md-3">
                            <div id="paypal-button-container"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnId" type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>--%>
    <!-- Modal -->

    <div id="ContenidoFluido" class="container-fluid" style="width: 92%; margin-top: 20px">

        <div class="row row-pb-md" style="padding-top: 30px;">
            <div class="col-md-10 col-md-offset-1">
                <div class="process-wrap">
                    <div id="circle01" class="process text-center active">
                        <p><span>01</span></p>
                        <h3 id="carritoDeCompras" class="active" runat="server">SHOPPING CART</h3>
                    </div>
                    <div id="circle02" class="process text-center">
                        <p><span style="transition: all 1s;">02</span></p>
                        <h3 id="afiliacion" runat="server" style="transition: all 1s;">MEMBERSHIP</h3>
                    </div>
                    <div id="circle03" class="process text-center">
                        <p><span id="circulitoDel" style="transition: all 1s;">03</span></p>
                        <h3 id="delivery" runat="server" style="transition: all 1s;">DELIVERY</h3>
                    </div>
                    <div id="circle04" class="process text-center">
                        <p><span id="circulito" style="transition: all 1s;">04</span></p>
                        <h3 id="ordenCompletada" runat="server" style="transition: all 1s;">ORDER COMPLETED</h3>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <hr />


        <div class="row">
            <div class="col-md-12" id="01carritoDeCompras" style="padding-left: 0px; padding-bottom: 40px">
                <!--TABLA DETALLE DE COMPRA-->
                <div class="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-xs-12 scrollflow -slide-top" style="padding-top: 30px; z-index: 3">
                    <div id="tablaCompra" class="container">
                        <div class="box-body table-responsive">
                            <table class="table table-hover table-condensed table-bordered w-auto" id="tbl_datosPro">
                                <thead class="table-info">
                                    <tr class="text-center">
                                        <th style="width: 150px"></th>
                                        <th id="tabla_produc_th1" runat="server">Product</th>
                                        <th id="tabla_price_th1" runat="server">Price</th>
                                        <th id="tabla_points_th1" runat="server">Points</th>
                                        <th id="tabla_quantity_th1" runat="server">Quantity</th>
                                        <th id="tabla_net_th1" runat="server">Net subtotal</th>
                                        <th id="tabla_subtotal_th1" runat="server">Subtotal points</th>
                                        <th id="tabla_actions_th1" runat="server">ACTIONS</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <% if (productosCarrito != null)
                                        {
                                            foreach (var productoCarrito in productosCarritoPaquete)
                                            {%>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-7 col-sm-7 col-md-7 center-block">
                                                    <img id="ImgFotoo" src="products/<%=productoCarrito.Foto %>" class="img-responsive" />
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center">

                                            <%=productoCarrito.NombreProducto %>
                                           
                                        </td>
                                        <td data-th="Price" class="text-center">$ <%=productoCarrito.PrecioUnitario.ToString("N2").Replace(",", ".") %></td>
                                        <td class="text-center">
                                            <%=productoCarrito.Puntos.ToString("N2").Replace(",", ".") %>
                                        </td>
                                        <td data-th="Quantity">
                                            <input type="number" id="<%=productoCarrito.Codigo %>" class="form-control text-center" value="<%=productoCarrito.Cantidad %>">
                                        </td>
                                        <td class="text-center">
                                            <label id="Sub<%=productoCarrito.Codigo %>"><%=productoCarrito.SubTotalNeto.ToString("N2").Replace(",", ".") %></label>

                                        </td>
                                        <td class="text-center">
                                            <label id="SubP<%=productoCarrito.Codigo %>"><%=productoCarrito.SubTotalPuntos.ToString("N2").Replace(",", ".") %></label>
                                        </td>
                                        <td class="actions" data-th="">
                                            <a onclick="ActualizarProducto('<%=productoCarrito.Codigo %>')" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></a>
                                            <%--                                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>--%>
                                            <a onclick="EliminarProducto('<%=productoCarrito.Codigo %>', this)" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>

                                </tbody>

                                <%}
                                    } %>
                            </table>

                        </div>
                    </div>

                </div>
                <!--DATOS DE COMPRA-->
                <div id="datosDeLaCompra" class="col-xl-3 col-lg-6 col-md-8 col-sm-8 scrollflow -slide-left -opacity" style="padding-top: 10px; z-index: 2">
                    <div id="resumenDeLaCompra">

                        <div id="bloqueDatosDeLaCompra" class="form-group" style="margin-top: -20px">
                            <div class="col-md-12 text-center">
                                <label style="font-size: 17px; font-weight: bolder" id="Shopping_label_1" runat="server">PURCHASE INFORMATION</label>
                            </div>

                            <div class=" row form-group" id="MostrarSoloLogueado">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra" id="Shopping_label_2" runat="server">Type of purchase: </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <select id="STipoCompra" runat="server" class="form-control btn-lg marginTop">
                                        </select>
                                    </div>
                                </div>
                                <div class="row col-md-12" style="display: none;">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra" id="Shopping_label_3" runat="server">Hearts:</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbCorazones" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra" id="Shopping_label_4" runat="server">Points for Rank:</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbPuntosRango" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra" id="Shopping_label_5" runat="server">Real points:</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbPuntosCompra" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra" id="Shopping_label_6" runat="server">Total price before savings :</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbPrecioTotal" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra" id="Shopping_label_7" runat="server">Total price after savings:</label>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="LbPrecioPagar" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div id="MostrarComboTiena" class="row form-group" style="margin-top: -10px;">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra" id="Shopping_label_8" runat="server">Store:</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <select id="ComboTienda" class="form-control btn-lg marginTop" runat="server"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px; display: none;">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelResuCompra" id="Shopping_label_9" runat="server">Payment method:</label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <select id="SMedioPago" class="form-control btn-lg marginTop" runat="server"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px; display: none;" id="DatosPreRegistro" runat="server">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="labelComprobante" id="Shopping_label_10" runat="server">Pre-Registration User: </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <select id="cboPreRegistro" class="form-control btn-lg marginTop" runat="server"></select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="btnRegistrarAfiliado" class="row col-md-12" runat="server">
                            <div class="col-sm-8 col-sm-offset-2">
                                <a class="btn btn-success form-control tablaSiguiente" runat="server" style="font-size: 12px" href="javascript:void('');" id="Shpping_a_1">Next <span class="glyphicon glyphicon-arrow-right"></span></a>
                            </div>
                        </div>
                        <div id="btnDetalleDelivery" class="row col-md-12" runat="server">
                            <div class="col-sm-8 col-sm-offset-2">
                                <a class="btn btn-success form-control tablaDelivery" runat="server" style="font-size: 12px" href="javascript:void('');" id="Shpping_a_2">Next <span class="glyphicon glyphicon-arrow-right"></span></a>
                            </div>
                        </div>
                        <%--<div class="row col-md-12" id="CompraNormal" runat="server">
                            <div class="col-sm-8 col-sm-offset-2">
                                <asp:Button ID="BntComprar" OnClientClick="return Comprar()" OnClick="BntComprar_Click" CssClass="btn btn-success form-control" runat="server" Text="Comprar" />
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="02afiliacion">
                <div style="width: 165px; padding-bottom: 20px">
                    <a class="btn btn-danger form-control 01" style="font-size: 13px" href="javascript:void('01');"><span class="glyphicon glyphicon-arrow-left" id="Shpping_a_3" runat="server"></span>&nbsp BACK</a>
                </div>
                <!--REGISTRO AFILIACION-->
                <div id="MostrarRegistroCliente" class="col-md-8">
                    <div class="row form-group colorlib-form">
                        <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto">
                            <h1 id="Shpping_h1_1" runat="server">MEMBERSHIP REGISTRATION</h1>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 16px" id="Shopping_label_11" runat="server">ACCOUNT DATA</label>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_12" runat="server">User</label>
                                    <asp:TextBox ID="txtUl" title="A username is required" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_13" runat="server">Password</label>
                                    <asp:TextBox ID="TxtCl" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity" id="btnCargaImagen" runat="server" style="display: none;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_14" runat="server">Profile picture</label>
                                    <label class="file-upload btn btn-success form-control marginTop" style="font-size: 15px">
                                        Enter your photo
                                    <input type="file" class="imagen form-control" id="imagen" name="MiImagen" accept="image/x-png,image/jpeg" runat="server" style="margin-top: 4px" />
                                    </label>
                                </div>
                            </div>

                            <div class="row col-md-12" id="imagenMostrada" runat="server" style="display: none;">
                                <div class="form-group col-md-4 scrollflow -opacity" style="margin-left: auto; margin-right: auto">
                                    <label id="Shopping_label_15" runat="server">My profile picture</label>
                                    <div id="imagePreview" class="center-block align-content-center">
                                        <img src="img/usuario1.png" class="img-fluid" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="margin-top: 13px">
                            <label style="font-weight: bold; font-size: 16px" id="Shopping_label_16" runat="server">Personal information</label>


                            <div class="row col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_17" runat="server">UpLine (*)</label>
                                    <asp:DropDownList ID="CboUpLine" runat="server" CssClass="form-control btn-lg marginTop" />

                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_18" runat="server">Customer type</label>
                                    <asp:DropDownList ID="cboTipoCliente" runat="server" CssClass="form-control btn-lg marginTop" Enabled="False" />
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_19" runat="server">Yachai Wasi Default</label>
                                    <asp:DropDownList ID="cboTipoEstablecimiento" runat="server" CssClass="form-control btn-lg marginTop" />
                                </div>
                            </div>

                            <div class="row col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_20" runat="server">Names (*)</label>
                                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_21" runat="server">Last Name (*)</label>
                                    <asp:TextBox ID="txtApPaterno" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-3 scrollflow -opacity" style="display: none;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_22" runat="server">Maternal Surname (*)</label>
                                    <asp:TextBox ID="txtApMaterno" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_23" runat="server">Sponsor (*)</label>
                                    <asp:TextBox ID="txtPatrocinador" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row col-md-12">

                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_24" runat="server">Birth (*)</label>
                                    <input type="text" id="datepicker" class="form-control text-uppercase marginTop" readonly runat="server" />
                                    <asp:HiddenField ID="FechaNaci" runat="server" />
                                    <asp:HiddenField ID="DivTipCompraAndPuntos" runat="server" />
                                </div>

                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_25" runat="server">Gender (*)</label>
                                    <asp:DropDownList ID="ComboSexo" runat="server" CssClass="form-control marginTop">
                                        <asp:ListItem Value="">Select:</asp:ListItem>
                                        <asp:ListItem Value="1">MALE</asp:ListItem>
                                        <asp:ListItem Value="2">FEMALE</asp:ListItem>
                                        <asp:ListItem Value="3">NOT SPECIFIC</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_26" runat="server">Document type (*)</label>
                                    <asp:DropDownList ID="ComboTipoDocumento" runat="server" CssClass="form-control marginTop">
                                        <asp:ListItem Value="">Select:</asp:ListItem>
                                        <asp:ListItem Value="1">SSN (Social Security Number)</asp:ListItem>
                                        <asp:ListItem Value="2">PASSPORT</asp:ListItem>
                                        <asp:ListItem Value="3">ITIN</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group col-md-3 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_27" runat="server">Document No. (*)</label>
                                    <asp:TextBox ID="txtNumDocumento" runat="server" CssClass="form-control text-uppercase marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>

                            </div>

                            <div class="row col-md-12">

                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_28" runat="server">Email (*)</label>
                                    <asp:TextBox ID="TxtCorreo" runat="server" CssClass="form-control text-uppercase marginTop" TextMode="Email"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_29" runat="server">Phone (*)</label>
                                    <asp:TextBox ID="TxtTelefono" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_30" runat="server">Mobile (*)</label>
                                    <asp:TextBox ID="TxtCelular" runat="server" onkeypress="return validarNumeros(event)" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_31" runat="server">Country (*)</label>
                                    <asp:DropDownList ID="cboPais" runat="server" CssClass="form-control marginTop" />
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_32" runat="server">Address (*)</label>
                                    <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_33" runat="server">Reference</label>
                                    <asp:TextBox ID="TxtReferencia" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="lblDepart" runat="server">State (*)</label>
                                    <asp:DropDownList ID="cboDepartamento" runat="server" CssClass="form-control marginTop" />
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="lblZipCode" runat="server">Zip Code (*)</label>
                                    <asp:DropDownList ID="cboZipCode" runat="server" CssClass="form-control marginTop" />
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity" style="display: none;">
                                    <label class="label" style="font-weight: bold" id="lblProv" runat="server">County</label>
                                    <asp:DropDownList ID="cboProvincia" runat="server" CssClass="form-control marginTop" />
                                </div>
                                <div id="divDistrito" class="form-group col-md-4 scrollflow -opacity" style="display: none;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_34" runat="server">District</label>
                                    <asp:DropDownList ID="cboDistrito" runat="server" CssClass="form-control marginTop" />
                                    <br />
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_35" runat="server">Yachai Wasi of gifts (*)</label>
                                    <asp:DropDownList ID="cboPremio" runat="server" CssClass="form-control btn-lg marginTop" />
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_36" runat="server">Choose lenguage notifications (*)</label>
                                    <asp:DropDownList ID="cbo_idioma" runat="server" CssClass="form-control marginTop">
                                        <asp:ListItem Value="0">Select:</asp:ListItem>
                                        <asp:ListItem Value="1">Spanish</asp:ListItem>
                                        <asp:ListItem Value="2">English</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label style="font-weight: bold; font-size: 16px" id="Shopping_label_37" runat="server">Bank Account Data</label>
                            <div class="row col-md-12" style="display: none;">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_38" runat="server">SSN/RUC</label>
                                    <asp:TextBox ID="TxtRUC" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_39" runat="server">Bank</label>
                                    <asp:TextBox ID="TxtBanco" runat="server" CssClass="form-control text-uppercase marginTop" onkeydown="validarLetras(event)"
                                        onkeyup="validarLetras(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_40" runat="server">Number of withdrawals</label>
                                    <asp:TextBox ID="TxtNumCuenDetracciones" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>

                            </div>

                            <div class="row col-md-12">
                                <div class="form-group col-md-4 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_41" runat="server">Acount Number</label>
                                    <asp:TextBox ID="TxtNumCuenDeposito" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6 scrollflow -opacity">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_42" runat="server">Swift Number</label>
                                    <asp:TextBox ID="TxtNumCuenInterbancaria" runat="server" CssClass="form-control marginTop" onkeypress="return validarNumeros(event)"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--RESUMEN FINAL DE LA COMPRA-->
                <div class="col-md-4">
                    <div id="resumenDeLaCompra2">
                        <div class="form-group colorlib-form">
                            <div class="col-md-12 text-center">
                                <label style="font-size: 17px; font-weight: bolder" id="Shopping_label_43" runat="server">SUMMARY OF PURCHASE</label>
                            </div>
                            <div class=" row form-group" id="MostrarSoloLogueado2" style="display: block;">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_44" runat="server">Type of purchase: </label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lbTCompra2"></asp:Label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_45" runat="server">Points for Rank:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label ID="lbPuntosRango2" CssClass="labelResuCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_46" runat="server">Purchase points:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label ID="lblPuntos2" CssClass="labelResuCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_47" runat="server">Total price before savings:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label ID="lblPTotal2" CssClass="labelResuCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_48" runat="server">Total price after savings:</label>
                                    </div>

                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label ID="lblPPagar2" CssClass="labelResuCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div id="MostrarComboTienda2" class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_49" runat="server">Store:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" ID="lbTienda2" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_50" runat="server">Payment method:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label ID="lbMPago2" CssClass="labelResuCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row col-md-12" id="CompraNormal2" runat="server">
                            <div class="col-md-3">
                            </div>
                            <div id="divAfilDelivery" class="row col-md-12" runat="server">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <a class="btn btn-success form-control tablaAfiDel" runat="server" style="font-size: 12px" href="javascript:void('');" id="Shopping_a_4">Next <span class="glyphicon glyphicon-arrow-right"></span></a>
                                </div>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="03Delivery">
                <div style="width: 165px; padding-bottom: 20px">
                    <a class="btn btn-danger form-control 02" id="Shopping_a_5" runat="server" style="font-size: 13px" href="javascript:void('01');"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp BACK</a>
                </div>
                <div id="DivDelivery" class="col-md-8">
                    <div class="row form-group colorlib-form">
                        <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto">
                            <h1 id="Shopping_h1_2" runat="server">PURCHASE INFORMATION</h1>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_51" runat="server">Purchase Holder</label>
                                    <select id="ddlTitular" class="form-control btn-lg marginTop" runat="server"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_52" runat="server">Name</label>
                                    <asp:TextBox ID="txtNameDelivery" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%;">
                                    <label class="label" style="font-weight: bold" id="lblLastNameDelivery" runat="server">Last Name</label>
                                    <asp:TextBox ID="txtLastNameDelivery" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_53" runat="server">Document</label>
                                    <asp:TextBox ID="txtDocDelivery" runat="server" CssClass="form-control text-uppercase marginTop" TextMode="Email"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_54" runat="server">Email</label>
                                    <asp:TextBox ID="txtEmailDelivery" runat="server" CssClass="form-control text-uppercase marginTop" TextMode="Email"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_55" runat="server">Adress</label>
                                    <asp:TextBox ID="txtDirectionDelivery" runat="server" CssClass="form-control text-uppercase marginTop"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_56" runat="server">Phone</label>
                                    <asp:TextBox ID="txtPhoneDelivery" runat="server" CssClass="form-control text-uppercase marginTop" TextMode="Phone"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_57" runat="server">State</label>
                                    <asp:DropDownList ID="cbo_departamento_carrito" runat="server" CssClass="form-control marginTop" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%;">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_58" runat="server">Zip Code</label>
                                    <asp:DropDownList ID="cbo_zipcode_carrito" runat="server" CssClass="form-control marginTop" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto; width: 80%; display: none">
                                    <label class="label" style="font-weight: bold" id="Shopping_label_59" runat="server">Zip Code</label>
                                    <asp:TextBox ID="txt_zipcode" runat="server" CssClass="form-control text-uppercase marginTop solo-numero" TextMode="Phone" MaxLength="5"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                        <div class="form-group scrollflow -pop -opacity" style="margin-left: auto; margin-right: auto;">
                            <button id="btnValDelivery" class="btn btn-success form-control" type="button" style="display: none" runat="server">Validate</button>
                        </div>
                    </div>
                </div>
                <!--RESUMEN FINAL DE LA COMPRA-->
                <div class="col-md-4">
                    <div id="resumenDeLaCompra3">

                        <div class="form-group colorlib-form">
                            <div class="col-md-12 text-center">
                                <label style="font-size: 17px; font-weight: bolder" id="Shopping_label_60" runat="server">SUMMARY OF PURCHASE</label>
                            </div>


                            <div class=" row form-group">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_61" runat="server">Type of purchase: </label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lblTipoCompraDel"></asp:Label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_62" runat="server">Points for Rank:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lblPtsRangoDel"></asp:Label>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_63" runat="server">Purchase points:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lblPtsRealDel"></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_64" runat="server">Total price before savings:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lblPrecBrutoDel"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_65" runat="server">Total price after savings:</label>
                                    </div>

                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lblPrecPagarDel"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_66" runat="server">Store:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lblTiendaDel"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_67" runat="server">Shipping & Handling:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lbl_shipping" Text="$0"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_68" runat="server">TAX:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lblTAX" Text="$0"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_69" runat="server">Total order:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lblTotalPago"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin-top: -10px; display: none;">
                                <div class="row col-md-12">
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <label class="labelResuCompra" id="Shopping_label_70" runat="server">Shipping and Handling:</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6 scrollflow -opacity">
                                        <asp:Label CssClass="labelResuCompra" runat="server" ID="lblPriceDel"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row col-md-12" id="divCheckTerminosAfi" runat="server">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-7">
                                <input id="chkTerminos" type="checkbox" class="form-check-input">
                                <span style="font-size: 13.5px; margin-left: 15px;" id="Shopping_span_1" runat="server">I declare that I have read and accepted the <a id="clicTerminosCondiciones" href="#">Terms and Conditions.</a> </span>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                        <div class="row col-md-12" id="divCheckTerminosSocio" runat="server">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-7">
                                <input id="chkTerminosSocio" type="checkbox" class="form-check-input">
                                <span style="font-size: 13.5px; margin-left: 15px;" id="Shopping_span_2" runat="server">I declare that I have read and accepted the <a id="clicTerminosCondiciones2" href="#">Terms and Conditions.</a> </span>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                        <div class="row col-md-12" id="Div3" runat="server">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-7">
                                <button id="btnUCDelivery" runat="server" class="btn btn-success form-control" type="button" style="display: none">Submit Order</button>
                                <button id="btnUCDelivery2" runat="server" class="btn btn-success form-control" style="display: none;" type="button" onclick="ConsumirShippingAPI();">Submit Order2</button>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="04ordenCompletada">
                <div class="row" id="complet" runat="server">
                    <div class="col-md-10 col-md-offset-1 text-center">
                        <img src="img/shoppingcart_accept_compra_12832.png" />
                        <br />
                        <br />
                        <br />
                        <h2 id="Shopping_h2_1" runat="server">Your order is currently being processed. You will receive an order confirmation email in the next 48 hours with the payment options.</h2>
                        <br />
                        <p>
                            <a href="MisComprasV2.aspx" class="btn btn-primary" id="Shopping_a_2" runat="server" style="height: 30px">Shopping history</a>
                            <a href="Store.aspx" class="btn btn-primary btn-outline" id="Shopping_a_3" runat="server" style="height: 30px">Keep buying</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <br />

    </div>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="js/moment.js"></script>
    <script src="js/carritoDeCompra5.js?v62"></script>
    <script src="js/file-uploadv1.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.solo-numero').numeric();
            var completada = "<%=Session["mostrarCompraTerminada"]%>";

            if (completada == 0) {
                $("#02afiliacion").hide();
                $("#03Delivery").hide();
                $("#04ordenCompletada").hide();
            }

            /*RETROCEDER DEL PASO 2 AL PASO 1*/
            $(".01").click(function (e) {
                $("#01carritoDeCompras").fadeIn(300);
                $("#tablaCompra").fadeIn(300);
                $("#resumenDeLaCompra").fadeIn(300);
                $("#02afiliacion").slideUp(0);
                var elemento = document.getElementById("circle01");
                elemento.classList.remove('active');
                //$("#resumenDeLaCompra2").slideUp(0);
            });

            /*IR DEL PASO 2 AL PASO 3*/
            $(".registrarPedido").click(function (e) {
                $("#04ordenCompletada").fadeIn(300);
                $("#02afiliacion").slideUp(0);
                $("01carritoDeCompras").slideUp(0);
                var ordenCompletada = document.getElementById("ordenCompletada");
                var elemento = document.getElementById("circle03");
                elemento.className += " active"
                ordenCompletada.className += " active"
                //$("#resumenDeLaCompra2").slideUp(0);
            });

            /**/
            //$(".02").click(function (e) {
            //    $("#02afiliacion").fadeIn(300);
            //    $("#04ordenCompletada").slideUp(0);
            //    var elemento = document.getElementById("circle03");
            //    elemento.classList.remove('active');
            //    //$("#resumenDeLaCompra2").slideUp(0);
            //});

            $('.tablaSiguiente').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 100);
                return false;
            });

            $('.tablaDelivery').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 100);
                return false;
            });

            $('.tablaAfiDel').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 100);
                return false;
            });

            var tipoComS = $("#STipoCompra").val();

            if (tipoComS == "07" | tipoComS == "08" | tipoComS == "09" | tipoComS == "10" |
                tipoComS == "11" | tipoComS == "12" | tipoComS == "13") {
                $("#circle02").hide();
                $("#circulitoDel").text("02");
                $("#circulito").text("03");
            } else {
                $("#circle02").show();
                $("#circulitoDel").text("03");
                $("#circulito").text("04");
            }



        });

        function ConsumirShippingAPI() {
            var settings = {
                "url": "https://secure.shippingapis.com/ShippingAPI.dll?API=RateV4&XML=<RateV4Request USERID=\"341SANTA6369\"> <Revision>2</Revision><Package ID=\"1\"><Service>Priority</Service> <ZipOrigination>84322</ZipOrigination> <ZipDestination>53225</ZipDestination> <Pounds>2</Pounds> <Ounces>0</Ounces> <Container>RECTANGULAR</Container> <Size>LARGE</Size> <Width>0</Width> <Length>0</Length> <Height>0</Height> <Girth>0</Girth> <Value>0</Value></Package></RateV4Request>", "method": "POST", "timeout": 0,
            };
            $.ajax(settings).done(function (response) {
                debugger;
                var valor;
                var valor2;
                var Rateinicial;
                var Ratefinal;
                var valor_final;
                for (let dato in response) {
                    // datos.resultado y datos.operaciones
                    for (let i in response[dato]) {
                        // Acceder a cada elemento de resultados y operaciones, según el primer ciclo
                        valor = String(response[dato][i]);
                        valor2 = valor.indexOf('<RateV4Response><Package ID');
                        if (valor2 != -1) {
                            valor_final = valor;
                            console.log(valor_final);
                        }
                    }
                }
                debugger;
                Rateinicial = valor_final.indexOf('<Rate>');
                Ratefinal = valor_final.indexOf('</Rate>');
                var RateExtraido;
                var TotalPago;
                var suma;
                if (Rateinicial != -1 && Ratefinal != -1) {
                    RateExtraido = "$ " + valor_final.substring(Rateinicial, Ratefinal);
                    document.getElementById('lbl_shipping').innerHTML = RateExtraido;
                    TotalPago = document.getElementById('lblTotalPago').innerHTML;//document.getElementById("lblTotalPago").value;
                    TotalPago = TotalPago.replace("$", "");
                    totalPago = parseFloat(TotalPago);
                    RateExtraido = RateExtraido.replace("$ <Rate>", "");
                    RateExtraido = parseFloat(RateExtraido);
                    suma = totalPago + RateExtraido;
                    document.getElementById('lblTotalPago').innerHTML = "$ " + suma;
                    //alert(suma);

                    console.log(RateExtraido);
                }
            });
        }

    </script>
    <script type="text/javascript">

        function IrCompraTerminada() {
            var ordenCompletada = document.getElementById("ordenCompletada");
            var elemento = document.getElementById("circle04");
            $("#04ordenCompletada").fadeIn(300);
            $("#03Delivery").slideUp(0);
            $("#02afiliacion").slideUp(0);
            $("#01carritoDeCompras").slideUp(0);
            $("#resumenDeLaCompra").slideUp(0);
            $("#tablaCompra").slideUp(0);
            elemento.className += " active"
            ordenCompletada.className += " active"
        }

        function formatDate(date) {
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + '/' + monthIndex + '/' + year;
        }

        function FormatearFecha(fecha) {
            var ms = Date.parse(fecha);
            var fecha = new Date(ms);

            var dd = fecha.getDate();
            var mm = fecha.getMonth() + 1;
            var yyyy = fecha.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            return dd + '/' + mm + '/' + yyyy;
        }

        function toDate(dateStr) {
            var parts = dateStr.split("/")
            return new Date(parts[2], parts[1] - 1, parts[0])
        }

        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });

        function pageLoad() {
            $('.file-upload').file_upload();

            function filePreview(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagePreview').html("<img src='" + e.target.result + "' style='height:200px' />");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $('.imagen').change(function () {
                filePreview(this);
            });
        }

    </script>
    <script>
        window.onload = function () {
            document.getElementById("idMenuTienda").style.color = 'white';
            document.getElementById("idMenuTienda").style.borderBottom = '3px solid white';
            document.getElementById("idSubMenuTienda").style.color = 'white';
            document.getElementById("idSubMenuTienda").style.borderBottom = '3px solid white';
        }
    </script>

    <script src="js/Banner de Store Template/jquery.flexslider-min.js"></script>
    <script src="js/Banner de Store Template/jquery.magnific-popup.min.js"></script>
    <script src="js/Banner de Store Template/jquery.stellar.min.js"></script>
    <script src="js/Banner de Store Template/main.js"></script>
</asp:Content>

