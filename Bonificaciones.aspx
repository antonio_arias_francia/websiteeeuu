﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Bonificaciones.aspx.cs" Inherits="SantaNaturaNetworkV3.Bonificaciones" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!--Posicionamiento de ciertos elementos-->
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">

    <style>
        .customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
            margin-left: auto;
            margin-right: auto;
        }

            .customers td, .customers th {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: center
            }

            .customers tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            .customers tr:hover {
                background-color: #ddd;
            }

            .customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                background-color: #0b1c6f;
                color: white;
            }

        /*------------------------------------Chart Comisiones-------------------------------------*/
        .is-empty {
            display: none !important;
        }

        /*------------------------------*/
        .customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
        }

            .customers td, .customers th {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: center
            }

            .customers tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            .customers tr:hover {
                background-color: #ddd;
            }

            .customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                background-color: #1e5fb1;
                color: white;
            }

        .bajarCombo {
            margin-top: 30px !important;
        }

        #bloqueCombitoyButton {
            margin-top: -30px !important;
        }

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            right: 0px;
            bottom: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(img/loadingPageSantanatura.gif) center no-repeat #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="assets/css/material-dashboard.css?v1" rel="stylesheet" />
    <link href="assets/css/google-roboto-300-700.css" rel="stylesheet" />

    <h1 style="text-align: center; margin-top: 100px; display: none">BONUSES</h1>
    <div style="background-color: #EEEEEE">
        <%--<div style="border: 1px solid; display: none">
            <br />
            <div class="col-12">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <table class="customers table-responsive" style="margin-top: 50px; margin-bottom: 20px">
                            <tr>
                                <th style="width: 300px">PERIODO</th>
                                <td style="width: 300px">ABRIL</td>
                            </tr>

                            <tr>
                                <th style="width: 300px">UNILEVEL</th>
                                <td style="width: 300px">
                                    <asp:Label runat="server" ID="txtUnilevel"></asp:Label></td>
                            </tr>
                            <tr>
                                <th style="width: 300px">AFILIACIONES</th>
                                <td style="width: 300px">
                                    <asp:Label runat="server" ID="txtAfiliacion"></asp:Label></td>
                            </tr>
                            <tr>
                                <th style="width: 300px">TIBURON</th>
                                <td style="width: 300px">
                                    <asp:Label runat="server" ID="txtTiburon"></asp:Label></td>
                            </tr>
                            <tr>
                                <th style="width: 300px">LOGRO BRONCE</th>
                                <td style="width: 300px">
                                    <asp:Label runat="server" ID="txtBronce"></asp:Label></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <table class="customers table-responsive" style="margin-top: 50px; margin-bottom: 20px">
                            <tr>
                                <th style="width: 300px">CONSUMO INTELIGENTE</th>
                                <td style="width: 300px">
                                    <asp:Label runat="server" ID="txtCI"></asp:Label></td>
                            </tr>
                            <tr>
                                <th style="width: 300px">CONSULTORES</th>
                                <td style="width: 300px">
                                    <asp:Label runat="server" ID="txtCon"></asp:Label></td>
                            </tr>
                            <tr>
                                <th style="width: 300px">BONO ESCOLARIDAD</th>
                                <td style="width: 300px">
                                    <asp:Label runat="server" ID="txtEscolaridad"></asp:Label></td>
                            </tr>
                            <tr>
                                <th style="width: 300px">BONO MERCADEO</th>
                                <td style="width: 300px">
                                    <asp:Label runat="server" ID="txtMercadeo"></asp:Label></td>
                            </tr>
                            <tr>
                                <th style="width: 300px">COMISION TOTAL</th>
                                <td style="width: 300px">
                                    <asp:Label runat="server" ID="txtMonto"></asp:Label></td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
            <br />
        </div>--%>

        <div class="header text-center" style="padding-top: 90px; display:none">
            <h2 class="title" id="title_1" runat="server" style="font-family: -webkit-body;">Commission Statistics</h2>
        </div>

        <div id="bloqueCombitoyButton" class="row" style="display: none">
            <div class="col-8">
                <asp:DropDownList runat="server" ID="cboSocios" CssClass="form-contrl js-example-templating" Width="235px" />
            </div>
            <div class="col-4 bajarCombo">
                <button type="button" id="btnComisionGrafica" runat="server" class="btnObtener">Get</button>
            </div>
        </div>


        <div class="container-fluid">

            <div class="row" id="bloqueRetencion" style="display:none">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="cadetblue">
                            <i class="material-icons">assignment</i>
                        </div>
                        <h3 class="card-title">Retention</h3>
                        <div class="card-content">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Type</th>
                                            <th class="text-left">Total</th>
                                            <th class="text-left">Assets</th>
                                            <th class="text-left">New</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left">Direct partner</td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtTotalSocioDirecto" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtActivosSociosDirecto" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtNuevosSociosDirecto" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Direct Consultant</td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtTotalConsultorDirecto" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtActivosConsultorDirecto" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtNuevosConsultorDirecto" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Smart Consumer Direct</td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtTotalCIDirecto" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtActivoCIDirecto" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtNuevoCIDirecto" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Network Partner</td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtTotalSocioRed" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtActivoSocioRed" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtNuevoSocioRed" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Network Consultant</td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtTotalConsultorRed" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtActivoConsultorRed" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtNuevoConsultorRed" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Smart Network Consultant</td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtTotalCIRed" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtActivoCIRed" /></td>
                                            <td class="text-left">
                                                <asp:Label runat="server" ID="txtNuevoCIRed" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="display: none">
                <div class="col-md-4" style="display: none">
                    <div class="card card-chart">
                        <div class="card-header" data-background-color="rose">
                            <div id="roundedLineChart" class="ct-chart"></div>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Rounded Line Chart</h4>
                            <p class="category">Line Chart</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="display: none">
                    <div class="card card-chart">
                        <div class="card-header" data-background-color="orange">
                            <div id="straightLinesChart" class="ct-chart"></div>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Straight Lines Chart</h4>
                            <p class="category">Line Chart with Points</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="display: none">
                    <div class="card card-chart">
                        <div class="card-header" data-background-color="blue">
                            <div id="simpleBarChart" class="ct-chart"></div>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Simple Bar Chart</h4>
                            <p class="category">Bar Chart</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none">
                <div id="colouredLineChart" class="col-md-6" style="margin-left: 54px; display: none;">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="blue">
                            <i class="material-icons">timeline</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Progressive General Bonus Line
                            </h4>
                        </div>
                        <div id="colouredRoundedLineChart" class="ct-chart"></div>
                        <div class="card-footer">
                            <h6>Legend</h6>
                            <i class="fa fa-circle text-info"></i>Montos[<asp:Label runat="server" ID="txtMontosLineaGeneral" />]
                        </div>
                    </div>
                </div>
                <div class="col-md-5" style="display: none">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="red">
                            <i class="material-icons">pie_chart</i>
                        </div>
                        <div class="card-content" style="padding-bottom: 2px;">
                            <h4 class="card-title">Bonus Percentage</h4>
                        </div>
                        <div id="chartPreferences" class="ct-chart"></div>
                        <div class="card-footer">
                            <h6>Legend</h6>
                            <i class="fa fa-circle text-info"></i>Unilevel(<asp:Label runat="server" ID="txtUNI" />%)
                        <i class="fa fa-circle text-warning"></i>Fast Start(<asp:Label runat="server" ID="txtFS" />%)
                        <i class="fa fa-circle text-danger"></i>Shark(<asp:Label runat="server" ID="txtTIBU" />%)
                        <i class="fa fa-circle text-success"></i>Bronze(<asp:Label runat="server" ID="txtBRON" />%)
                        <i class="fa fa-circle text-gray"></i>Smart Consumer(<asp:Label runat="server" ID="txtCIN" />%)
                        <i class="fa fa-circle"></i>Consultant(<asp:Label runat="server" ID="txtCONSUL" />%)
                        <i class="fa fa-circle" style="color: blueviolet"></i>Scholarship(<asp:Label runat="server" ID="txtESCO" />%)
                        <i class="fa fa-circle" style="color: chocolate"></i>Marketing(<asp:Label runat="server" ID="txtMERCA" />%)
                        <i class="fa fa-circle" style="color: gold"></i>Matrix(<asp:Label runat="server" ID="txtMATRI" />%)
                        </div>
                    </div>
                </div>
            </div>
            <div id="idColouredBarsChart" class="row" style="display: flex; justify-content: center; display: none">
                <div class="col-md-7" style="display: none">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="blue">
                            <i class="material-icons">timeline</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Progressive line of Bonuses 
                            </h4>
                        </div>
                        <div id="colouredBarsChart" class="ct-chart"></div>
                        <div class="card-footer">
                            <h6>Legend</h6>
                            <i class="fa fa-circle text-info"></i>Unilevel [<asp:Label runat="server" ID="txtProgreUni" />]
                        <br />
                            <i class="fa fa-circle text-warning"></i>Fast Start [<asp:Label runat="server" ID="txtProgreFast" />]<br />
                            <i class="fa fa-circle text-danger"></i>Smart Consumer [<asp:Label runat="server" ID="txtProgreCI" />]<br />
                            <i class="fa fa-circle text-success"></i>Consultant [<asp:Label runat="server" ID="txtProgreCon" />]
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <svg id="efectoGusanito" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" style="margin-top: -20px;">
            <path fill="#EEEEEE" fill-opacity="1" d="M0,96L30,85.3C60,75,120,53,180,74.7C240,96,300,160,360,160C420,160,480,96,540,90.7C600,85,660,139,720,154.7C780,171,840,149,900,128C960,107,1020,85,1080,101.3C1140,117,1200,171,1260,165.3C1320,160,1380,96,1410,64L1440,32L1440,0L1410,0C1380,0,1320,0,1260,0C1200,0,1140,0,1080,0C1020,0,960,0,900,0C840,0,780,0,720,0C660,0,600,0,540,0C480,0,420,0,360,0C300,0,240,0,180,0C120,0,60,0,30,0L0,0Z"></path>
        </svg>

        <div id="bloqueDetalleComision" style="background: floralwhite; padding-top: 120px; margin-top: -220px; padding-right: 15px; padding-left: 15px">

            <h1 id="DetalleDeComision" runat="server" style="text-align: center; font-size: 2.8em;">Commission Detail
            </h1>
            <div id="bloqueCombitoyButtonPeriodo" class="row">
                <div>
                    <asp:DropDownList runat="server" ID="ddlPais" CssClass="form-contrl js-example-templating" Width="235px" />
                </div>
                <div>
                    <asp:DropDownList runat="server" ID="ddlPeriodo" CssClass="form-contrl js-example-templating" Width="235px" />
                </div>
                <div class="bajarCombo">
                    <button type="button" id="btnObtenerPeriodo" class="btnObtener">Get</button>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">

                    <div id="bloqueBono1" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <table class="customers" style="margin-top: 50px; margin-bottom: 20px">
                            <%--        <tr>
                <th>Company</th>
                <th>Contact</th>
            </tr>--%>
                            <tr>
                                <th id="label_1" runat="server">PERIOD</th>
                                <td>
                                    <asp:Label runat="server" ID="txtNomPeriodo"></asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_2" runat="server">UNILEVEL BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtUnilevel">0</asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_3" runat="server">FAST START BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtAfiliacion">0</asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_4" runat="server">SHARK BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtTiburon">0</asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_5" runat="server">AREX BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtBronce">0</asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_6" runat="server">GRATI BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtGrati">0</asp:Label></td>
                            </tr>
                        </table>
                    </div>
                    <div id="bloqueBono2" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <table class="customers" style="margin-top: 50px; margin-bottom: 20px">
                            <tr>
                                <th id="label_7" runat="server">INTELLIGENT CONSUMER BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtCI">0</asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_8" runat="server">CONSULTANT BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtCon">0</asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_9" runat="server">SCHOOL BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtEscolaridad">0</asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_10" runat="server">MARKETING BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtMercadeo">0</asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_11" runat="server">MATRIX BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtMatricial">0</asp:Label></td>
                            </tr>
                            <tr>
                                <th id="label_12" runat="server">FAMILY BONUS</th>
                                <td>
                                    <asp:Label runat="server" ID="txtFamiliar">0</asp:Label></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xs-12" style="display: flex; justify-content: center">
                        <p runat="server" id="label_13" style="margin-top: auto; margin-bottom: auto; margin-right: 10px; font-family: 'Dancing Script', cursive; font-size: 25px;">
                            Total commission:
                        <asp:Label runat="server" ID="txtComiTotal">0</asp:Label>
                        </p>
                    </div>

                </div>
            </div>
            <div id="bloqueRow" class="row" style="margin-top: 50px;">
                <div id="bloqueBonoUnilevel" class="col-md-6" style="padding-left: 32px;">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="cadetblue">
                            <i class="material-icons">assignment</i>
                        </div>
                        <h3 runat="server" id="label_14" class="card-title">Unilevel bonus</h3>
                        <div class="card-content">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th runat="server" id="label_15" class="text-center">Level</th>
                                            <th runat="server" id="label_16" class="text-left">Amount S/.</th>
                                            <th runat="server" id="label_17" class="text-left">Percentage %</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI1" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI1" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">2</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI2" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI2" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">3</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI3" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI3" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">4</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI4" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI4" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">5</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI5" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI5" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">6</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI6" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI6" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">7</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI7" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI7" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">8</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI8" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI8" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">9</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI9" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI9" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">10</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI10" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI10" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">11</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI11" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI11" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">12</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI12" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI12" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">13</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI13" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI13" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">14</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI14" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI14" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">15</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtUNI15" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORUNI15" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="1"></td>
                                            <td class="td-total" runat="server" id="label_18">Total amount:
                                            </td>
                                            <td class="td-price">
                                                <small>S/.</small><asp:Label runat="server" ID="txtSumaUnilevel" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="bloqueFastStart" class="col-md-6" style="padding-right: 32px;">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="cadetblue">
                            <i class="material-icons">assignment</i>
                        </div>
                        <h3 class="card-title">Fast Start</h3>
                        <div class="card-content">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th runat="server" id="label_19" class="text-center">Level</th>
                                            <th runat="server" id="label_20" class="text-left">Amount S/.</th>
                                            <th runat="server" id="label_21" class="text-left">Percentage %</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtNivel1" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORAFI1" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">2</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtNivel2" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORAFI2" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">3</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtNivel3" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORAFI3" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">4</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtNivel4" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORAFI4" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">5</td>
                                            <td class="text-left">S/.
                                            <asp:Label runat="server" ID="txtNivel5" /></td>
                                            <td class="text-left">%
                                            <asp:Label runat="server" ID="txtPORAFI5" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="1"></td>
                                            <td runat="server" id="label_22" class="td-total">Total amount:
                                            </td>
                                            <td class="td-price">
                                                <small>S/.</small><asp:Label runat="server" ID="txtSumaAfiliacion" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    
    <script src="../assets/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/js/material.min.js?v1" type="text/javascript"></script>
    <script src="../assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    
    <script src="assets/js/chartist.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="js/Comisiones4.js?v8"></script>
    <script src="assets/js/jquery.datatables.js"></script>
    <script src="assets/js/dataTables.bootstrap.js"></script>
    <script src="../assets/js/jquery.tagsinput.js"></script>
    <script src="../assets/js/material-dashboard.js"></script>

    <script>
        $(".js-example-templating").select2({
        });

        window.onload = function () {
            document.getElementById("clicBonif").style.color = 'white';
            document.getElementById("clicBonif").style.borderBottom = '3px solid white';
        }

        //$(document).ready(function () {
        //    demo2.initCharts();
        //});
    </script>
</asp:Content>
