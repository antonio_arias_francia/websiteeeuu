﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="CompletePreRegistration.aspx.cs" Inherits="SantaNaturaNetworkV3.CompletePreRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="assets/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/r-2.2.7/datatables.min.css" />
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrapv2.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .table tbody tr td:last-child {
            display: flex;
            justify-content: center;
        }

        .sorting_1 {
            background-color: inherit !important;
        }

        .table thead tr th {
            text-align: center;
        }

        .table tbody tr td {
            text-align: center;
        }

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(img/loadingPageSantanatura.gif) center no-repeat #fff;
        }

        .container2 {
            max-width: 400px !important;
        }

        .select2-container {
            width: 100% !important;
        }

        .btn-ft {
            font-size: 1.7rem !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container" style="margin-top: 100px; margin-bottom: 50px">
        <h1 style="text-align: center; margin-bottom: 50px" id="h1" runat="server">COMPLETE REGISTRATION</h1>
        <div class="row">
            <div class="col-lg-12">
                <table id="table_id" class="table table-condensed display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th id="th1" runat="server">DATE</th>
                            <th id="th2" runat="server">DOCUMENT</th>
                            <th id="th3" runat="server">NAMES</th>
                            <th id="th4" runat="server">LASTNAME</th>
                            <th id="th5" runat="server">UPLINE</th>
                            <th id="th6" runat="server">PACKAGE</th>
                            <th id="th7" runat="server">UPDATE</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="container2 container modal-dialog" id="modalTamano" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 style="text-align: center; font-weight: bold" class="modal-title" id="exampleModalLabel">Actualizar Registro</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body1">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label id="txNombre" runat="server">NAMES :</label>
                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    <label style="margin-top: 7px;" id="txTipoC" runat="server">CUSTOMER TYPE :</label>
                                    <select runat="server" id="ddlTC" class="form-control text-uppercase">
                                            <option value="01">Socio</option>
                                            <option value="05">Consultor</option>
                                            <option value="03">C. Inteligente</option>
                                    </select>
                                    <label style="margin-top: 7px;" id="txUpline" runat="server">UPLINE :</label>
                                    <select runat="server" id="ddlUpline" class="form-control js-example-templating"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnCancelar" runat="server" type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button id="btnActualizar" runat="server" type="button" class="btn btn-success">Update</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->


    <script src="js/bootstrap4.min.js" type="text/javascript"></script>
    <script src="js/file-uploadv1.js" type="text/javascript"></script>
    <script src="js/proyecto2/jqueryDataTablesPremioSocios.js" type="text/javascript"></script>
    <script src="js/proyecto2/estiloTablasPremioSocios.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.7/dist/sweetalert2.all.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js" type="text/javascript"></script>
    <script src="js/jquery.numeric.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/CompletarPreRegistro.js?v1"></script>
    <script>
        $(document).ready(function () {
            $(".js-example-templating").select2({
            });
        });
    </script>
</asp:Content>
