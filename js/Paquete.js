﻿//DEFINIR VARIABLES
var tabla, aaf, data, estados, idTexto;   

$(function () {
    $("#exampleModal input").autocomplete({
        source: function (request, responce) {
            $.ajax({
                url: "Autocompletado.asmx/FiltrarNombreProductos",
                method: "post",
                CORS: true,
                contentType: "application/json;charset=utf-8",
                data: "{ 'palabra': '" + request.term + "'}",
                dataType: 'json',
                success: function (data) {
                    responce(data.d);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("In The ERROR");
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        }
    });
}); 

//LISTADO DE PRODUCTOS
function addRowDT(obj) {
    tabla = $("#tbl_paquetes").DataTable();
    tabla.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].estado == true) {
            estados = "Activo";
        } else {
            estados = "Desactivado";
        }
        tabla.fnAddData([
            obj[i].idPaquete,
            obj[i].nombre,
            estados,
            '<button id="Actualizar" value="Actualizar" title="Actualizar" class="btn btn-primary btn-update" data-toggle="modal" data-target="#exampleModal"><i class="far fa-edit"></i></button>',
            obj[i].observacion,
            obj[i].paquetePais,
            obj[i].estado,
            obj[i].estado,
            obj[i].idProductoPais1,
            obj[i].idProductoPais2,
            obj[i].idProductoPais3,
            obj[i].idProductoPais4,
            obj[i].idProductoPais5,
            obj[i].idProductoPais6,
            obj[i].idProductoPais7,
            obj[i].idProductoPais8,
            obj[i].idProductoPais9,
            obj[i].idProductoPais10,
            obj[i].cantidad1,
            obj[i].cantidad2,
            obj[i].cantidad3,
            obj[i].cantidad4,
            obj[i].cantidad5,
            obj[i].cantidad6,
            obj[i].cantidad7,
            obj[i].cantidad8,
            obj[i].cantidad9,
            obj[i].cantidad10,
            obj[i].nombre1Peru,
            obj[i].nombre2Peru,
            obj[i].nombre3Peru,
            obj[i].nombre4Peru,
            obj[i].nombre5Peru,
            obj[i].nombre6Peru,
            obj[i].nombre7Peru,
            obj[i].nombre8Peru,
            obj[i].nombre9Peru,
            obj[i].nombre10Peru
        ]);
    }
}

// IDBOLIVIAPAIS data[18]

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "GenerarPacketeProducto.aspx/ListaPaquetes",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            console.log(data.d);
            addRowDT(data.d);
        }
    });
}

//CARGAR DATOS EN MODAL
function fillModalData() {
    $("#txtNombrePaquete").val(data[1]);
    $("#txtObservacion").val(data[4]);
    $("#toogleEEUU").prop('checked', data[7]).change();
    $("#txtIDEEUU1").val(data[28]);
    $("#txtIDEEUU2").val(data[29]);
    $("#txtIDEEUU3").val(data[30]);
    $("#txtIDEEUU4").val(data[31]);
    $("#txtIDEEUU5").val(data[32]);
    $("#txtIDEEUU6").val(data[33]);
    $("#txtIDEEUU7").val(data[34]);
    $("#txtIDEEUU8").val(data[35]);
    $("#txtIDEEUU9").val(data[36]);
    $("#txtIDEEUU10").val(data[37]);
    $("#txtPrecioEEUU1").val(data[18]);
    $("#txtPrecioEEUU2").val(data[19]);
    $("#txtPrecioEEUU3").val(data[20]);
    $("#txtPrecioEEUU4").val(data[21]);
    $("#txtPrecioEEUU5").val(data[22]);
    $("#txtPrecioEEUU6").val(data[23]);
    $("#txtPrecioEEUU7").val(data[24]);
    $("#txtPrecioEEUU8").val(data[25]);
    $("#txtPrecioEEUU9").val(data[26]);
    $("#txtPrecioEEUU10").val(data[27]);
}

// ABRIR MODAL
$("#btnNuevoPaquete").click(function (e) {
    $("#btnRegistrar").show();
    $("#btnActualizar").hide();
    $("#exampleModal input").val("");
    insertarPrecio();
    $("#exampleModal textarea").val("");
    $('input').iCheck('uncheck');
    $("#exampleModal input[type='checkbox']").prop('checked', false).change();
    // other initialization here, if you want to
    
    $('.solo-numero').numeric();
});

function insertarPrecio() {
    $("#txtPrecioEEUU1").val("0");
    $("#txtPrecioEEUU2").val("0");
    $("#txtPrecioEEUU3").val("0");
    $("#txtPrecioEEUU4").val("0");
    $("#txtPrecioEEUU5").val("0");
    $("#txtPrecioEEUU6").val("0");
    $("#txtPrecioEEUU7").val("0");
    $("#txtPrecioEEUU8").val("0");
    $("#txtPrecioEEUU9").val("0");
    $("#txtPrecioEEUU10").val("0");
}

//FUNCIONES PARA REGISTRAR
$("#btnRegistrar").click(function (e) {
    var nombre = $("#txtNombrePaquete").val();
    var idEEUU1 = $("#txtIDEEUU1").val();
    var idEEUU2 = $("#txtIDEEUU2").val();
    var idEEUU3 = $("#txtIDEEUU3").val();
    var idEEUU4 = $("#txtIDEEUU4").val();
    var idEEUU5 = $("#txtIDEEUU5").val();
    var idEEUU6 = $("#txtIDEEUU6").val();
    var idEEUU7 = $("#txtIDEEUU7").val();
    var idEEUU8 = $("#txtIDEEUU8").val();
    var idEEUU9 = $("#txtIDEEUU9").val();
    var idEEUU10 = $("#txtIDEEUU10").val();
    var precioEEUU1 = $("#txtPrecioEEUU1").val();
    var precioEEUU2 = $("#txtPrecioEEUU2").val();
    var precioEEUU3 = $("#txtPrecioEEUU3").val();
    var precioEEUU4 = $("#txtPrecioEEUU4").val();
    var precioEEUU5 = $("#txtPrecioEEUU5").val();
    var precioEEUU6 = $("#txtPrecioEEUU6").val();
    var precioEEUU7 = $("#txtPrecioEEUU7").val();
    var precioEEUU8 = $("#txtPrecioEEUU8").val();
    var precioEEUU9 = $("#txtPrecioEEUU9").val();
    var precioEEUU10 = $("#txtPrecioEEUU10").val();

    e.preventDefault();
    if (nombre == "") {
        FaltaNombre();
    } else if ((idEEUU1 != "" & precioEEUU1 == "0") | (idEEUU1 == "" & precioEEUU1 != "0") | precioEEUU1 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU2 != "" & precioEEUU2 == "0") | (idEEUU2 == "" & precioEEUU2 != "0") | precioEEUU2 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU3 != "" & precioEEUU3 == "0") | (idEEUU3 == "" & precioEEUU3 != "0") | precioEEUU3 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU4 != "" & precioEEUU4 == "0") | (idEEUU4 == "" & precioEEUU4 != "0") | precioEEUU4 == "") {
        FaltaDatoPeru();
    } else if ((idEEUU5 != "" & precioEEUU5 == "0") | (idEEUU5 == "" & precioEEUU5 != "0") | precioEEUU5 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU6 != "" & precioEEUU6 == "0") | (idEEUU6 == "" & precioEEUU6 != "0") | precioEEUU6 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU7 != "" & precioEEUU7 == "0") | (idEEUU7 == "" & precioEEUU7 != "0") | precioEEUU7 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU8 != "" & precioEEUU8 == "0") | (idEEUU8 == "" & precioEEUU8 != "0") | precioEEUU8 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU9 != "" & precioEEUU9 == "0") | (idEEUU9 == "" & precioEEUU9 != "0") | precioEEUU9 == "") {
        FaltaDatoPeru();
    } else if ((idEEUU10 != "" & precioEEUU10 == "0") | (idEEUU10 == "" & precioEEUU10 != "0") | precioEEUU10 == "") {
        FaltaDatoEEUU();
    }
    else {
        RegistroPaquete();
    }
});

function RegistroPaquete() {
    var nombre = $("#txtNombrePaquete").val();
    var estaEEUU = document.getElementById("toogleEEUU").checked;
    var observacion = $("#txtObservacion").val();
    var idEEUU1 = $("#txtIDEEUU1").val();
    var idEEUU2 = $("#txtIDEEUU2").val();
    var idEEUU3 = $("#txtIDEEUU3").val();
    var idEEUU4 = $("#txtIDEEUU4").val();
    var idEEUU5 = $("#txtIDEEUU5").val();
    var idEEUU6 = $("#txtIDEEUU6").val();
    var idEEUU7 = $("#txtIDEEUU7").val();
    var idEEUU8 = $("#txtIDEEUU8").val();
    var idEEUU9 = $("#txtIDEEUU9").val();
    var idEEUU10 = $("#txtIDEEUU10").val();
    var precioEEUU1 = $("#txtPrecioEEUU1").val();
    var precioEEUU2 = $("#txtPrecioEEUU2").val();
    var precioEEUU3 = $("#txtPrecioEEUU3").val();
    var precioEEUU4 = $("#txtPrecioEEUU4").val();
    var precioEEUU5 = $("#txtPrecioEEUU5").val();
    var precioEEUU6 = $("#txtPrecioEEUU6").val();
    var precioEEUU7 = $("#txtPrecioEEUU7").val();
    var precioEEUU8 = $("#txtPrecioEEUU8").val();
    var precioEEUU9 = $("#txtPrecioEEUU9").val();
    var precioEEUU10 = $("#txtPrecioEEUU10").val();

    var obja = JSON.stringify({
        nombrePa: nombre, observacionPa: observacion, paisPa: "08", estadoPa: estaEEUU,
        precioPaEEUU1: precioEEUU1, precioPaEEUU2: precioEEUU2, precioPaEEUU3: precioEEUU3, precioPaEEUU4: precioEEUU4, precioPaEEUU5: precioEEUU5,
        precioPaEEUU6: precioEEUU6, precioPaEEUU7: precioEEUU7, precioPaEEUU8: precioEEUU8, precioPaEEUU9: precioEEUU9, precioPaEEUU10: precioEEUU10,
        idEEUUPa1: idEEUU1, idEEUUPa2: idEEUU2, idEEUUPa3: idEEUU3, idEEUUPa4: idEEUU4, idEEUUPa5: idEEUU5,
        idEEUUPa6: idEEUU6, idEEUUPa7: idEEUU7, idEEUUPa8: idEEUU8, idEEUUPa9: idEEUU9, idEEUUPa10: idEEUU10
    });
    
    $.ajax({
        type: "POST",
        url: "GenerarPacketeProducto.aspx/RegistrarPaquete",
        data: obja,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (response) {
            console.log(response);
            alertme();
        }
    });
}

//FUNCIONES PARA ACTUALIZAR
$(document).on('click', '.btn-update', function (e) {
    $("#btnRegistrar").hide();
    $("#btnActualizar").show();
    $("#exampleModal input[type='checkbox']").prop('checked', false).change();
    e.preventDefault();
    var row = $(this).parent().parent()[0];
    data = tabla.fnGetData(row);
    console.log(data);
    fillModalData();
});

function updateDataAjax() {

    var idpaquetePrin = data[0];
    var idpaquetePEEUU = data[5];
    
    var nombre = $("#txtNombrePaquete").val();
    var estaEEUU = document.getElementById("toogleEEUU").checked;
    var observacion = $("#txtObservacion").val();
    var idEEUU1 = $("#txtIDEEUU1").val();
    var idEEUU2 = $("#txtIDEEUU2").val();
    var idEEUU3 = $("#txtIDEEUU3").val();
    var idEEUU4 = $("#txtIDEEUU4").val();
    var idEEUU5 = $("#txtIDEEUU5").val();
    var idEEUU6 = $("#txtIDEEUU6").val();
    var idEEUU7 = $("#txtIDEEUU7").val();
    var idEEUU8 = $("#txtIDEEUU8").val();
    var idEEUU9 = $("#txtIDEEUU9").val();
    var idEEUU10 = $("#txtIDEEUU10").val();
    var precioEEUU1 = $("#txtPrecioEEUU1").val();
    var precioEEUU2 = $("#txtPrecioEEUU2").val();
    var precioEEUU3 = $("#txtPrecioEEUU3").val();
    var precioEEUU4 = $("#txtPrecioEEUU4").val();
    var precioEEUU5 = $("#txtPrecioEEUU5").val();
    var precioEEUU6 = $("#txtPrecioEEUU6").val();
    var precioEEUU7 = $("#txtPrecioEEUU7").val();
    var precioEEUU8 = $("#txtPrecioEEUU8").val();
    var precioEEUU9 = $("#txtPrecioEEUU9").val();
    var precioEEUU10 = $("#txtPrecioEEUU10").val();
    

    var obj = JSON.stringify({
        idPaquete: idpaquetePrin, idPaquetePaisEEUU: idpaquetePEEUU, nombrePa: nombre,
        observacionPa: observacion, estadoEEUUPa: estaEEUU,
        precioPaEEUU1: precioEEUU1, precioPaEEUU2: precioEEUU2, precioPaEEUU3: precioEEUU3, precioPaEEUU4: precioEEUU4, precioPaEEUU5: precioEEUU5,
        precioPaEEUU6: precioEEUU6, precioPaEEUU7: precioEEUU7, precioPaEEUU8: precioEEUU8, precioPaEEUU9: precioEEUU9, precioPaEEUU10: precioEEUU10,
        idEEUUPa1: idEEUU1, idEEUUPa2: idEEUU2, idEEUUPa3: idEEUU3, idEEUUPa4: idEEUU4, idEEUUPa5: idEEUU5,
        idEEUUPa6: idEEUU6, idEEUUPa7: idEEUU7, idEEUUPa8: idEEUU8, idEEUUPa9: idEEUU9, idEEUUPa10: idEEUU10
    });
    
    $.ajax({
        type: "POST",
        url: "GenerarPacketeProducto.aspx/ActualizarPaquete",
        data: obj,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (response) {
            console.log(response);
            Swal.fire({
                title: 'Perfecto!',
                text: 'Producto Actualizado',
                type: "success"
            }).then(function () {
                window.location = "GenerarPacketeProducto.aspx";
            });
        }
    });
}

$("#btnActualizar").click(function (e) {
    
    var nombre = $("#txtNombrePaquete").val();
    var idEEUU1 = $("#txtIDEEUU1").val();
    var idEEUU2 = $("#txtIDEEUU2").val();
    var idEEUU3 = $("#txtIDEEUU3").val();
    var idEEUU4 = $("#txtIDEEUU4").val();
    var idEEUU5 = $("#txtIDEEUU5").val();
    var idEEUU6 = $("#txtIDEEUU6").val();
    var idEEUU7 = $("#txtIDEEUU7").val();
    var idEEUU8 = $("#txtIDEEUU8").val();
    var idEEUU9 = $("#txtIDEEUU9").val();
    var idEEUU10 = $("#txtIDEEUU10").val();
    var precioEEUU1 = $("#txtPrecioEEUU1").val();
    var precioEEUU2 = $("#txtPrecioEEUU2").val();
    var precioEEUU3 = $("#txtPrecioEEUU3").val();
    var precioEEUU4 = $("#txtPrecioEEUU4").val();
    var precioEEUU5 = $("#txtPrecioEEUU5").val();
    var precioEEUU6 = $("#txtPrecioEEUU6").val();
    var precioEEUU7 = $("#txtPrecioEEUU7").val();
    var precioEEUU8 = $("#txtPrecioEEUU8").val();
    var precioEEUU9 = $("#txtPrecioEEUU9").val();
    var precioEEUU10 = $("#txtPrecioEEUU10").val();

    e.preventDefault();
    if (nombre == "") {
        FaltaNombre();
    } else if ((idEEUU1 != "" & precioEEUU1 == "0") | (idEEUU1 == "" & precioEEUU1 != "0") | precioEEUU1 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU2 != "" & precioEEUU2 == "0") | (idEEUU2 == "" & precioEEUU2 != "0") | precioEEUU2 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU3 != "" & precioEEUU3 == "0") | (idEEUU3 == "" & precioEEUU3 != "0") | precioEEUU3 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU4 != "" & precioEEUU4 == "0") | (idEEUU4 == "" & precioEEUU4 != "0") | precioEEUU4 == "") {
        FaltaDatoPeru();
    } else if ((idEEUU5 != "" & precioEEUU5 == "0") | (idEEUU5 == "" & precioEEUU5 != "0") | precioEEUU5 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU6 != "" & precioEEUU6 == "0") | (idEEUU6 == "" & precioEEUU6 != "0") | precioEEUU6 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU7 != "" & precioEEUU7 == "0") | (idEEUU7 == "" & precioEEUU7 != "0") | precioEEUU7 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU8 != "" & precioEEUU8 == "0") | (idEEUU8 == "" & precioEEUU8 != "0") | precioEEUU8 == "") {
        FaltaDatoEEUU();
    } else if ((idEEUU9 != "" & precioEEUU9 == "0") | (idEEUU9 == "" & precioEEUU9 != "0") | precioEEUU9 == "") {
        FaltaDatoPeru();
    } else if ((idEEUU10 != "" & precioEEUU10 == "0") | (idEEUU10 == "" & precioEEUU10 != "0") | precioEEUU10 == "") {
        FaltaDatoEEUU();
    }
    else {
        updateDataAjax();
    }

});

//ALERTAS
function alertme() {
    Swal.fire({
        title: 'Perfecto!',
        text: 'Paquete Registrado',
        type: "success"
    }).then(function () {
        window.location = "GenerarPacketeProducto.aspx";
    });
}
function FaltaNombre() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'No registró el nombre del paquete',
        type: "error"
    });
}
function FaltaDatoEEUU() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'No registró un dato del paquete Perú',
        type: "error"
    });
}

sendDataAjax();