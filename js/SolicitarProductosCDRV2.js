﻿var tabla, data, estados, tipoPag, tabla2, data2, tabla3, data3, contador, listaPro, data4, okRegistro, okActualiza, listaPSRegistro, precioTotalCDR = 0;

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "SolicitarStockCDR.aspx/ListarSolicitudesGeneradas",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            console.log(data.d);
            addRowDT(data.d);
        }
    });
}

function sendDataAjaxRegistrar() {
    $.ajax({
        type: "POST",
        url: "SolicitarStockCDR.aspx/ListarProductosPedidoCDR",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data2) {
            console.log(data2.d);
            addRowDTRegistrar(data2.d);
            listaPro = data2.d;
        }
    });
}

function sendDataAjaxDetalle(idSoli) {

    var obj = JSON.stringify({ idSolicitud: idSoli });
    $.ajax({
        type: "POST",
        url: "SolicitarStockCDR.aspx/ListarProductosxSolicitud",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data3) {
            console.log(data3.d);
            addRowDTDetalle(data3.d);
        }
    });
}

function addRowDT(obj) {
    tabla = $("#tbl_cdr").DataTable();
    tabla.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        tabla.fnAddData([
            obj[i].Fila,
            obj[i].FechaSolicitud,
            obj[i].CDRPS,
            '<button value="Detalle" title="Detalle" type="button" class="btn btn-primary btn-deta" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-search"></i></button>',
            obj[i].IdSolicitud
        ]);
    }
}

function addRowDTRegistrar(obj) {
    tabla2 = $("#tbl_registro").DataTable({
        "bPaginate": false,
        "bSort": false
    });
    contador = 0;
    tabla2.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        contador++;
        var imagenProducto = obj[i].Imagen;
        tabla2.fnAddData([
            obj[i].Fila,
            obj[i].NombreProducto,
            '<img src="products/' + imagenProducto + '" style="height: 80px">',
            obj[i].IDPS,
            '<input type="text" id="cant' + i + '" name="" class="form-control daterange" autocomplete="nope" style="background-color: lightgreen; width:90px" value="0">',
            obj[i].IDProducto
        ]);
    }
}

function addRowDTDetalle(obj) {
    tabla3 = $("#tbl_detalle").DataTable({
        "bPaginate": false,
        "bSort": false
    });
    tabla3.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        var imagenProducto = obj[i].Imagen;
        var cant = obj[i].Cantidad;
        tabla3.fnAddData([
            obj[i].Fila,
            obj[i].NombreProducto,
            '<img src="products/' + imagenProducto + '" style="height: 80px">',
            obj[i].IDPS,
            '<input type="text" id="cantA' + i + '" name="" class="form-control daterange" style="background-color: lightgreen; width:90px" readonly value="' + cant + '">'
        ]);
    }
}

sendDataAjax();

$("#btnModals").click(function (e) {
    e.preventDefault();
    var tipoCompra = $("#cboTipoPago").val();

    if (tipoCompra == "" | tipoCompra == "0") {
        FaltaTipoPago();
    } else {
        $('#exampleModal2').modal('show');
        sendDataAjaxRegistrar();
    }
});

$(document).on('click', '.btn-deta', function (e) {
    e.preventDefault();
    var row2 = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row2);
    sendDataAjaxDetalle(datax[4]);
});

$("#btnRegistrar").click(function (e) {
    e.preventDefault();
    var myArray = [];
    for (var i = 0; i < contador; i++) {
        var DatosArray = {};
        var canInput = $("#cant" + i + "").val();
        var codigo = listaPro[i].IDProducto;
        var idPeruS = listaPro[i].IDPS;
        var idProPais = listaPro[i].IDProductoXPais;
        var imgP = listaPro[i].Imagen;
        precioTotalCDR = precioTotalCDR + (canInput * listaPro[i].PrecioCDR)
        DatosArray['codigo'] = codigo;
        DatosArray['PS'] = idPeruS;
        DatosArray['imgP'] = imgP;
        DatosArray['cantidad'] = canInput;
        DatosArray['idProductoPais'] = idProPais;
        myArray.push(DatosArray);
    }
    var tipoCompra = $("#cboTipoPago").val();
    var montoLC = $("#txtLineaCredito").text();
    var montoComision = $("#txtSaldoComision").text();
    if (tipoCompra == "3" && parseFloat(montoLC) < precioTotalCDR) {
        MontoFaltante();
    } else if (tipoCompra == "4" && parseFloat(montoComision) < precioTotalCDR) {
        MontoFaltante();
    } else {
        registrarSolicitudCDR(myArray);
    }
});

function registrarSolicitudCDR(myArray) {
    var tipoCompra = $("#cboTipoPago").val();
    var montoLC = $("#txtLineaCredito").text();
    var montoComision = $("#txtSaldoComision").text();
    var obj = JSON.stringify({
        prueba: myArray, TipoPagoS: tipoCompra, montoPagoS: precioTotalCDR,
        montoLCS: parseFloat(montoLC), montoComisionS: parseFloat(montoComision)
    });
    $.ajax({
        type: "POST",
        url: "SolicitarStockCDR.aspx/RegistrarStock",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (okRegistro) {
            console.log(okRegistro.d);
                Swal.fire({
                    title: 'Perfecto!',
                    text: 'Stock Solicitado',
                    type: "success"
                }).then(function () {
                    if (okRegistro.d == "") {
                        window.location = "SolicitarStockCDR.aspx";
                    } else {
                        window.location = okRegistro.d;
                    }
                    
                });
        }
    });
}

function MontoFaltante() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'No tiene el monto suficiente para este tipo de compra',
        type: "error"
    });
}

function FaltaTipoPago() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Debe seleccionar el Tipo de Pago',
        type: "error"
    });
}