﻿var tabla, data, tabla2, data2, data3;

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "AprobarPedidosCDR.aspx/ListarSolicitudesTotales",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            console.log(data.d);
            addRowDT(data.d);
        }
    });
}

function addRowDT(obj) {
    tabla = $("#tbl_cdr").DataTable();
    tabla.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        var btnActualiza = "";
        if (obj[i].Estado == "Aprobado") { btnActualiza = "style='display:none'"; }
        tabla.fnAddData([
            obj[i].Fila,
            obj[i].FechaSolicitud,
            obj[i].CDRPS,
            obj[i].Estado,
            '<button id="btnAbrirModal" value="Detalle" title="Detalle" type="button" class="btn btn-primary btn-deta" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-search"></i></button>',
            '<button id="btnAprobar" value="Aprobar" title="Aprobar" type="button" class="btn btn-success btn-aprob"' + btnActualiza + '><i class="fas fa-check"></i></button>',
            obj[i].IdSolicitud,
            obj[i].DNICDR
        ]);
    }
}

$(document).on('click', '.btn-aprob', function (e) {
    e.preventDefault();
    var row2 = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row2);
    aprobarSolicitud(datax[6], datax[7]);
});

function aprobarSolicitud(idSoli, dniCDR) {

    var obj = JSON.stringify({ idSolicitud: idSoli, dni: dniCDR });
    $.ajax({
        type: "POST",
        url: "AprobarPedidosCDR.aspx/AprobarSolicitudCDR",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data3) {
            console.log(data3.d);
            Swal.fire({
                title: 'Perfecto!',
                text: 'Solicitud Aprobada',
                type: "success"
            }).then(function () {
                window.location = "AprobarPedidosCDR.aspx";
            });
        }
    });
}

$(document).on('click', '.btn-deta', function (e) {
    e.preventDefault();
    var row2 = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row2);
    $("#txtCDRDetalle").val(datax[2]);
    sendDataAjaxDetalle(datax[6]);
});

function sendDataAjaxDetalle(idSoli) {

    var obj = JSON.stringify({ idSolicitud: idSoli });
    $.ajax({
        type: "POST",
        url: "AprobarPedidosCDR.aspx/ListarProductosxSolicitud",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data2) {
            console.log(data2.d);
            addRowDTDetalle(data2.d);
        }
    });
}

function addRowDTDetalle(obj) {
    tabla3 = $("#tbl_detalle").DataTable({
        "bPaginate": false,
        "bSort": false
    });
    tabla3.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        var imagenProducto = obj[i].Imagen;
        var cant = obj[i].Cantidad;
        tabla3.fnAddData([
            obj[i].Fila,
            obj[i].NombreProducto,
            '<img src="products/' + imagenProducto + '" style="height: 80px">',
            obj[i].IDPS,
            '<input type="text" id="cantA' + i + '" name="" class="form-control daterange" style="background-color: lightgreen; width:90px" readonly value="' + cant + '">'
        ]);
    }
}

sendDataAjax();