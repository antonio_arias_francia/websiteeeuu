﻿var input = document.getElementById('SMedioPago');
var inputTCompra = document.getElementById('STipoCompra');
var inputTienda = document.getElementById('ComboTienda');
var inputTitular = document.getElementById('ddlTitular');
var inputPais = document.getElementById('cboPais');
localStorage['SMedioPago'] = "06";
$("#SMedioPago").val("06");

var ZipCodeGeneral;
/* Luego se debe eliminar para habilitar la seleccion de titular*/
RellenarDatosDeliveryWS();
/**/
var RUC_Validado = 0, ValidarDelivery = 0;
var Ruc_Perfil = "", montoCompraEval = 0, puntosCompraEval = 0, montoTAX = 0, tipoCliente = "", filtro_preregistro = 0, MontoPagoG = 0, handling = 0, lenguaje = "",
    MontoStrPayPal = "", listaPro, flag = 0, idCliente = "", idPedPP = "", _ddd = {}, detail = [];
ObtenerRUC_Perfil();
CambioTipoCliente();
ObtenerTipoCliente();
input.onchange = function () {
    localStorage['SMedioPago'] = this.value;
    GuardarIDCombos('MPago', this.value);
}
inputTCompra.onchange = function () {
    var preregis = $('#cboPreRegistro').val();
    if (preregis != "0" && preregis != "" && preregis != null) {
        if (this.value == "01" || this.value == "02" || this.value == "03" ||
            this.value == "04" || this.value == "05" || this.value == "06" ||
            this.value == "23") {
            if ((tipoCliente == "01" && (this.value == "05" || this.value == "06")) ||
                (tipoCliente != "01" && (this.value != "05" || this.value != "06"))) {
                $('#STipoCompra').val(localStorage['STipoCompra']);
                NoCambioTipoCliente();
            } else {
                localStorage['STipoCompra'] = this.value;
                GuardarIDCombos('TCompra', this.value);
            }
        } else {
            $('#STipoCompra').val(localStorage['STipoCompra']);
            NoCambioPreRegistro();
        }
    } else {
        localStorage['STipoCompra'] = this.value;
        GuardarIDCombos('TCompra', this.value);
    }
}
inputTienda.onchange = function () {
    localStorage['ComboTienda'] = this.value;
    GuardarIDCombos('TiendaS', this.value);
}
inputTitular.onchange = function () {
    localStorage['ddlTitular'] = this.value;
    CargarTitular();
}
inputPais.onchange = function () {
    if (this.value == "01") {
        $("#divDistrito").show();
        $("#lblDepart").text("Department");
        $("#lblProv").text("Province");
    } else {
        $("#divDistrito").hide();
        $("#lblDepart").text("State");
        $("#lblProv").text("County");
    }
}

$.ajax({
    type: "POST",
    url: "EditarPerfil.aspx/GetPais_EEUU",
    data: "{}",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: false,
    success: function (result) {
        $("#cboPais").empty();
        $("#cboPais").append("<option value='0'>------</option>");
        $.each(result.d, function (key, value) {
            $("#cboPais").append($("<option></option>").val(value.Codigo).html(value.Nombre));
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus + ": " + XMLHttpRequest.responseText);
    }
});

$("#cboPais").change(function () {
    var params = new Object();
    params.pais = $("#cboPais").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDepartamentosByPais",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDepartamento").empty();
            $("#cboDepartamento").append("<option value='0'>------</option>");
            CargarProvincia();
            CargarDistrito();
            $.each(result.d, function (key, value) {
                $("#cboDepartamento").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

$("#cboDepartamento").change(function () {

    var params = new Object();
    params.departamento = $("#cboDepartamento").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetZipCodeByDepartamento",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboZipCode").empty();
            $("#cboZipCode").append("<option value='0'>------</option>");
            $.each(result.d, function (key, value) {
                $("#cboZipCode").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

//$("#cboDepartamento").change(function () {
//    var params = new Object();
//    params.departamento = $("#cboDepartamento").val();
//    params = JSON.stringify(params);

//    $.ajax({
//        type: "POST",
//        url: "EditarPerfil.aspx/GetProvinciaByDepartamento",
//        data: params,
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        async: false,
//        success: function (result) {
//            $("#cboProvincia").empty();
//            $("#cboProvincia").append("<option value='0'>--Select--</option>");
//            CargarDistrito();
//            $.each(result.d, function (key, value) {
//                $("#cboProvincia").append($("<option></option>").val(value.Codigo).html(value.Nombre));
//            });
//        },
//        error: function (XMLHttpRequest, textStatus, errorThrown) {
//            alert(textStatus + ": " + XMLHttpRequest.responseText);
//        }
//    });
//});

$("#cboProvincia").change(function () {
    var params = new Object();
    params.provincia = $("#cboProvincia").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDistritoByProvincia",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDistrito").empty();
            $("#cboDistrito").append("<option value='0'>------</option>");
            $.each(result.d, function (key, value) {
                $("#cboDistrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

function CargarTitular() {
    var titular = $('#ddlTitular').val();
    if (titular == "2") {
        RellenarDatosDeliveryWS();
    } else {
        $('#txtNameDelivery').val("");
        $('#txtLastNameDelivery').val("");
        $('#txtDocDelivery').val("");
        $('#txtEmailDelivery').val("");
        $('#txtDirectionDelivery').val("");
        $('#txtPhoneDelivery').val("");
        document.getElementById("txtNameDelivery").readOnly = false;
        document.getElementById("txtLastNameDelivery").readOnly = false;
        document.getElementById("txtDocDelivery").readOnly = false
        document.getElementById("txtEmailDelivery").readOnly = false
        document.getElementById("txtDirectionDelivery").readOnly = false
        document.getElementById("txtPhoneDelivery").readOnly = false
    }
}

function RellenarDatosDeliveryWS() {
    $.ajax({
        type: "POST",
        url: "ShoppingCart.aspx/RellenarDatosDelivery",
        data: "{}",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            $('#txtNameDelivery').val(Datos.Nombres);
            $('#txtLastNameDelivery').val(Datos.Apellidos);
            $('#txtDocDelivery').val(Datos.Documento);
            $('#txtEmailDelivery').val(Datos.Correo);
            $('#txtDirectionDelivery').val(Datos.Direccion);
            $('#txtPhoneDelivery').val(Datos.Celular);
            document.getElementById("txtNameDelivery").readOnly = true;
            document.getElementById("txtLastNameDelivery").readOnly = true;
            document.getElementById("txtDocDelivery").readOnly = true;
            document.getElementById("txtEmailDelivery").readOnly = true;
            document.getElementById("txtPhoneDelivery").readOnly = true;
            document.getElementById("txtDirectionDelivery").readOnly = true;
        }
    });
}

function CargarDepartamentos() {
    var params = new Object();
    params.pais = $("#cboPais").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDepartamentosByPais",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDepartamento").empty();
            $("#cboDepartamento").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboDepartamento").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarProvincia() {
    var params = new Object();
    params.departamento = $("#cboDepartamento").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetProvinciaByDepartamento",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboProvincia").empty();
            $("#cboProvincia").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboProvincia").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarDistrito() {
    var params = new Object();
    params.provincia = $("#cboProvincia").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDistritoByProvincia",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDistrito").empty();
            $("#cboDistrito").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboDistrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CambioTipoCliente() {

    var Tcompra = $('#STipoCompra').val();
    if (Tcompra == "01" || Tcompra == "02" || Tcompra == "03" ||
        Tcompra == "04" || Tcompra == "23") {
        $('#cboTipoCliente').val("01");
    }
    else if (Tcompra == "05") {
        $('#cboTipoCliente').val("05");
    }
    else {
        $('#cboTipoCliente').val("03");
    }
}

function ObtenerRUC_Perfil() {
    var departAf = $('#cboDepartamento').val();
    var obj = JSON.stringify({
        departAfi: departAf
    });
    $.ajax({
        type: "POST",
        url: "ShoppingCart.aspx/ObtenerRUC_Perfil",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            Ruc_Perfil = Datos.RUC;
            lenguaje = Datos.lenguaje;
            filtro_preregistro = Datos.PreRegistro;
            montoCompraEval = parseFloat(Datos.montoTotalPagar);
            puntosCompraEval = parseFloat(Datos.puntosTotal);
            //montoTAX = parseFloat(Datos.tax).toFixed(2);
            MontoPagoG = parseFloat(0) + parseFloat(montoCompraEval);
            $('#lblTAX').text("$" + 0);
            $('#lblTotalPago').text("$" + MontoPagoG.toFixed(2));
            MostrarBotonCompra();
        }
    });
}

function ObtenerTipoCliente() {
    $.ajax({
        type: "POST",
        url: "Store.aspx/ObtenerTipoCliente",
        data: "{}",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            tipoCliente = dataS.d;
        }
    });
}

function MostrarBotonCompra() {
    var StipoC = $("#STipoCompra").val();
    var SpreRe = $("#cboPreRegistro").val();

    if (SpreRe == "0" && filtro_preregistro == 0) {
        if (StipoC == "01" || StipoC == "02" || StipoC == "03" || StipoC == "04" ||
            StipoC == "05" || StipoC == "06" || StipoC == "23") {
            $("#btnRegistrarAfiliado").show();
            $("#btnDetalleDelivery").hide();
            $("#divCheckTerminosAfi").show();
            $("#divCheckTerminosSocio").hide();
        }
        else if (StipoC != "01" || StipoC != "02" || StipoC != "03" || StipoC != "04" ||
            StipoC != "05" || StipoC != "06" || StipoC != "23") {
            $("#btnRegistrarAfiliado").hide();
            $("#btnDetalleDelivery").show();
            $("#divCheckTerminosAfi").hide();
            $("#divCheckTerminosSocio").hide();
        }
    }
    else {
        $("#btnRegistrarAfiliado").hide();
        $("#btnDetalleDelivery").show();
        $("#divCheckTerminosAfi").hide();
        $("#divCheckTerminosSocio").show();
    }
}

function GuardarIDCombos(combo, idcombo) {
    var departAf = $('#cboDepartamento').val();
    MostrarBotonCompra();
    var obj = JSON.stringify({
        comboS: combo, idComboS: idcombo, departAfi: departAf
    });
    $.ajax({
        type: "POST",
        url: "ShoppingCart.aspx/GuardarIDCombos",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            if (Datos.Mensaje == "Reset") {
                $("#tbl_datosPro tbody tr").remove();
                montoTAX = 0;
                montoCompraEval = 0;
                puntosCompraEval = 0;
                $('#LbPuntosRango').text(0);
                $('#LbPuntosCompra').text(0);
                $('#LbPrecioTotal').text("$" + 0);
                $('#LbPrecioPagar').text("$" + 0);
                $('#lblTAX').text(0);
                $('#lblTotalPago').text("$" + 0);

                $('#lbPuntosRango2').text(0);
                $('#lblPuntos2').text(0);
                $('#lblPTotal2').text("$" + 0);
                $('#lblPPagar2').text("$" + 0);

                $('#lblPtsRangoDel').text(0);
                $('#lblPtsRealDel').text(0);
                $('#lblPrecBrutoDel').text("$" + 0);
                $('#lblPrecPagarDel').text("$" + 0);
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'These changes reset the order',
                }).then(function () {
                    window.location = "Store.aspx";
                });
            } else {
                //montoTAX = parseFloat(Datos.Tax).toFixed(2);
                montoCompraEval = parseFloat(Datos.MontoPago).toFixed(2);
                $('#lbTCompra2').text($("#STipoCompra option:selected").text());
                $('#lbTienda2').text($("#ComboTienda option:selected").text());
                $('#lblTipoCompraDel').text($("#STipoCompra option:selected").text());
                $('#lblTiendaDel').text($("#ComboTienda option:selected").text());
                var MontoPagoG = parseFloat(0) + parseFloat(montoCompraEval);
                $('#lblTAX').text("$" + parseFloat(0).toFixed(2));
                $('#lblTotalPago').text("$" + parseFloat(MontoPagoG).toFixed(2));
                if (combo == "TCompra") {
                    $('#LbPrecioPagar').text("$" + parseFloat(Datos.MontoPago).toFixed(2));
                }
                if (combo == "Pregis" && (idcombo != "0" && idcombo != "" && idcombo != null)) {
                    localStorage['STipoCompra'] = Datos.IdPaquete;
                    $('#STipoCompra').val(Datos.IdPaquete);
                }
            }
        }
    });
}

function ActualizarProducto(idProductoPais) {
    ValidarDelivery = 0;
    var cant = $("#" + idProductoPais + "").val();
    var departAf = $('#cboDepartamento').val();
    var StipoC = $("#STipoCompra").val();

    var obj = JSON.stringify({
        idProdPais: idProductoPais, nuevaCantidad: cant, tipCompra: StipoC, departAfi: departAf
    });
    $.ajax({
        type: "POST",
        url: "ShoppingCart.aspx/ActualizarProducto",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            montoCompraEval = parseFloat(Datos.Monto).toFixed(2);
            puntosCompraEval = parseFloat(Datos.Puntos).toFixed(2);
            //montoTAX = parseFloat(Datos.Tax).toFixed(2);
            $('#LbPuntosRango').text(Datos.PuntosPromo);
            $('#LbPuntosCompra').text(Datos.Puntos);
            $('#LbPrecioTotal').text("$" + parseFloat(Datos.Total).toFixed(2));
            $('#LbPrecioPagar').text("$" + parseFloat(Datos.Monto).toFixed(2));
            $('#Sub' + idProductoPais + '').text("$" + parseFloat(Datos.SubTotalProd).toFixed(2));
            $('#SubP' + idProductoPais + '').text(Datos.SubTotalPunt);
            $('#lblTAX').text(parseFloat(0).toFixed(2));
            var MontoPagoG = parseFloat(0) + parseFloat(montoCompraEval);
            $('#lblTotalPago').text("$" + parseFloat(MontoPagoG).toFixed(2));

            $('#lbPuntosRango2').text(Datos.PuntosPromo);
            $('#lblPuntos2').text(Datos.Puntos);
            $('#lblPTotal2').text("$" + parseFloat(Datos.Total).toFixed(2));
            $('#lblPPagar2').text("$" + parseFloat(Datos.Monto).toFixed(2));

            $('#lblPtsRangoDel').text(Datos.PuntosPromo);
            $('#lblPtsRealDel').text(Datos.Puntos);
            $('#lblPrecBrutoDel').text("$" + parseFloat(Datos.Total).toFixed(2));
            $('#lblPrecPagarDel').text("$" + parseFloat(Datos.Monto).toFixed(2));

            var alertaAc = (lenguaje == "1") ? "Producto Actualizado" : "Product Updated";
            var titulo = (lenguaje == "1") ? "¡Perfecto!" : "Perfect!";

            Swal.fire({
                title: titulo,
                text: alertaAc,
                type: "success"
            }).then(function () {
                //window.location = "ShoppingCart.aspx";
            });;
        }
    });
}

function EliminarProducto(idProductoPais, t) {
    ValidarDelivery = 0;
    var index = $(t).parent().parent().index();
    var departAf = $('#cboDepartamento').val();
    var obj = JSON.stringify({
        idProdPais: idProductoPais, departAfi: departAf
    });
    $.ajax({
        type: "POST",
        url: "ShoppingCart.aspx/EliminarProducto",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            //montoTAX = parseFloat(Datos.Tax).toFixed(2);
            montoCompraEval = parseFloat(Datos.Monto).toFixed(2);
            puntosCompraEval = parseFloat(Datos.Puntos).toFixed(2);
            $('#LbPuntosRango').text(Datos.PuntosPromo);
            $('#LbPuntosCompra').text(Datos.Puntos);
            $('#LbPrecioTotal').text("$" + parseFloat(Datos.Total).toFixed(2));
            $('#LbPrecioPagar').text("$" + parseFloat(Datos.Monto).toFixed(2));
            $('#lblTAX').text(0);
            var MontoPagoG = parseFloat(0) + parseFloat(montoCompraEval);
            $('#lblTotalPago').text("$" + MontoPagoG);


            $('#lbPuntosRango2').text(Datos.PuntosPromo);
            $('#lblPuntos2').text(Datos.Puntos);
            $('#lblPTotal2').text("$" + parseFloat(Datos.Total).toFixed(2));
            $('#lblPPagar2').text("$" + parseFloat(Datos.Monto).toFixed(2));

            $('#lblPtsRangoDel').text(Datos.PuntosPromo);
            $('#lblPtsRealDel').text(Datos.Puntos);
            $('#lblPrecBrutoDel').text("$" + parseFloat(Datos.Total).toFixed(2));
            $('#lblPrecPagarDel').text("$" + parseFloat(Datos.Monto).toFixed(2));

            $("#tbl_datosPro> tbody > tr").eq(index).remove();
            var alertaAc = (lenguaje == "1") ? "Producto Eliminado" : "Removed Product";
            var titulo = (lenguaje == "1") ? "¡Perfecto!" : "Perfect!";

            Swal.fire({
                title: titulo,
                text: alertaAc,
                type: "success"
            }).then(function () {
                if (Datos.Mensaje == "0") {
                    window.location = "Store.aspx";
                }
            });
        }
    });
}

//$(".tablaSiguiente").click(function (e) {
//    var StipoC = $("#STipoCompra").val();
//    var Spuntos = puntosCompraEval;
//    var SprecioPago = montoCompraEval;
//    var alerta = "";
//    if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
//        StipoC == "11" | StipoC == "12" | StipoC == "13") {

//    } else {
//        if (StipoC == "01" | StipoC == "02" | StipoC == "03" | StipoC == "04" |
//            StipoC == "05" | StipoC == "06" | StipoC == "23") {
//            if (StipoC == "01") {
//                if (Spuntos >= 60 & SprecioPago >= 115.2) {
//                    CompletarDatosAfiliacion();
//                } else {
//                    alerta = (lenguaje == "1") ? "Necesitas 60 puntos y un monto de $ 115.20 para realizar la compra" : "You need 60 points and an amount of $ 115.20 to make the purchase";
//                    FaltaMontoUpgrade(alerta);
//                }
//            }
//            else if (StipoC == "02") {
//                if (Spuntos >= 105 & SprecioPago >= 189) {
//                    CompletarDatosAfiliacion();
//                } else {
//                    alerta = (lenguaje == "1") ? "Necesitas 105 puntos y un monto de $ 189 para realizar la compra" : "You need 105 points and an amount of $ 189 to make the purchase";
//                    FaltaMontoUpgrade(alerta);
//                }
//            }
//            else if (StipoC == "03") {
//                if (Spuntos >= 255 & SprecioPago >= 428.4) {
//                    CompletarDatosAfiliacion();
//                } else {
//                    alerta = (lenguaje == "1") ? "Necesitas 255 puntos y un monto de $ 428.4 para realizar la compra" : "You need 255 points and an amount of $ 428.4 to make the purchase";
//                    FaltaMontoUpgrade(alerta);
//                }
//            }
//            else if (StipoC == "04") {
//                if (Spuntos >= 510 & SprecioPago >= 734.4) {
//                    CompletarDatosAfiliacion();
//                } else {
//                    alerta = (lenguaje == "1") ? "Necesitas 510 puntos y un monto de $ 734.4 para realizar la compra" : "You need 510 points and an amount of $ 734.4 to make the purchase";
//                    FaltaMontoUpgrade(alerta);
//                }
//            }
//            else if (StipoC == "05") {
//                if (SprecioPago >= 100) {
//                    CompletarDatosAfiliacion();
//                } else {
//                    alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 realizar la compra" : "You need an amount of $ 100 to make the purchase";
//                    FaltaMontoUpgrade(alerta);
//                }
//            }
//            else if (StipoC == "06") {
//                if (SprecioPago >= 100) {
//                    CompletarDatosAfiliacion();
//                } else {
//                    alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 realizar la compra" : "You need an amount of $ 100 to make the purchase";
//                    FaltaMontoUpgrade(alerta);
//                }
//            }
//            else if (StipoC == "23") {
//                if (Spuntos >= 1020 & SprecioPago >= 1468.8) {
//                    CompletarDatosAfiliacion();
//                } else {
//                    alerta = (lenguaje == "1") ? "Necesitas 1020 puntos y un monto de $ 1468.8 para realizar la compra" : "You need 1020 points and an amount of $ 1468.8 to make the purchase";
//                    FaltaMontoUpgrade(alerta);
//                }
//            }
//            else {
//                CompletarDatosAfiliacion();
//            }
//        }

//    }
//});

//$(".tablaDelivery").click(function (e) {
//    var StipoC = $("#STipoCompra").val();
//    var Spuntos = puntosCompraEval;
//    var SprecioPago = montoCompraEval;

//    if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
//        StipoC == "11" | StipoC == "12" | StipoC == "13" | filtro_preregistro == 1) {
//        if (StipoC == "01") {
//            if (Spuntos >= 60 & SprecioPago >= 115.20) {
//                RellenarDatosDelivery();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 60 puntos y un monto de $ 115.20 para realizar la compra" : "You need 60 points and an amount of $ 115.20 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "02") {
//            if (Spuntos >= 105 & SprecioPago >= 189) {
//                RellenarDatosDelivery();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 105 puntos y un monto de $ 189 para realizar la compra" : "You need 105 points and an amount of $ 189 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "03") {
//            if (Spuntos >= 255 & SprecioPago >= 428.4) {
//                RellenarDatosDelivery();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 255 puntos y un monto de $ 428.4 para realizar la compra" : "You need 255 points and an amount of $ 428.4 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "04") {
//            if (Spuntos >= 510 & SprecioPago >= 734.4) {
//                RellenarDatosDelivery();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 510 puntos y un monto de $ 734.4 para realizar la compra" : "You need 510 points and an amount of $ 734.4 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "05") {
//            if (SprecioPago >= 100) {
//                RellenarDatosDelivery();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "06") {
//            if (SprecioPago >= 100) {
//                RellenarDatosDelivery();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "23") {
//            if (Spuntos >= 1020 & SprecioPago >= 1468.8) {
//                RellenarDatosDelivery();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 1020 puntos y un monto de $ 1468.8 para realizar la compra" : "You need 1020 points and an amount of $ 1468.8 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else {
//            RellenarDatosDelivery();
//        }

//    } else {
//        var afiliacion = document.getElementById("afiliacion");
//        var elemento = document.getElementById("circle02");
//        $("#tablaCompra").slideUp(0);
//        $("#resumenDeLaCompra").slideUp(0);
//        $("#02afiliacion").fadeIn(300);
//        elemento.className += " active";
//        afiliacion.className += " active";
//    }
//});

$(".tablaSiguiente").click(function (e) {
    var StipoC = $("#STipoCompra").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;
    var alerta = "";
    if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
        StipoC == "11" | StipoC == "12" | StipoC == "13") {

    } else {
        if (StipoC == "01" | StipoC == "02" | StipoC == "03" | StipoC == "04" |
            StipoC == "05" | StipoC == "06" | StipoC == "23") {
            if (StipoC == "01") {
                if (Spuntos >= 58) {
                    CompletarDatosAfiliacion();
                } else {
                    alerta = (lenguaje == "1") ? "Necesitas 58 puntos para realizar la compra" : "You need 58 points to make the purchase";
                    FaltaMontoUpgrade(alerta);
                }
            }
            else if (StipoC == "02") {
                if (Spuntos >= 116) {
                    CompletarDatosAfiliacion();
                } else {
                    alerta = (lenguaje == "1") ? "Necesitas 116 puntos para realizar la compra" : "You need 116 points to make the purchase";
                    FaltaMontoUpgrade(alerta);
                }
            }
            else if (StipoC == "03") {
                if (Spuntos >= 256) {
                    CompletarDatosAfiliacion();
                } else {
                    alerta = (lenguaje == "1") ? "Necesitas 256 puntos para realizar la compra" : "You need 256 points to make the purchase";
                    FaltaMontoUpgrade(alerta);
                }
            }
            else if (StipoC == "04") {
                if (Spuntos >= 504) {
                    CompletarDatosAfiliacion();
                } else {
                    alerta = (lenguaje == "1") ? "Necesitas 504 puntos para realizar la compra" : "You need 504 points to make the purchase";
                    FaltaMontoUpgrade(alerta);
                }
            }
            else if (StipoC == "05") {
                if (SprecioPago >= 100) {
                    CompletarDatosAfiliacion();
                } else {
                    alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 realizar la compra" : "You need an amount of $ 100 to make the purchase";
                    FaltaMontoUpgrade(alerta);
                }
            }
            else if (StipoC == "06") {
                if (SprecioPago >= 100) {
                    CompletarDatosAfiliacion();
                } else {
                    alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 realizar la compra" : "You need an amount of $ 100 to make the purchase";
                    FaltaMontoUpgrade(alerta);
                }
            }
            else if (StipoC == "23") {
                if (Spuntos >= 1002) {
                    CompletarDatosAfiliacion();
                } else {
                    alerta = (lenguaje == "1") ? "Necesitas 1002 puntos para realizar la compra" : "You need 1002 points to make the purchase";
                    FaltaMontoUpgrade(alerta);
                }
            }
            else {
                CompletarDatosAfiliacion();
            }
        }

    }
});

$(".tablaDelivery").click(function (e) {
    var StipoC = $("#STipoCompra").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;

    if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
        StipoC == "11" | StipoC == "12" | StipoC == "13" | filtro_preregistro == 1) {
        if (StipoC == "01") {
            if (Spuntos >= 58) {
                RellenarDatosDelivery();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 58 puntos para realizar la compra" : "You need 58 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 116) {
                RellenarDatosDelivery();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 116 puntos para realizar la compra" : "You need 116 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 256) {
                RellenarDatosDelivery();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 256 puntos para realizar la compra" : "You need 256 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 504) {
                RellenarDatosDelivery();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 504 puntos para realizar la compra" : "You need 504 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                RellenarDatosDelivery();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                RellenarDatosDelivery();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1002) {
                RellenarDatosDelivery();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 1002 puntos para realizar la compra" : "You need 1002 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else {
            RellenarDatosDelivery();
        }

    } else {
        var afiliacion = document.getElementById("afiliacion");
        var elemento = document.getElementById("circle02");
        $("#tablaCompra").slideUp(0);
        $("#resumenDeLaCompra").slideUp(0);
        $("#02afiliacion").fadeIn(300);
        elemento.className += " active";
        afiliacion.className += " active";
    }
});

$(".tablaAfiDel").click(function (e) {
    ObtenerRUC_Perfil();
    var ordenCompletada = document.getElementById("delivery");
    var elemento = document.getElementById("circle03");
    $("#03Delivery").fadeIn(300);
    $("#04ordenCompletada").slideUp(0);
    $("#02afiliacion").slideUp(0);
    $("#01carritoDeCompras").slideUp(0);
    $("#resumenDeLaCompra").slideUp(0);
    $("#tablaCompra").slideUp(0);
    elemento.className += " active"
    ordenCompletada.className += " active"

});

$(".02").click(function (e) {
    var StipoC = $("#STipoCompra").val();
    if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
        StipoC == "11" | StipoC == "12" | StipoC == "13" | filtro_preregistro == 1) {
        $("#01carritoDeCompras").fadeIn(300);
        $("#tablaCompra").fadeIn(300);
        $("#resumenDeLaCompra").fadeIn(300);
        $("#02afiliacion").slideUp(0);
        $("#03Delivery").slideUp(0);
        var elemento = document.getElementById("circle01");
        elemento.classList.remove('active');
    } else {
        $("#02afiliacion").fadeIn(300);
        $("#tablaCompra").slideUp(0);
        $("#resumenDeLaCompra").slideUp(0);
        $("#03Delivery").slideUp(0);
        var elemento = document.getElementById("circle02");
        elemento.classList.remove('active');
    }

});

$("#btnUCDelivery").click(function (e) {
    var SpreRe = $("#cboPreRegistro").val();
    var StipoC = $("#STipoCompra").val();
    var chkTerminos = document.getElementById("chkTerminos").checked;
    var chkTerminosSocio = document.getElementById("chkTerminosSocio").checked;
    var alerta = (lenguaje == "1") ? "Debe volver a seleccionar el codigo postal para la asignación correspondiente de Envío y Manejo e IMPUESTOS" :
        "You must reselect the zip code for the corresponding assignment of Shipping & Handling and TAX";
    var condiciones = (lenguaje == "1") ? "Debe aceptar los términos y condiciones" : "You must accept the terms and conditions";

    if (ValidarDelivery == 0) {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: alerta,
        });
    } else {
        if (SpreRe == "0" && filtro_preregistro == 0) {
            if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
                StipoC == "11" | StipoC == "12" | StipoC == "13") {
                ValidarPedidoConsumoUpgrade();
            }
            else {
                if (chkTerminos == false) {
                    FaltaSeleccionar(condiciones);
                } else {
                    ValidarPedidoAfiliacion();
                }
            }
        }
        else {
            if (chkTerminosSocio == false) {
                FaltaSeleccionar(condiciones);
            } else {
                ValidarPedidoPreRegistro();
            }
        }
    }
});

$("#btnComPendiente").click(function (e) {
    var SpreRe = $("#cboPreRegistro").val();
    var StipoC = $("#STipoCompra").val();

    if (SpreRe == "0" && filtro_preregistro == 0) {
        if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
            StipoC == "11" | StipoC == "12" | StipoC == "13") {
            $('#btnCancelM').click();
            $('.modal-backdrop').remove();
            $('#page_loader').show();
            CompraConsumo_Upgrade("", "", "", "", "", "", "");
        }
        else {
            $('#btnCancelM').click();
            $('.modal-backdrop').remove();
            $('#page_loader').show();
            CompraAfiliacion();
        }
    }
    else {
        $('#btnCancelM').click();
        $('.modal-backdrop').remove();
        $('#page_loader').show();
        CompraConsumo_Upgrade("", "", "", "", "", "", "");
    }
});

function MostrarModalMedioPago() {

    ValidarStock();
    //else { CambiarMontosPedidoPaypal(); }
}

function CambiarMontosPedidoPaypal() {

    var Handling = parseFloat(handling).toString();
    var MontoItems = parseFloat(montoCompraEval).toString();
    var Tax = parseFloat(montoTAX).toString();
    MontoStrPayPal = MontoPagoG.toFixed(2).toString();

    var _ddd = {
        amount: MontoStrPayPal,
        tax: Tax,
        shipping: Handling,
        amountItems: MontoItems,
        idPedido: idPedPP
    };

    var obj = JSON.stringify({
        receiveReplace: _ddd
    });
    $.ajax({
        type: "POST",
        url: "http://localhost:51536/Autocompletado.asmx/ReplaceDataPaypal",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
        }
    });
}

function CompletarDatosAfiliacion() {
    var afiliacion = document.getElementById("afiliacion");
    var elemento = document.getElementById("circle02");
    $("#tablaCompra").slideUp(0);
    $("#resumenDeLaCompra").slideUp(0);
    $("#02afiliacion").fadeIn(300);
    elemento.className += " active";
    afiliacion.className += " active";
}

function RellenarDatosDelivery() {
    var ordenCompletada = document.getElementById("delivery");
    var elemento = document.getElementById("circle03");
    $("#03Delivery").fadeIn(300);
    $("#04ordenCompletada").slideUp(0);
    $("#02afiliacion").slideUp(0);
    $("#01carritoDeCompras").slideUp(0);
    $("#resumenDeLaCompra").slideUp(0);
    $("#tablaCompra").slideUp(0);
    elemento.className += " active"
    ordenCompletada.className += " active"
}

//function ValidarPedidoPreRegistro() {
//    var StipoC = $("#STipoCompra").val();
//    var StipoP = $("#SMedioPago").val();
//    var Stienda = $("#ComboTienda").val();
//    var Stitular = $("#ddlTitular").val();
//    var SNombre = $("#txtNameDelivery").val();
//    var SApellido = $("#txtLastNameDelivery").val();
//    var SDocumento = $("#txtDocDelivery").val();
//    var SDireccion = $("#txtDirectionDelivery").val();
//    var SCorreo = $("#txtEmailDelivery").val();
//    var SCelular = $("#txtPhoneDelivery").val();
//    var Spuntos = puntosCompraEval;
//    var SprecioPago = montoCompraEval;
//    var alerta = "";

//    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
//        FaltanDatos();
//    }
//    else if (SprecioPago <= 0) {
//        FaltanProductos();
//    }
//    else if (Stitular == "0" || Stitular == "" || Stitular == null) {
//        FaltaTitular();
//    }
//    else if (SNombre == "" || SApellido == "" || SDocumento == "" ||
//        SDireccion == "" || SCorreo == "" || SCelular == "") {
//        FaltaDatosTitular();
//    }
//    else if (Stitular == "2" && SDireccion == "") {
//        FaltaDatosTitular();
//    }
//    else {
//        if (StipoC == "01") {
//            if (Spuntos >= 60 & SprecioPago >= 115.20) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 60 puntos y un monto de $ 115.20 para realizar la compra" : "You need 60 points and an amount of $ 115.20 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "02") {
//            if (Spuntos >= 105 & SprecioPago >= 189) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 105 puntos y un monto de $ 189 para realizar la compra" : "You need 105 points and an amount of $ 189 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "03") {
//            if (Spuntos >= 255 & SprecioPago >= 428.4) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 255 puntos y un monto de $ 428.4 para realizar la compra" : "You need 255 points and an amount of $ 428.4 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "04") {
//            if (Spuntos >= 510 & SprecioPago >= 734.4) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 510 puntos y un monto de $ 734.4 para realizar la compra" : "You need 510 points and an amount of $ 734.4 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "05") {
//            if (SprecioPago >= 100) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "06") {
//            if (SprecioPago >= 100) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "23") {
//            if (Spuntos >= 1020 & SprecioPago >= 1468.8) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 1020 puntos y un monto de $ 1468.8 para realizar la compra" : "You need 1020 points and an amount of $ 1468.8 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else {
//            //CompraConsumo_Upgrade();
//            MostrarModalMedioPago();
//        }
//    }
//}

//function ValidarPedidoConsumoUpgrade() {
//    var StipoC = $("#STipoCompra").val();
//    var StipoP = $("#SMedioPago").val();
//    var Stienda = $("#ComboTienda").val();
//    var Stitular = $("#ddlTitular").val();
//    var SNombre = $("#txtNameDelivery").val();
//    var SApellido = $("#txtLastNameDelivery").val();
//    var SDocumento = $("#txtDocDelivery").val();
//    var SDireccion = $("#txtDirectionDelivery").val();
//    var SCorreo = $("#txtEmailDelivery").val();
//    var SCelular = $("#txtPhoneDelivery").val();
//    var Spuntos = puntosCompraEval;
//    var SprecioPago = montoCompraEval;

//    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
//        FaltanDatos();
//    }
//    else if (SprecioPago <= 0) {
//        FaltanProductos();
//    }
//    else if (Stitular == "0" || Stitular == "" || Stitular == null) {
//        FaltaTitular();
//    }
//    else if (SNombre == "" || SApellido == "" || SDocumento == "" ||
//        SDireccion == "" || SCorreo == "" || SCelular == "") {
//        FaltaDatosTitular();
//    }
//    else if (Stitular == "2" && SDireccion == "") {
//        FaltaDatosTitular();
//    }
//    else {
//        if (StipoC == "08") {
//            if (Spuntos >= 50 & SprecioPago >= 112.50) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 50 puntos y un monto de $ 112.50 para realizar la compra" : "You need 50 points and an amount of $ 112.50 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "09") {
//            if (Spuntos >= 200 & SprecioPago >= 372.8) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 200 puntos y un monto de $ 372.8 para realizar la compra" : "You need 200 points and an amount of $ 372.8 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "10") {
//            if (Spuntos >= 450 & SprecioPago >= 660.6) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 450 puntos y un monto de $ 660.6 para realizar la compra" : "You need 450 points and an amount of $ 660.6 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "11") {
//            if (Spuntos >= 150 & SprecioPago >= 279.6) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 150 puntos y un monto de $ 279.6 para realizar la compra" : "You need 150 points and an amount of $ 279.6 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "12") {
//            if (Spuntos >= 400 & SprecioPago >= 587.2) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 400 puntos y un monto de $ 587.2 para realizar la compra" : "You need 400 points and an amount of $ 587.2 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "13") {
//            if (Spuntos >= 250 & SprecioPago >= 367) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 250 puntos y un monto de $ 367 para realizar la compra" : "You need 250 points and an amount of $ 367 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "24") {
//            if (Spuntos >= 950 & SprecioPago >= 1415.5) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 950 puntos y un monto de $ 1415.5 para realizar la compra" : "You need 950 points and an amount of $ 1415.5 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "25") {
//            if (Spuntos >= 900 & SprecioPago >= 1341) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 900 puntos y un monto de $ 1341 para realizar la compra" : "You need 900 points and an amount of $ 1341 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "26") {
//            if (Spuntos >= 750 & SprecioPago >= 1117.5) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 750 puntos y un monto de $ 1117.5 para realizar la compra" : "You need 750 points and an amount of $ 1117.5 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "27") {
//            if (Spuntos >= 500 & SprecioPago >= 745) {
//                //CompraConsumo_Upgrade();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 500 puntos y un monto de $ 745 para realizar la compra" : "You need 500 points and an amount of $ 745 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        } else {
//            //CompraConsumo_Upgrade();
//            MostrarModalMedioPago();
//        }
//    }
//}

//function ValidarPedidoAfiliacion() {
//    var StipoC = $("#STipoCompra").val();
//    var txCorreo = $("#TxtCorreo").val();
//    var StipoP = $("#SMedioPago").val();
//    var Stienda = $("#ComboTienda").val();
//    var Stitular = $("#ddlTitular").val();
//    var SNombre = $("#txtNameDelivery").val();
//    var SDocumento = $("#txtDocDelivery").val();
//    var SDireccion = $("#txtDirectionDelivery").val();
//    var SCorreo = $("#txtEmailDelivery").val();
//    var SCelular = $("#txtPhoneDelivery").val();
//    var cUpline = $("#CboUpLine").val();
//    var cDistrito = $("#cboDistrito").val();
//    var cZipCode = $("#cboZipCode").val();
//    var cDepartamento = $("#cboDepartamento").val();
//    var cPremio = $("#cboPremio").val();
//    var tCelular = $("#TxtCelular").val();
//    var tDireccion = $("#txtDireccion").val();
//    var tDocumento = $("#txtNumDocumento").val();
//    var tFechaNac = $("#datepicker").val();
//    var fechaNac = toDate(tFechaNac);
//    var Spuntos = puntosCompraEval;
//    var SprecioPago = montoCompraEval;
//    var idioma = $("#cbo_idioma").val();
//    var alerta = "";
//    fechaNac.setFullYear(fechaNac.getFullYear() + 21);

//    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
//        FaltanDatos();
//    }
//    else if (tFechaNac == "") {
//        alerta = (lenguaje == "1") ? "Debes ingresar una fecha de nacimiento correcta" : "You must enter a correct date of birth";
//        FaltaSeleccionar(alerta);
//    }
//    else if (idioma == "0") {
//        alerta = (lenguaje == "1") ? "Debes elegir el idioma" : "You must choose the language";
//        FaltaSeleccionar(alerta);
//    }
//    else if (fechaNac >= tFechaNac) {
//        alerta = (lenguaje == "1") ? "El cliente debe ser mayor de 21 años." : "The member must be over 21 years old";
//        FaltaSeleccionar(alerta);
//    }
//    else if (txCorreo == "") {
//        alerta = (lenguaje == "1") ? "Debes ingresar un correo electrónico válido" : "You must enter a valid Email";
//        FaltaSeleccionar(alerta);
//    }
//    else if (cDepartamento == "") {
//        alerta = (lenguaje == "1") ? "Debes seleccionar el estado" : "You must select the State";
//        FaltaSeleccionar(alerta);
//    }
//    else if (cZipCode == "") {
//        alerta = (lenguaje == "1") ? "You must select the Zip Code" : "You must select the Zip Code";
//        FaltaSeleccionar(alerta);
//    }
//    else if (cPremio == "") {
//        alerta = (lenguaje == "1") ? "Debe seleccionar el Y.W. Premio" : "You must select the Y.W. Premio";
//        FaltaSeleccionar(alerta);
//    }
//    else if (tCelular == "") {
//        alerta = (lenguaje == "1") ? "Debes ingresar un número de celular válido" : "You must enter a valid mobile number";
//        FaltaSeleccionar(alerta);
//    }
//    else if (tDireccion == "") {
//        alerta = (lenguaje == "1") ? "Debe introducir una dirección válida" : "You must enter a valid address";
//        FaltaSeleccionar(alerta);
//    }
//    else if (tDocumento == "" || tDocumento.length < 8) {
//        alerta = (lenguaje == "1") ? "Debes ingresar un documento válido" : "You must enter a valid document";
//        FaltaSeleccionar(alerta);
//    }
//    else if (Stitular == "0" || Stitular == "" || Stitular == null) {
//        FaltaTitular();
//    }
//    else if (SNombre == "" || SDocumento == "" ||
//        SDireccion == "" || SCorreo == "" || SCelular == "") {
//        FaltaDatosTitular();
//    }
//    else if (Stitular == "2" && SDireccion == "") {
//        FaltaDatosTitular();
//    }
//    else if (cUpline == "" && (StipoC != "05" && StipoC != "06")) {
//        FaltaUpline();
//    }
//    else {
//        if (StipoC == "01") {
//            if (Spuntos >= 60 & SprecioPago >= 115.20) {
//                //CompraAfiliacion();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 60 puntos y un monto de $ 115.20 para realizar la compra" : "You need 60 points and an amount of $ 115.20 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "02") {
//            if (Spuntos >= 105 & SprecioPago >= 189) {
//                //CompraAfiliacion();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 105 puntos y un monto de $ 189 para realizar la compra" : "You need 105 points and an amount of $ 189 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "03") {
//            if (Spuntos >= 255 & SprecioPago >= 428.4) {
//                //CompraAfiliacion();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 255 puntos y un monto de $ 428.4 para realizar la compra" : "You need 255 points and an amount of $ 428.4 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "04") {
//            if (Spuntos >= 510 & SprecioPago >= 734.4) {
//                //CompraAfiliacion();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 510 puntos y un monto de $ 734.4 para realizar la compra" : "You need 510 points and an amount of $ 734.4 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "05") {
//            if (SprecioPago >= 100) {
//                //CompraAfiliacion();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "06") {
//            if (SprecioPago >= 100) {
//                //CompraAfiliacion();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else if (StipoC == "23") {
//            if (Spuntos >= 1020 & SprecioPago >= 1468.8) {
//                //CompraAfiliacion();
//                MostrarModalMedioPago();
//            } else {
//                alerta = (lenguaje == "1") ? "Necesitas 1020 puntos y un monto de $ 1468.8 para realizar la compra" : "You need 1020 points and an amount of $ 1468.8 to make the purchase";
//                FaltaMontoUpgrade(alerta);
//            }
//        }
//        else {
//            //CompraAfiliacion();
//            MostrarModalMedioPago();
//        }
//    }

//}

function ValidarPedidoPreRegistro() {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    var Stienda = $("#ComboTienda").val();
    var Stitular = $("#ddlTitular").val();
    var SNombre = $("#txtNameDelivery").val();
    var SApellido = $("#txtLastNameDelivery").val();
    var SDocumento = $("#txtDocDelivery").val();
    var SDireccion = $("#txtDirectionDelivery").val();
    var SCorreo = $("#txtEmailDelivery").val();
    var SCelular = $("#txtPhoneDelivery").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;
    var alerta = "";

    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
        FaltanDatos();
    }
    else if (SprecioPago <= 0) {
        FaltanProductos();
    }
    else if (Stitular == "0" || Stitular == "" || Stitular == null) {
        FaltaTitular();
    }
    else if (SNombre == "" || SApellido == "" || SDocumento == "" ||
        SDireccion == "" || SCorreo == "" || SCelular == "") {
        FaltaDatosTitular();
    }
    else if (Stitular == "2" && SDireccion == "") {
        FaltaDatosTitular();
    }
    else {
        if (StipoC == "01") {
            if (Spuntos >= 58) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 58 puntos para realizar la compra" : "You need 58 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 116) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 116 puntos para realizar la compra" : "You need 116 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 256) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 256 puntos para realizar la compra" : "You need 256 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 504) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 504 puntos para realizar la compra" : "You need 504 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1002) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 1002 puntos para realizar la compra" : "You need 1002 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else {
            //CompraConsumo_Upgrade();
            MostrarModalMedioPago();
        }
    }
}

function ValidarPedidoConsumoUpgrade() {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    var Stienda = $("#ComboTienda").val();
    var Stitular = $("#ddlTitular").val();
    var SNombre = $("#txtNameDelivery").val();
    var SApellido = $("#txtLastNameDelivery").val();
    var SDocumento = $("#txtDocDelivery").val();
    var SDireccion = $("#txtDirectionDelivery").val();
    var SCorreo = $("#txtEmailDelivery").val();
    var SCelular = $("#txtPhoneDelivery").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;

    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
        FaltanDatos();
    }
    else if (SprecioPago <= 0) {
        FaltanProductos();
    }
    else if (Stitular == "0" || Stitular == "" || Stitular == null) {
        FaltaTitular();
    }
    else if (SNombre == "" || SApellido == "" || SDocumento == "" ||
        SDireccion == "" || SCorreo == "" || SCelular == "") {
        FaltaDatosTitular();
    }
    else if (Stitular == "2" && SDireccion == "") {
        FaltaDatosTitular();
    }
    else {
        if (StipoC == "08") {
            if (Spuntos >= 50) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 50 puntos para realizar la compra" : "You need 50 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "09") {
            if (Spuntos >= 200) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 200 puntos para realizar la compra" : "You need 200 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "10") {
            if (Spuntos >= 450) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 450 puntos para realizar la compra" : "You need 450 point to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "11") {
            if (Spuntos >= 150) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 150 puntos para realizar la compra" : "You need 150 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "12") {
            if (Spuntos >= 400) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 400 puntos para realizar la compra" : "You need 400 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "13") {
            if (Spuntos >= 250) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 250 puntos para realizar la compra" : "You need 250 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "24") {
            if (Spuntos >= 950) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 950 puntos para realizar la compra" : "You need 950 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "25") {
            if (Spuntos >= 900) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 900 puntos para realizar la compra" : "You need 900 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "26") {
            if (Spuntos >= 750) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 750 puntos para realizar la compra" : "You need 750 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "27") {
            if (Spuntos >= 500) {
                //CompraConsumo_Upgrade();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 500 puntos para realizar la compra" : "You need 500 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        } else {
            //CompraConsumo_Upgrade();
            MostrarModalMedioPago();
        }
    }
}

function ValidarPedidoAfiliacion() {
    var StipoC = $("#STipoCompra").val();
    var txCorreo = $("#TxtCorreo").val();
    var StipoP = $("#SMedioPago").val();
    var Stienda = $("#ComboTienda").val();
    var Stitular = $("#ddlTitular").val();
    var SNombre = $("#txtNameDelivery").val();
    var SDocumento = $("#txtDocDelivery").val();
    var SDireccion = $("#txtDirectionDelivery").val();
    var SCorreo = $("#txtEmailDelivery").val();
    var SCelular = $("#txtPhoneDelivery").val();
    var cUpline = $("#CboUpLine").val();
    var cDistrito = $("#cboDistrito").val();
    var cZipCode = $("#cboZipCode").val();
    var cDepartamento = $("#cboDepartamento").val();
    var cPremio = $("#cboPremio").val();
    var tCelular = $("#TxtCelular").val();
    var tDireccion = $("#txtDireccion").val();
    var tDocumento = $("#txtNumDocumento").val();
    var tFechaNac = $("#datepicker").val();
    var fechaNac = toDate(tFechaNac);
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;
    var idioma = $("#cbo_idioma").val();
    var alerta = "";
    fechaNac.setFullYear(fechaNac.getFullYear() + 21);

    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
        FaltanDatos();
    }
    else if (tFechaNac == "") {
        alerta = (lenguaje == "1") ? "Debes ingresar una fecha de nacimiento correcta" : "You must enter a correct date of birth";
        FaltaSeleccionar(alerta);
    }
    else if (idioma == "0") {
        alerta = (lenguaje == "1") ? "Debes elegir el idioma" : "You must choose the language";
        FaltaSeleccionar(alerta);
    }
    else if (fechaNac >= tFechaNac) {
        alerta = (lenguaje == "1") ? "El cliente debe ser mayor de 21 años." : "The member must be over 21 years old";
        FaltaSeleccionar(alerta);
    }
    else if (txCorreo == "") {
        alerta = (lenguaje == "1") ? "Debes ingresar un correo electrónico válido" : "You must enter a valid Email";
        FaltaSeleccionar(alerta);
    }
    else if (cDepartamento == "") {
        alerta = (lenguaje == "1") ? "Debes seleccionar el estado" : "You must select the State";
        FaltaSeleccionar(alerta);
    }
    else if (cZipCode == "") {
        alerta = (lenguaje == "1") ? "You must select the Zip Code" : "You must select the Zip Code";
        FaltaSeleccionar(alerta);
    }
    else if (cPremio == "") {
        alerta = (lenguaje == "1") ? "Debe seleccionar el Y.W. Premio" : "You must select the Y.W. Premio";
        FaltaSeleccionar(alerta);
    }
    else if (tCelular == "") {
        alerta = (lenguaje == "1") ? "Debes ingresar un número de celular válido" : "You must enter a valid mobile number";
        FaltaSeleccionar(alerta);
    }
    else if (tDireccion == "") {
        alerta = (lenguaje == "1") ? "Debe introducir una dirección válida" : "You must enter a valid address";
        FaltaSeleccionar(alerta);
    }
    else if (tDocumento == "" || tDocumento.length < 8) {
        alerta = (lenguaje == "1") ? "Debes ingresar un documento válido" : "You must enter a valid document";
        FaltaSeleccionar(alerta);
    }
    else if (Stitular == "0" || Stitular == "" || Stitular == null) {
        FaltaTitular();
    }
    else if (SNombre == "" || SDocumento == "" ||
        SDireccion == "" || SCorreo == "" || SCelular == "") {
        FaltaDatosTitular();
    }
    else if (Stitular == "2" && SDireccion == "") {
        FaltaDatosTitular();
    }
    else if (cUpline == "" && (StipoC != "05" && StipoC != "06")) {
        FaltaUpline();
    }
    else {
        if (StipoC == "01") {
            if (Spuntos >= 58) {
                //CompraAfiliacion();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 58 puntos para realizar la compra" : "You need 58 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 116) {
                //CompraAfiliacion();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 116 puntos para realizar la compra" : "You need 116 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 256) {
                //CompraAfiliacion();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 256 puntos para realizar la compra" : "You need 256 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 504) {
                //CompraAfiliacion();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 504 puntos para realizar la compra" : "You need 504 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                //CompraAfiliacion();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                //CompraAfiliacion();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas un monto de $ 100 para realizar la compra" : "You need an amount of $ 100 to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1002) {
                //CompraAfiliacion();
                MostrarModalMedioPago();
            } else {
                alerta = (lenguaje == "1") ? "Necesitas 1002 puntos para realizar la compra" : "You need 1002 points to make the purchase";
                FaltaMontoUpgrade(alerta);
            }
        }
        else {
            //CompraAfiliacion();
            MostrarModalMedioPago();
        }
    }

}

function CompraConsumo_Upgrade(create_time, update_time, id_dateail, status, email, name, payer_id) {
    var Stienda = $("#ComboTienda").val();
    var tiendaTx = $("#ComboTienda option:selected").text();
    var Stitular = $("#ddlTitular").val();
    var StipoC = $("#STipoCompra").val();
    var tipoCTx = $("#STipoCompra option:selected").text();
    var StipoP = $("#SMedioPago").val();
    var SNombre = $("#txtNameDelivery").val();
    var SApellido = $("#txtLastNameDelivery").val();
    var SDocumento = $("#txtDocDelivery").val();
    var SDireccion = $("#txtDirectionDelivery").val();
    var SCorreo = $("#txtEmailDelivery").val();
    var SCelular = $("#txtPhoneDelivery").val();
    var SEstado = $("#cbo_departamento_carrito option:selected").val();
    var cZipCodeDel = ZipCodeGeneral;
    // $("#txt_zipcode").val();
    // alert(cZipCodeDel);
    // return;
    var obj = JSON.stringify({
        cTienda: Stienda, txTienda: tiendaTx, cTitularRUC: Stitular, idTCompra: StipoC, txTCompra: tipoCTx, cTipoPago: StipoP,
        txNombres: SNombre, txDocumento: SDocumento, txDireccion: SDireccion, txCorreo: SCorreo, txCelular: SCelular, handling: handling,
        ZipCodeTx: cZipCodeDel, create_time: create_time, updt_time: update_time, id_detail: id_dateail, status_pay: status, email_pay: email,
        name_pay: name, payer_id: payer_id, txApellidos: SApellido, txEstado: SEstado
    });
    $.ajax({
        type: "POST",
        url: "ShoppingCart.aspx/RealizarCompraConsumoUpgrade",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            $('#page_loader').hide();
            if (Datos.Result.Mensaje == "OK") {
                IrCompraTerminada();
            } else {
                Swal.fire({
                    title: 'Ooops...!',
                    text: Datos.Result.Mensaje,
                    type: "error"
                });
            }

        }
    });
}

function CompraAfiliacion() {
    var Stienda = $("#ComboTienda").val();
    var tiendaTx = $("#ComboTienda option:selected").text();
    var Stitular = $("#ddlTitular").val();
    var StipoC = $("#STipoCompra").val();
    var tipoCTx = $("#STipoCompra option:selected").text();
    var StipoP = $("#SMedioPago").val();
    var SNombre = $("#txtNameDelivery").val();
    var SDocumento = $("#txtDocDelivery").val();
    var SDireccion = $("#txtDirectionDelivery").val();
    var SCorreo = $("#txtEmailDelivery").val();
    var SCelular = $("#txtPhoneDelivery").val();
    var cZipCode = $("#cboZipCode").val();
    var cZipCodeDel = ipCodeGeneral;// $("#txt_zipcode").val();

    var obj = JSON.stringify({
        cTienda: Stienda, txTienda: tiendaTx, sNombres: SNombre, sDocumento: SDocumento, sDireccion: SDireccion, sCorreo: SCorreo, sCelular: SCelular,
        cTitularRUC: Stitular, idTCompra: StipoC, txTCompra: tipoCTx, cTipoPago: StipoP, txCorreo: $("#TxtCorreo").val(),
        txDocumento: $("#txtNumDocumento").val(), txUsuario: $("#txtUl").val(), txFechaNac: $("#datepicker").val(),
        txCDRPreferido: $("#cboTipoEstablecimiento").val(), txCDRPremio: $("#cboPremio").val(), txClave: $("#TxtCl").val(),
        txNombre: $("#txtNombre").val(), txApellidoPat: $("#txtApPaterno").val(), txApellidoMat: $("#txtApMaterno").val(),
        txSexo: $("#ComboSexo option:selected").text(), txTipoDoc: $("#ComboTipoDocumento option:selected").text(),
        txTelefono: $("#TxtTelefono").val(), txCelular: $("#TxtCelular").val(), cPais: $("#cboPais").val(),
        cDepartamento: $("#cboDepartamento").val(), cProvincia: $("#cboProvincia").val(), cDistrito: "", ZipCode: cZipCode,
        txDireccion: $("#txtDireccion").val(), txReferencia: $("#TxtReferencia").val(), txDetraccion: $("#TxtNumCuenDetracciones").val(),
        txRUC: $("#TxtRUC").val(), txBanco: $("#TxtBanco").val(), txNumCuenta: $("#TxtNumCuenDeposito").val(), handling: handling,
        txInterbancaria: $("#TxtNumCuenInterbancaria").val(), cTCliente: $("#cboTipoCliente").val(), txUpline: $("#CboUpLine").val(),
        Idioma: $("#cbo_idioma").val(), ZipCodeTx: cZipCodeDel
    });
    $.ajax({
        type: "POST",
        url: "ShoppingCart.aspx/RealizarCompraAfiliacion",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;

            if (Datos.Mensaje == "OK") {
                IrCompraTerminada();
            } else {
                Swal.fire({
                    title: 'Ooops...!',
                    text: Datos.Mensaje,
                    type: "error"
                });
            }

        }
    });
}

function MostrarComboTiendaAndDatosDelivery(value) {
    var divComboTienda = document.getElementById('MostrarComboTiena');
    var divDatosDelivery = document.getElementById('MostrarDatosParaDelivery');

    if (value == "1") {
        divComboTienda.style.display = "block";
        divDatosDelivery.style.display = "none"
    }
    else {
        divComboTienda.style.display = "none";
        divDatosDelivery.style.display = "block"
    }
}

function validarLetras(e) {
    var keyCode = (e.keyCode ? e.keyCode : e.which);
    if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
        e.preventDefault();
    }
}

function validarNumeros(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$("#btnValDelivery").click(function (e) {
    var zipcode = $("#txt_zipcode").val();

    if (zipcode == null || zipcode == "") {
        ZipCodeErroneo();
    } else {
        ObtenerHandling(zipcode);
    }
});

function ObtenerHandling(zipCode) {
    // var zipCode = $("#cbo_zipcode_carrito option: selected").text();// $('#txt_zipcode').val();
    //alert("mirame: " + zipCode);
    //return;
    var obj = JSON.stringify({
        ZipCode: zipCode
    });
    $.ajax({
        type: "POST",
        url: "ShoppingCart.aspx/ObtenerHandlingShiping",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {

            var Datos = dataS.d;
            if (Datos.Mensaje == "OK") {
                montoTAX = parseFloat(Datos.Tax).toFixed(2);
                handling = parseFloat(Datos.Handling).toFixed(2);
                MontoPagoG = parseFloat(montoTAX) + parseFloat(handling) + parseFloat(montoCompraEval);
                listaPro = Datos.ListaShopCart;
                idCliente = Datos.IdCliente;
                $('#lblTAX').text("$" + montoTAX);
                $('#lbl_shipping').text("$" + handling);
                $('#lblTotalPago').text("$" + MontoPagoG.toFixed(2));
                EditarPayPal();
                ValidarDelivery = 1;
                var alerta = (lenguaje == "1") ? "Data validada" : "Validated data";
                var titulo = (lenguaje == "1") ? "¡Perfecto!" : "Perfect!";
                var wrong = (lenguaje == "1") ? "ZipCode incorrecto" : "ZipCode is wrong";

                Swal.fire({
                    title: titulo,
                    text: alerta,
                    type: "success"
                });
            } else {
                ValidarDelivery = 0;
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: wrong,
                });
            }

        }
    });
}

function ValidarStock() {

    var tiendaTx = $("#ComboTienda option:selected").text();
    var obj = JSON.stringify({
        txTienda: tiendaTx
    });
    $.ajax({
        type: "POST",
        url: "ShoppingCart.aspx/ConsultaStockPaypal",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            if (Datos == "OK") {
                $('#exampleModal').modal({ backdrop: 'static', keyboard: false })
                $('#exampleModal').modal('show');
                if (flag == 0) { CallPayPal(); }
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: Datos,
                });
            }

        }
    });
}

function FaltaTitular() {
    var wrong = (lenguaje == "1") ? "Debe seleccionar el titular de la compra" : "You must select the owner of the purchase";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: wrong,
    })
}
function FaltaDatosTitular() {
    var wrong = (lenguaje == "1") ? "Debes completar todos los datos del propietario de la compra, puedes verificar tu perfil." : "You must fill in all the details of the purchase owner, you can verify your profile.";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: wrong,
    })
}
function FaltaUpline() {
    var wrong = (lenguaje == "1") ? "Debe seleccionar el upline" : "You must select the Upline";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: wrong,
    })
}
function FaltanDatos() {
    var wrong = (lenguaje == "1") ? "Debes seleccionar todos los datos de compra" : "You must select all the purchase data";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: wrong,
    })
}
function FaltaMontoUpgrade(mensaje) {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: mensaje,
    })
}
function FaltaSeleccionar(mensaje) {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: mensaje,
    })
}
function FaltanProductos() {
    var wrong = (lenguaje == "1") ? "Debe agregar productos al carrito" : "You must add products to the cart";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: wrong,
    })
}
function NoCambioPreRegistro() {
    var wrong = (lenguaje == "1") ? "Estás completando un pre-registro, no puedes seleccionar esta opción" : "You are completing a pre-registration, you cannot select this option";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: wrong,
    })
}
function ZipCodeErroneo() {
    var wrong = (lenguaje == "1") ? "ZipCode incorrecto" : "Wrong zipcode";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: wrong,
    })
}
function PagoIncompleto() {
    $('#page_loader').hide();
    var wrong = (lenguaje == "1") ? "El pago no ha sido completado" : "The payment has not been completed";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: wrong,
    })
}

var params = new Object();
params.pais = "08";//$("#cboPais").val();//PAIS EEUU
params = JSON.stringify(params);

$.ajax({
    type: "POST",
    url: "EditarPerfil.aspx/GetDepartamentosByPais",
    data: params,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: false,
    success: function (result) {
        $("#cbo_departamento_carrito").empty();
        $("#cbo_departamento_carrito").append("<option value='0'>--Select--</option>");
        $.each(result.d, function (key, value) {
            $("#cbo_departamento_carrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus + ": " + XMLHttpRequest.responseText);
    }
});

$("#cbo_departamento_carrito").change(function () {
    // var cbPais = $("#cboPais").val();
    var depa = $("#cbo_departamento_carrito").val();
    var idbtnUCDelivery = document.getElementById('btnUCDelivery');
    if (depa == "0") {
        idbtnUCDelivery.style.display = "none";// "block";
    }

    var params = new Object();
    var url = "EditarPerfil.aspx/GetZipCodeByDepartamento";
    params.departamento = $("#cbo_departamento_carrito").val();
    params = JSON.stringify(params);
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {

            $("#cbo_zipcode_carrito").empty();
            $("#cbo_zipcode_carrito").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cbo_zipcode_carrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

$("#cbo_zipcode_carrito").change(function () {
    var zip_code = $(this).find('option:selected').text();// $("#cbo_zipcode_carrito").val();
    ZipCodeGeneral = zip_code;
    var idbtnUCDelivery = document.getElementById('btnUCDelivery');
    if (zip_code == "--Select--" || zip_code == "0") {
        idbtnUCDelivery.style.display = "none";
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'You must choose ZipCode',
        });
    }
    else {
        idbtnUCDelivery.style.display = "block";
        ObtenerHandling(zip_code);
    }
});

function EditarPayPal() {
    _ddd = {}; detail = [];
    var zipcode = $("#cbo_zipcode_carrito option:selected").text();
    var names = $("#txtNameDelivery").val();
    var surname = $("#txtLastNameDelivery").val();
    var estadoTx = $("#cbo_departamento_carrito option:selected").val();
    var SDireccion = $("#txtDirectionDelivery").val();
    var Handling = parseFloat(handling).toFixed(2).toString();
    var MontoItems = parseFloat(montoCompraEval).toFixed(2).toString();
    var Tax = parseFloat(montoTAX).toFixed(2).toString();
    MontoStrPayPal = MontoPagoG.toFixed(2).toString();
    detail = [];
    var taxItem = { currency_code: "USD", value: "0" };

    for (var i = 0; i < listaPro.length; i++) {
        var unMont = listaPro[i].SubTotalNeto / listaPro[i].Cantidad;
        var det = {
            description: "",
            name: listaPro[i].NombreProducto,
            quantity: listaPro[i].Cantidad.toString(),
            sku: "",
            tax: taxItem,
            unit_amount: { currency_code: "USD", value: unMont.toFixed(2).toString() }
        };
        detail.push(det);
    }

    _ddd = {
        amount: MontoStrPayPal,
        tax: Tax,
        shipping: Handling,
        amountItems: MontoItems,
        currencyCode: "USD",
        direccion: SDireccion,
        estado: estadoTx,
        postal_code: zipcode,
        nombres: names,
        apellidos: surname,
        orderDetail: detail
    };
}

function CallPayPal() {
    flag = 1;
    var SpreRe = $("#cboPreRegistro").val();
    var StipoC = $("#STipoCompra").val();

    paypal.Buttons({
        // Call your server to set up the transaction
        createOrder: function (data, actions) {
            //return fetch('/demo/checkout/api/paypal/order/create/', {
            //return fetch('https://global.mundosantanatura.com/Autocompletado.asmx/GenerarIdPaypal', {
            return fetch('Autocompletado.asmx/GenerarIdPaypal', {
                method: 'POST',
                headers: { "Content-type": "application/json" },
                body: JSON.stringify({ receive: _ddd })
            }).then(function (res) {
                return res.json();
            }).then(function (orderData) {
                idPedPP = orderData.d;
                return orderData.d;
            });
        },

        // Call your server to finalize the transaction
        onApprove: function (data, actions) {
            //return fetch('https://global.mundosantanatura.com/Autocompletado.asmx/getSessionToken', {
            return fetch('Autocompletado.asmx/getSessionToken', {
                method: 'POST',
                headers: { "Content-type": "application/json;" },
                body: JSON.stringify({ idPay: data.orderID, idClient: idCliente })
            }).then(function (res) {
                return res.json();
            }).then(function (orderData) {
                var Datos = orderData.d;
                $('#btnCancelM').click();
                $('.modal-backdrop').remove();
                $('#page_loader').show();
                if (Datos.status == "COMPLETED") {
                    //var emailPayer = (details.payer.email_address == null) ? "" : details.payer.email_address;
                    //var namePayer = (details.payer.name == null) ? "" : details.payer.name.given_name + ' ' + details.payer.name.surname;
                    var payerID = (data.payerID == null) ? "" : data.payerID;
                    var createTime = (Datos.purchase_units[0].payments.captures[0].create_time == null) ? "" : Datos.purchase_units[0].payments.captures[0].create_time;
                    var updtTime = (Datos.purchase_units[0].payments.captures[0].update_time == null) ? "" : Datos.purchase_units[0].payments.captures[0].update_time;
                    if (SpreRe == "0" && filtro_preregistro == 0) {
                        if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
                            StipoC == "11" | StipoC == "12" | StipoC == "13") {
                            CompraConsumo_Upgrade(createTime, updtTime, Datos.purchase_units[0].payments.captures[0].id, Datos.status,
                                "", "", payerID);
                        }
                        else {
                            CompraAfiliacion();
                        }
                    }
                    else {
                        CompraConsumo_Upgrade(createTime, updtTime, Datos.purchase_units[0].payments.captures[0].id, Datos.status,
                            "", "", payerID);
                    }
                } else {
                    return actions.restart();
                    //PagoIncompleto();
                }
            });
        }

    }).render('#paypal-button-container');
}

//function CallPayPal() {
//    var SpreRe = $("#cboPreRegistro").val();
//    var StipoC = $("#STipoCompra").val();

//    paypal.Buttons({
//        createOrder: function (data, actions) {
//            // This function sets up the details of the transaction, including the amount and line item details.
//            return actions.order.create({
//                purchase_units: [{
//                    amount: {
//                        value: MontoStrPayPal,
//                        currency_code: 'USD'
//                    }
//                }]
//            });
//        },
//        onApprove: function (data, actions) {
//            // This function captures the funds from the transaction.
//            return actions.order.capture().then(function (details) {
//                // This function shows a transaction success message to your buyer.
//                //alert('Transaction completed by ' + details.payer.name.given_name);
//                $('#btnCancelM').click();
//                $('.modal-backdrop').remove();
//                $('#page_loader').show();
//                if (details.status == "COMPLETED") {
//                    var emailPayer = (details.payer.email_address == null) ? "" : details.payer.email_address;
//                    var namePayer = (details.payer.name == null) ? "" : details.payer.name.given_name + ' ' + details.payer.name.surname;
//                    var payerID = (details.payer.payer_id == null) ? "" : details.payer.payer_id;
//                    if (SpreRe == "0" && filtro_preregistro == 0) {
//                        if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
//                            StipoC == "11" | StipoC == "12" | StipoC == "13") {
//                            CompraConsumo_Upgrade(details.create_time, details.update_time, details.id, details.status,
//                                emailPayer, namePayer, payerID);
//                        }
//                        else {
//                            CompraAfiliacion();
//                        }
//                    }
//                    else {
//                        CompraConsumo_Upgrade(details.create_time, details.update_time, details.id, details.status,
//                            emailPayer, namePayer, payerID);
//                    }
//                } else {
//                    PagoIncompleto();
//                }
//            });
//        }
//    }).render('#paypal-button-container');
//}


