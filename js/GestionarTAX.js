﻿var tabla;
sendDataAjax();

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "GestionarTAX.aspx/ListaDatosTAX",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            console.log(data.d);
            addRowDT(data.d);
        }
    });
}

function addRowDT(obj) {
    tabla = $("#tbl_cdr").DataTable();
    tabla.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        tabla.fnAddData([
            obj[i].Documento,
            obj[i].Apodo,
            obj[i].CDRPS,
            '<input type="text" id="cantA' + obj[i].Documento + '" name="" class="form-control" style="background-color: lightgreen; width:90px" value="' + obj[i].TAX + '">',
            '<button id="btnUpdate" value="UpdateP" title="UpdateP" type="button" class="btn btn-success btn-updt"><i class="fas fa-sync-alt"></i></button>'
        ]);
    }
}

$(document).on('click', '.btn-updt', function (e) {
    e.preventDefault();
    var row = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row);
    var tax = $("#cantA" + datax[0] + "").val();
    ActualizarInversionInicial(datax[0], tax);
});

function ActualizarInversionInicial(dniCDR, tax) {

    var obj = JSON.stringify({ DNICDR: dniCDR, TAX: tax });
    $.ajax({
        type: "POST",
        url: "GestionarTAX.aspx/ActualizarDatosTAX",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data4) {
            alertme();
        }
    });
}

function alertme() {
    Swal.fire({
        title: 'Perfecto!',
        text: 'TAX Actualizado',
        type: "success"
    }).then(function () {
    });
}