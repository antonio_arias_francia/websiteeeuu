﻿var inputPais = document.getElementById('cboPais');
inputPais.onchange = function () {

    var idioma_actual = $('#hdn_Idioma_online').val();
    //alert("x: " + x);
    
    if (this.value == "01") {
        $("#divDistrito").show();
        $("#divProvincia").show();
        $("#ZipCode").hide();
        $("#divApeMat").show();
        if (idioma_actual == "1") {
            $("#lblDepart").text("Departmento");
            $("#lblProv").text("Provincia");
        } else {
            $("#lblDepart").text("Department");
            $("#lblProv").text("Province");
        }
        
    } else {
        $("#divDistrito").hide();
        $("#divProvincia").hide();
        $("#ZipCode").show();
        $("#divApeMat").hide();

        if (idioma_actual == "1") {
            $("#lblDepart").text("Estado");
            $("#lblProv").text("Código postal (*)");
        } else {
            $("#lblDepart").text("State");
            $("#lblProv").text("County");
        }
    }

    CargaTipoDocumento(this.value);
}

$.ajax({
    type: "POST",
    url: "EditarPerfil.aspx/GetPais",
    data: "{}",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: false,
    success: function (result) {
        $("#cboPaisStore").empty();
        $("#cboPaisStore").append("<option value='0'>------</option>");
        $.each(result.d, function (key, value) {
            $("#cboPaisStore").append($("<option></option>").val(value.Codigo).html(value.Nombre));
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus + ": " + XMLHttpRequest.responseText);
    }
});

$.ajax({
    type: "POST",
    url: "EditarPerfil.aspx/GetPais",
    data: "{}",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: false,
    success: function (result) {
        $("#cboPais").empty();
        $("#cboPais").append("<option value='0'>------</option>");
        $.each(result.d, function (key, value) {
            $("#cboPais").append($("<option></option>").val(value.Codigo).html(value.Nombre));
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus + ": " + XMLHttpRequest.responseText);
    }
});

$("#cboPais").change(function () {
    var params = new Object();
    params.pais = $("#cboPais").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDepartamentosByPais",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDepartamento").empty();
            $("#cboDepartamento").append("<option value='0'>------</option>");
            CargarProvincia();
            CargarDistrito();
            $.each(result.d, function (key, value) {
                $("#cboDepartamento").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

$("#cboDepartamento").change(function () {
    var cbPais = $("#cboPais").val();
    var params = new Object();
    var url = (cbPais == "01" ? "EditarPerfil.aspx/GetProvinciaByDepartamento" : "EditarPerfil.aspx/GetZipCodeByDepartamento");
    params.departamento = $("#cboDepartamento").val();
    params = JSON.stringify(params);


    $.ajax({
        type: "POST",
        url: url,
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            if (cbPais == "01") {
                $("#cboProvincia").empty();
                $("#cboProvincia").append("<option value='0'>------</option>");
                CargarDistrito();
                $.each(result.d, function (key, value) {
                    $("#cboProvincia").append($("<option></option>").val(value.Codigo).html(value.Nombre));
                });
            } else {
                $("#cboZipCode").empty();
                $("#cboZipCode").append("<option value='0'>------</option>");
                $.each(result.d, function (key, value) {
                    $("#cboZipCode").append($("<option></option>").val(value.Codigo).html(value.Nombre));
                });
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

$("#cboProvincia").change(function () {
    var params = new Object();
    params.provincia = $("#cboProvincia").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDistritoByProvincia",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDistrito").empty();
            $("#cboDistrito").append("<option value='0'>------</option>");
            $.each(result.d, function (key, value) {
                $("#cboDistrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});


function paqueteCliente(ddl) {

    dropdown = ddl.options[ddl.selectedIndex].value;

    if (ddl.options[ddl.selectedIndex].value == "05" || ddl.options[ddl.selectedIndex].value == "06") {
        $('#CboUpLine').attr("disabled", true);
        $("#CboUpLine").val("0");
    } else {
        $('#CboUpLine').attr("disabled", false);
    }
}

function CargaTipoDocumento(pais) {
    $('#ComboTipoDocumento').attr("disabled", false);
    if (pais == "01") {
        $("#ComboTipoDocumento").empty();
        $("#ComboTipoDocumento").append($("<option></option>").val("01").html("DOCUMENTO DE IDENTIDAD"));
        $("#ComboTipoDocumento").append($("<option></option>").val("02").html("PASAPORTE"));
    }
    else if (pais == "02" || pais == "09") {
        $("#ComboTipoDocumento").empty();
        $("#ComboTipoDocumento").append($("<option></option>").val("03").html("CÉDULA"));
        $("#ComboTipoDocumento").append($("<option></option>").val("04").html("PASAPORTE"));
    }
    else if (pais == "07") {
        $("#ComboTipoDocumento").empty();
        $("#ComboTipoDocumento").append($("<option></option>").val("05").html("CÉDULA DE IDENTIDAD"));
        $("#ComboTipoDocumento").append($("<option></option>").val("04").html("PASAPORTE"));
    }
    else if (pais == "08") {
        $("#ComboTipoDocumento").empty();
        $("#ComboTipoDocumento").append($("<option></option>").val("04").html("PASAPORTE"));
        $("#ComboTipoDocumento").append($("<option></option>").val("06").html("ITIN"));
        $("#ComboTipoDocumento").append($("<option></option>").val("07").html("DRIVERS LICENSE"));
    }
}

function CargarDepartamentos() {
    var params = new Object();
    params.pais = $("#cboPais").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDepartamentosByPais",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDepartamento").empty();
            $("#cboDepartamento").append("<option value='0'>------</option>");
            $.each(result.d, function (key, value) {
                $("#cboDepartamento").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarProvincia() {
    var params = new Object();
    params.departamento = $("#cboDepartamento").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetProvinciaByDepartamento",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboProvincia").empty();
            $("#cboProvincia").append("<option value='0'>------</option>");
            $.each(result.d, function (key, value) {
                $("#cboProvincia").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarDistrito() {
    var params = new Object();
    params.provincia = $("#cboProvincia").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDistritoByProvincia",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDistrito").empty();
            $("#cboDistrito").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboDistrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}


$("#btnRegistrar").click(function (e) {
    var day = new Date();
    var fecha = $("#datepicker").val();
    var dni = $("#txtNumDocumento").val();
    var usuario = $("#txtUl").val();
    var clave = $("#TxtCl").val();
    var upline = $("#CboUpLine").val();
    var paquete = $("#ddlPaquete").val("01");
    var nombre = $("#txtNombre").val();
    var apellidoPat = $("#txtApPaterno").val();
    var tdoc = $("#ComboTipoDocumento").val();
    var sexo = $("#ComboSexo").val();
    var correo = $("#TxtCorreo").val();
    var pais = $("#cboPais").val();
    var departamento = $("#cboDepartamento").val();
    var provincia = $("#cboProvincia").val();
    var distrito = $("#cboDistrito").val();
    var cbZipCode = $("#cboZipCode").val();
    var direccion = $("#txtDireccion").val();
    var celular = $("#TxtCelular").val();
    var fechaNac = toDate(fecha);
    var cdrPremio = $("#cboPremio").val();
    var paisStore = $("#cboPaisStore").val();
    var idioma = $("#cbo_idioma").val();


    e.preventDefault();

    fechaNac.setFullYear(fechaNac.getFullYear() + 21);
    if (usuario == "") {
        FaltaUsuario();
    } else if (clave == "") {
        FaltaClave();
    } else if (paquete == "0") {
        FaltaPackete();
    } else if (fecha == "") {
        FaltaFechaNac();
    } else if (fechaNac >= day) {
        FaltaFechaNac();
    } else if (paquete == "0") {
        FaltaPaquete();
    } else if ((paquete != "05" || paquete == "06") && upline == "0") {
        FaltaUpline();
    } else if (nombre == "") {
        FaltaNombre();
    } else if (celular == "") {
        FaltaCelular();
    } else if (apellidoPat == "") {
        FaltaApellido();
    } else if (tdoc == "0") {
        FaltaTipoDoc();
    } else if (dni == "") {
        FaltaDocumento();
    } else if (sexo == "0") {
        FaltaSexo();
    } else if (correo == "") {
        FaltaCorreo();
    } else if (pais == "0") {
        FaltaPais();
    } else if (departamento == "0") {
        FaltaDepartamento();
    } else if (provincia == "0" && pais == "01") {
        FaltaProvincia();
    } else if (distrito == "0" && pais == "01") {
        FaltaDistrito();
    } else if (cbZipCode == "0" && pais == "08") {
        FaltaZipCode();
    } else if (direccion == "") {
        FaltaDireccion();
    } else if (idioma == "0") {
        FaltaIdioma();
    } else {
        ValidarCorreo();
    }

});
// CONDICIONES PARA REGISTRAR
function ValidarCorreo() {
    var correo = $("#TxtCorreo").val();
    var doc = $("#txtNumDocumento").val();
    var usu = $("#txtUl").val();
    var valida = document.getElementById('fileUpload').files.length;
    var obj = JSON.stringify({ correoS: correo, documento: doc, usuario: usu });
    $.ajax({
        type: "POST",
        url: "PreRegistroSocio.aspx/ValidarDatos",
        data: obj,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (response) {
            console.log(response);
            if (response.d == "0") {
                if (valida == 0) {
                    RegistroCliente();
                } else {
                    UploadFile();
                }
            } else {
                Swal.fire({
                    title: 'Ooops...!',
                    text: response.d,
                    type: "error"
                });
            }
        }
    });
}

function UploadFile() {
    var fileUpload = $("#fileUpload").get(0);
    var files = fileUpload.files;

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    data.append("archivo", "socios");

    $.ajax({
        url: "FileUpload.ashx",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            var r = result;
            console.log(result.d);
            if (r == "") {
                RegistroCliente();
            } else {
                alert(result);
            }
        },
        error: function (err) {
            alert(err.statusText)
        }
    });
}

function RegistroCliente() {
    var day = moment().format("DD/MM/YYYY"), imagenCliente = "";
    var valida = document.getElementById('fileUpload').files.length;
    if (valida != 0) {
        imagenCliente = document.getElementById('fileUpload').files[0].name;
    }

    var obj = JSON.stringify({
        numeroDocUd: $("#txtNumDocumento").val(), usuarioUd: $("#txtUl").val(), claveUd: $("#TxtCl").val(), paisStore: $("#cboPais").val(),
        nombresUd: $("#txtNombre").val(), apellidoPatUd: $("#txtApPaterno").val(), apellidoMatUd: $("#txtApMaterno").val(),
        fechaNacUd: $("#datepicker").val(), sexoUd: $("#ComboSexo option:selected").text(), tipoDocUd: $("#ComboTipoDocumento option:selected").text(),
        correoUd: $("#TxtCorreo").val(), telefonoUd: $("#TxtTelefono").val(), celularUd: $("#TxtCelular").val(), paisUd: $("#cboPais").val(),
        departamentoUd: $("#cboDepartamento").val(), provinciaUd: $("#cboProvincia").val(), distritoUd: $("#cboDistrito").val(),
        direccionUd: $("#txtDireccion").val(), referenciaUd: $("#TxtReferencia").val(), detraccionUd: $("#TxtNumCuenDetracciones").val(),
        rucUd: $("#TxtRUC").val(), bancoUd: $("#TxtBanco").val(), depositoUd: $("#TxtNumCuenDeposito").val(), interbancariaUd: $("#TxtNumCuenInterbancaria").val(),
        patrocinadorUd: $("#txtPatrocinador").val(), uplineUd: $("#CboUpLine").val(), establecimientoUd: $("#cboTipoEstablecimiento").val(),
        imagenUd: imagenCliente, fecharegistro: day, paqueteUd: "01", cdrPremioUd: $("#cboPremio").val(), CodigoPostalUd: $("#cboZipCode").val(), Idioma: $("#cbo_idioma").val()
    });

    $.ajax({
        type: "POST",
        url: "PreRegistroSocio.aspx/PreRegistro",
        data: obj,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function () {
            alertme();
        }
    });
}

function toDate(dateStr) {
    var parts = dateStr.split("/")
    return new Date(parts[2], parts[1] - 1, parts[0])
}

//ALERTAS
function alertme() {
    Swal.fire({
        title: 'Perfecto!',
        text: 'The user has been registered',
        type: "success"
    }).then(function () {
        window.location = "PreRegistroSocio.aspx";
    });
}
function FaltaUsuario() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not register the User field',
        type: "error"
    });
}
function FaltaClave() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'You did not register the Password field',
        type: "error"
    });
}
function FaltaUpline() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'You must select the Upline',
        type: "error"
    });
}
function FaltaPremio() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'You must select the Yachay Wasi of Gifts',
        type: "error"
    });
}
function FaltaPackete() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Do not select the Package field',
        type: "error"
    });
}
function FaltaPaquete() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'You did not select the customer package',
        type: "error"
    });
}
function FaltaNombre() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not register the Names field',
        type: "error"
    });
}
function FaltaCelular() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not register the Mobile',
        type: "error"
    });
}
function FaltaApellido() {
    Swal.fire({
        title: 'No registró un Apellido!',
        text: 'You must put the first surname',
        type: "error"
    });
}
function FaltaTipoDoc() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not define the Document Type',
        type: "error"
    });
}
function FaltaDocumento() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not register the Document Number',
        type: "error"
    });
}
function FaltaFechaNac() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Incorrect Date of Birth',
        type: "error"
    });
}
function FaltaSexo() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not define sex',
        type: "error"
    });
}
function FaltaCorreo() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'The mail format is invalid',
        type: "error"
    });
}
function FaltaPais() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not define the Country',
        type: "error"
    });
}
function FaltaPaisStore() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'It did not define the Country Store',
        type: "error"
    });
}
function FaltaDepartamento() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not define the Department',
        type: "error"
    });
}
function FaltaProvincia() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not select the Province',
        type: "error"
    });
}
function FaltaDistrito() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not select District',
        type: "error"
    });
}
function FaltaDireccion() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not register the Address',
        type: "error"
    });
}
function FaltaCDR() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Does not select the Y.W.',
        type: "error"
    });
}
function FaltaZipCode() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'You must select the Zip Code',
        type: "error"
    });
}
function FaltaIdioma() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'You must choose the language',
        type: "error"
    });
}