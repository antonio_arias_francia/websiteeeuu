﻿var flag = 0, listaPro;
function AbrilModalPay(ticket) {
    DevolverDatosTicketPaypal(ticket);
}

function DevolverDatosTicketPaypal(Ticket) {

    $.ajax({
        type: 'POST',
        url: 'MisComprasV2.aspx/DevolverDatosPaypal',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        cache: false,
        data: '{Ticket: "' + Ticket + '"}',
        success: function (data) {
            var Datos = data.d;
            listaPro = Datos.ListaCartShop;
            $('#exampleModal').modal({ backdrop: 'static', keyboard: false })
            $('#exampleModal').modal('show');
            if (flag == 0) {
                CallPayPal(Datos.MontoNeto, Datos.Tax, Datos.Handling, Ticket, Datos.IdCliente, Datos.CodEstado,
                    Datos.Direccion, Datos.CodPostal, Datos.Nombres, Datos.Apellidos);
            }
        },
        error: function (xhr, status, error) {
            var error_especifico = eval("(" + xhr.responseText + ")");
            var error_mostrar = '';
            if (xhr.status === 0) {
                error_mostrar = ('Verificar conexión a internet - ' + error_especifico.Message);
            } else if (xhr.status == 404) {
                error_mostrar = ('URL solicitada no encontrada [Error 404] - ' + error_especifico.Message);
            } else if (xhr.status == 500) {
                error_mostrar = ('Error interno del servidor [Error 500] - ' + error_especifico.Message);
            } else if (status === 'parsererror') {
                error_mostrar = ('JSON solicitado falló - ' + error_especifico.Message);
            } else if (status === 'timeout') {
                error_mostrar = ('Error en tiempo de espera - ' + error_especifico.Message);
            } else if (status === 'abort') {
                error_mostrar = ('Solicitud Ajax abortada - ' + error_especifico.Message);
            } else {
                error_mostrar = ('Error desconocido: - ' + error_especifico.Message);
            }

        }
    });
}

function CallPayPal(MontoPagoItems, Tax, Handling, Ticket, idCliente, estadoTx, SDireccion, zipcode, names, surname) {
    flag = 1;
    var HandlingTax = parseFloat(Tax) + parseFloat(Handling);
    MontoPagoG = parseFloat(MontoPagoItems) + parseFloat(HandlingTax);
    let detail = [];
    var taxItem = { currency_code: "USD", value: "0" };

    for (var i = 0; i < listaPro.length; i++) {
        let det = {
            description: "",
            name: listaPro[i].Nombre,
            quantity: listaPro[i].Cantidad.toString(),
            sku: "",
            tax: taxItem,
            unit_amount: { currency_code: "USD", value: listaPro[i].PrecioPS.toString() }
        };
        detail.push(det);
    }

    var _ddd = {
        amount: MontoPagoG.toFixed(2).toString(),
        tax: Tax.toString(),
        shipping: Handling.toString(),
        amountItems: MontoPagoItems.toString(),
        currencyCode: "USD",
        direccion: SDireccion,
        estado: estadoTx,
        postal_code: zipcode,
        nombres: names,
        apellidos: surname,
        orderDetail: detail
    };

    paypal.Buttons({
        // Call your server to set up the transaction
        createOrder: function (data, actions) {
            //return fetch('/demo/checkout/api/paypal/order/create/', {
            return fetch('Autocompletado.asmx/GenerarIdPaypal', {
                method: 'POST',
                headers: { "Content-type": "application/json" },
                body: JSON.stringify({ receive: _ddd })
            }).then(function (res) {
                return res.json();
            }).then(function (orderData) {
                return orderData.d;
            });
        },

        // Call your server to finalize the transaction
        onApprove: function (data, actions) {
            return fetch('Autocompletado.asmx/getSessionToken', {
                method: 'POST',
                headers: { "Content-type": "application/json;" },
                body: JSON.stringify({ idPay: data.orderID, idClient: idCliente })
            }).then(function (res) {
                return res.json();
            }).then(function (orderData) {
                var Datos = orderData.d;
                $('#btnCancelM').click();
                $('.modal-backdrop').remove();
                $('#page_loader').show();
                if (Datos.status == "COMPLETED") {
                    //var emailPayer = (details.payer.email_address == null) ? "" : details.payer.email_address;
                    //var namePayer = (details.payer.name == null) ? "" : details.payer.name.given_name + ' ' + details.payer.name.surname;
                    var payerID = (data.payerID == null) ? "" : data.payerID;
                    var createTime = (Datos.purchase_units[0].payments.captures[0].create_time == null) ? "" : Datos.purchase_units[0].payments.captures[0].create_time;
                    var updtTime = (Datos.purchase_units[0].payments.captures[0].update_time == null) ? "" : Datos.purchase_units[0].payments.captures[0].update_time;
                    debugger;
                    FinalizarCompra(createTime, updtTime, Datos.purchase_units[0].payments.captures[0].id, Datos.status,
                        "", "", payerID, Ticket);
                } else {
                    return actions.restart();
                    //PagoIncompleto();
                }
            });
        }

    }).render('#paypal-button-container');
}

//function CallPayPal(MontoStrPayPal, Ticket) {
//    paypal.Buttons({
//        createOrder: function (data, actions) {
//            return actions.order.create({
//                purchase_units: [{
//                    amount: {
//                        value: MontoStrPayPal,
//                        currency_code: 'USD'
//                    }
//                }]
//            });
//        },
//        onApprove: function (data, actions) {
//            return actions.order.capture().then(function (details) {
//                $('#btnCancelM').click();
//                $('.modal-backdrop').remove();
//                $('#page_loader').show();
//                if (details.status == "COMPLETED") {
//                    var emailPayer = (details.payer.email_address == null) ? "" : details.payer.email_address;
//                    var namePayer = (details.payer.name == null) ? "" : details.payer.name.given_name + ' ' + details.payer.name.surname;
//                    var payerID = (details.payer.payer_id == null) ? "" : details.payer.payer_id;
//                    FinalizarCompra(details.create_time, details.update_time, details.id, details.status,
//                        emailPayer, namePayer, payerID, Ticket);
//                } else {
//                    PagoIncompleto();
//                }
//            });
//        }
//    }).render('#paypal-button-container');
//}

function FinalizarCompra(create_time, update_time, id_dateail, status, email, name, payer_id, Ticket) {
    var obj = JSON.stringify({
        ticket: Ticket, create_time: create_time, updt_time: update_time, id_detail: id_dateail, status_pay: status, email_pay: email,
        name_pay: name, payer_id: payer_id
    });
    $.ajax({
        type: "POST",
        url: "MisComprasV2.aspx/FinalizarCompraPaypal",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            $('#page_loader').hide();
            window.location = "MisComprasV2.aspx";
        }
    });
}

function PagoIncompleto() {
    $('#page_loader').hide();
    var wrong = "The payment has not been completed";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: wrong,
    })
}