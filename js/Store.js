﻿ObtenerTipoCliente();
var input = document.getElementById('SMedioPago');
var inputTCompra = document.getElementById('STipoCompra');
var inputTienda = document.getElementById('ComboTienda');
var inputPregis = document.getElementById('cboPreRegistro');
var inputPregis2 = document.getElementById('cboPreRegistro2');
var tipoCliente = "", lenguaje = "";
localStorage['SMedioPago'] = "06";
$("#SMedioPago").val("06");

input.onchange = function () {
    localStorage['SMedioPago'] = this.value;
    GuardarIDCombos('MPago', this.value);
}
inputTCompra.onchange = function () {
    var preregis = $('#cboPreRegistro').val();
    if (preregis != "0" && preregis != "" && preregis != null) {
        if (this.value == "01" || this.value == "02" || this.value == "03" ||
            this.value == "04" || this.value == "05" || this.value == "06" ||
            this.value == "23") {
            if ((tipoCliente == "01" && (this.value == "05" || this.value == "06")) ||
                (tipoCliente != "01" && (this.value != "05" || this.value != "06"))) {
                $('#STipoCompra').val(localStorage['STipoCompra']);
                NoCambioTipoCliente();
            } else {
                localStorage['STipoCompra'] = this.value;
                GuardarIDCombos('TCompra', this.value);
            }
        } else {
            $('#STipoCompra').val(localStorage['STipoCompra']);
            NoCambioPreRegistro();
        }
    } else {
        localStorage['STipoCompra'] = this.value;
        GuardarIDCombos('TCompra', this.value);
    }
}
inputTienda.onchange = function () {
    localStorage['ComboTienda'] = this.value;
    GuardarIDCombos('TiendaS', this.value);
}
inputPregis.onchange = function () {
    localStorage['cboPreRegistro'] = this.value;
    localStorage['cboPreRegistro2'] = this.value;
    GuardarIDCombos('Pregis', this.value);
}
inputPregis2.onchange = function () {
    localStorage['cboPreRegistro'] = this.value;
    localStorage['cboPreRegistro2'] = this.value;
    GuardarIDCombos('Pregis', this.value);
}

function ObtenerTipoCliente() {
    $.ajax({
        type: "POST",
        url: "Store.aspx/ObtenerTipoCliente",
        data: "{}",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            tipoCliente = Datos.tipoCliente;
            lenguaje = Datos.lenguaje;
        }
    });
}

function GuardarIDCombos(combo, idcombo) {
    var obj = JSON.stringify({
        comboS: combo, idComboS: idcombo
    });
    $.ajax({
        type: "POST",
        url: "Store.aspx/GuardarIDCombos",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            if (Datos.Mensaje == "Reset") {
                $('#LbPrecioTotal').text("$" + 0);
                $('#LbPrecioPagar').text("$" + 0);
                $('#LbPuntosCompra').text(0);
                $('#LbPuntosRango').text(0);
                $('#LbPrecioTotal2').text("$" + 0);
                $('#LbPrecioPagar2').text("$" + 0);
                $('#LbPuntosCompra2').text(0);
                $('#LbPuntosRango2').text(0);
            } else {
                if (combo == "TCompra") {
                    $('#LbPrecioPagar').text("$" + Datos.MontoPago);
                }
                if (combo == "Pregis" && (idcombo != "0" && idcombo != "" && idcombo != null)) {
                    localStorage['STipoCompra'] = Datos.IdPaquete;
                    $('#STipoCompra').val(Datos.IdPaquete);
                }
            }
            
        }
    });
}

function AgregarCarrito(codigo) {
    $('#page_loader').show();
    var cMPago = $("#SMedioPago").val();
    var cCom = $('#STipoCompra').val();
    var cTienda = $('#ComboTienda').val();

    if (cCom != '0' && cTienda != '0' && cMPago != '0') {
        var cantidad = $('#' + codigo + '').val();
        var obj = JSON.stringify({
            codProductoCarrito: codigo, cantidadProductos: cantidad
        });
        $.ajax({
            type: "POST",
            url: "Store.aspx/AgregarProducto",
            data: obj,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOptions, throwError) {
                console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (dataS) {
                var Datos = dataS.d;
                if (Datos.Mensaje == "OK") {
                    $('#LbPrecioTotal').text("$" + parseFloat(Datos.Total).toFixed(2));
                    $('#LbPrecioPagar').text("$" + parseFloat(Datos.Monto));
                    $('#LbPuntosCompra').text(Datos.Puntos);
                    $('#LbPuntosRango').text(Datos.PuntosPromo);
                    $('#LbPrecioTotal2').text("$" + parseFloat(Datos.Total).toFixed(2));
                    $('#LbPrecioPagar2').text("$" + parseFloat(Datos.Monto));
                    $('#LbPuntosCompra2').text(Datos.Puntos);
                    $('#LbPuntosRango2').text(Datos.PuntosPromo);
                    $('#page_loader').hide();
                    var alerta = (lenguaje == "1") ? "Producto agregado" : "Product added";
                    var titulo = (lenguaje == "1") ? "¡Perfecto!" : "Perfect!";
                    Swal.fire({
                        title: titulo,
                        text: alerta,
                        type: "success"
                    });
                }
                else {
                    $('#page_loader').hide();
                    Swal.fire({
                        title: 'Ooops...!',
                        text: Datos.Mensaje,
                        type: "error"
                    });
                }
            }
        });
    } else {
        var alerta = (lenguaje == "1") ? "Debes agregar productos al carrito" : "You must add products to the cart";
        Swal.fire({
            title: 'Ooops...!',
            text: alerta,
            type: "error"
        });
    }

}

function irCarrito() {
    var cMPago = $("#SMedioPago").val();
    var cCom = $('#STipoCompra').val();
    var cTienda = $('#ComboTienda').val();
    if (cCom != '0' && cTienda != '0' && cMPago != '0') {
        $.ajax({
            type: "POST",
            url: "Store.aspx/CantidadProdCarrito",
            data: "{}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOptions, throwError) {
                console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (dataS) {
                if (dataS.d == 0) {
                    location.href = "ShoppingCart.aspx";
                }
                else {
                    Swal.fire({
                        title: 'Ooops...!',
                        text: dataS.d,
                        type: "error"
                    });
                }
            }
        });
    } else {
        var alerta = (lenguaje == "1") ? "Debes seleccionar todos los datos de la compra" : "You must select all the purchase details";
        Swal.fire({
            title: 'Ooops...!',
            text: alerta,
            type: "error"
        });
    }

}

function VerTodosProductos() {
    location.href = "Store.aspx";
}

$(function () {
    $("[id$=txtNomProducto]").autocomplete({
        source: function (request, responce) {
            $.ajax({
                url: "Autocompletado.asmx/FiltrarNombreProductos",
                method: "post",
                CORS: true,
                contentType: "application/json;charset=utf-8",
                data: "{ 'palabra': '" + request.term + "'}",
                dataType: 'json',
                success: function (data) {
                    responce(data.d);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("In The ERROR");
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        }
    });
});

function NoCambioPreRegistro() {
    var alerta = (lenguaje == "1") ? "Estás completando un pre-registro, no puedes seleccionar esta opción" : "You are completing a pre-registration, you cannot select that option";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: alerta,
    })
}

function NoCambioTipoCliente() {
    var alerta = (lenguaje == "1") ? "No puede realizar cambios en el tipo de cliente" : "You cannot make changes to the customer type";
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: alerta,
    })
}