﻿$(document).ready(function () {
    //$(".se-pre-con").fadeOut("slow");
    $('#page_loader').show();
    var employees = [];
    var dni = "", lenguaje = "";
    capturarDNI();
    sendDataAjax();


    function capturarDNI() {

        $.ajax({
            type: "POST",
            url: "MapaDePatrocinio.aspx/CapturarDNI",
            data: {},
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOptions, throwError) {
                console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                var Datos = data.d;
                dni = Datos.DNI;
                lenguaje = Datos.Idioma;
            }
        });
    }


    function sendDataAjax() {
        $.ajax({
            type: "POST",
            url: "MapaDePatrocinio.aspx/ListarEstructuraPatrocinio",
            data: {},
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOptions, throwError) {
                console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (data) {
                //console.log(data.d);
                var prueba = [];
                prueba = data.d;
                prueba.forEach(function (element) {
                    var objp = {
                        "EmployeeID": element.EmployeeID,
                        "LastName": element.LastName,
                        "ReportsTo": element.ReportsTo,
                        "Telefono": element.Telefono,
                        "IDRANGO": element.IDRANGO,
                        "TIPOC": element.TIPOC,
                        "PAQUETE": element.PAQUETE,
                        "Pais": element.Pais,
                        "Fecha": element.Fecha,
                        "PP": element.PP
                    }
                    employees.push(objp);

                });
                console.log(employees);
                var source =
                {
                    dataType: "json",
                    dataFields: [
                        { name: 'EmployeeID', type: 'string' },
                        { name: 'ReportsTo', type: 'string' },
                        { name: 'LastName', type: 'string' },
                        { name: 'Telefono', type: 'string' },
                        { name: 'IDRANGO', type: 'string' },
                        { name: 'TIPOC', type: 'string' },
                        { name: 'PAQUETE', type: 'string' },
                        { name: 'Pais', type: 'string' },
                        { name: 'Fecha', type: 'string' },
                        { name: 'PP', type: 'string' }
                    ],
                    hierarchy:
                    {
                        keyDataField: { name: 'EmployeeID' },
                        parentDataField: { name: 'ReportsTo' }
                    },
                    id: 'EmployeeID',
                    localData: employees
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                // create Tree Grid
                var names = "", tipocli = "", paquete = "", celular = "", pais = "", fecharegistro = "";
                names = (lenguaje == "1") ? "Nombres" : "Names";
                tipocli = (lenguaje == "1") ? "Tipo de Cliente" : "Client type";
                paquete = (lenguaje == "1") ? "Paquete" : "Package";
                celular = (lenguaje == "1") ? "Celular" : "Mobile";
                pais = (lenguaje == "1") ? "País" : "Country";
                fecharegistro = (lenguaje == "1") ? "Fecha de registro" : "Registration date";
                $("#treegrid").jqxTreeGrid(
                    {
                        theme: 'darkblue',
                        source: dataAdapter,
                        width: '100%',
                        //sortable: true,
                        ready: function () {
                            $("#treegrid").jqxTreeGrid('expandRow', dni.trim());
                        },
                        columns: [
                            //{ text: 'Nivel', dataField: 'IDRANGO', width: '380', align: 'center', cellsalign: 'left' },
                            { text: names, dataField: 'LastName', width: '380', align: 'center' },
                            { text: tipocli, dataField: 'TIPOC', width: '90', align: 'center' },
                            { text: 'PP', dataField: 'PP', width: '90', align: 'center' },
                            { text: paquete, dataField: 'PAQUETE', width: '140', align: 'center' },
                            { text: celular, dataField: 'Telefono', width: '150', align: 'center', cellsalign: 'center' },
                            { text: pais, dataField: 'Pais', width: '150', align: 'center', cellsalign: 'center' },
                            { text: fecharegistro, dataField: 'Fecha', width: '200', align: 'center', cellsalign: 'center' }

                        ]
                        //,
                        //columnGroups: [
                        //    { text: 'Nombre', name: 'Nombre' }
                        //]
                    });
                $('#page_loader').hide();
            }
        });
    }

});


