﻿//VARIABLES
var tabla, tabla2, data, data2, estados, tipoPag, filtroaplicado;
filtroaplicado = 0;
tabla = $("#tbl_compras").DataTable();

// LISTAR COMPRAS
function addRowDT(obj) {
    
    tabla.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        var mostrar = (obj[i].Estado == 0 ? "none" : "block");
        if (obj[i].Estado == 0) {
            estados = "Aprobado";
        } else if (obj[i].Estado == 1) {
            estados = "Pendiente";
        } else if (obj[i].Estado == 2) {
            estados = "Pendiente";
        } else {
            estados = "Anulado";
        }

        if (obj[i].TipoPago == '06') {
            tipoPag = "Transferencia";
        } else if (obj[i].TipoPago == '07') {
            tipoPag = "Paypal";
        } 

        tabla.fnAddData([
            obj[i].Ticket,
            obj[i].FechaPagoReporte,
            obj[i].IdopPeruShop,
            (obj[i].NombreCliente + " " + obj[i].ApellidoPat + " " + obj[i].ApellidoMat),
            obj[i].Cantidad,
            obj[i].MontoAPagar,
            obj[i].PuntosTotal,
            obj[i].Despacho,
            tipoPag,
            estados,
            obj[i].PaqueteSocio,
            '<button id="Detalle" value="Detalle" title="Detalle" class="btn btn-primary btn-deta" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-search"></i></button>',
            '<button id="ValidarC" value="Validar" style="display:' + mostrar+'" title="Validar" class="btn btn-success btn-val"><i class="fas fa-check"></i></button>',
            '<button value="Eliminar" title="Eliminar" class="btn btn-danger btn-delet"><i class="far fa-minus-square"></i></button>',
            obj[i].Ticket,
            obj[i].IdCliente,
            obj[i].idTipoCompra,
            obj[i].DNICliente,
            obj[i].Correo,
            obj[i].PaqueteSocio_PERU,
            obj[i].TipoCompra
        ]);
    }
}

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/ListarCompraGeneral",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            console.log(data.d);
            addRowDT(data.d);
        }
    });
}

function sendDataAjaxFiltro(fecha1, fecha2) {
    filtroaplicado = 1;
    var obj = JSON.stringify({ fecha1: fecha2, fecha2: fecha1 });

    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/ListarCompraGeneralFiltrado",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            console.log(data.d);
            addRowDT(data.d);
        }
    });
}

//validar
$(document).on('click', '.btn-val', function (e) {
    e.preventDefault();
    var row, dataRow, id, nombre, estable, fechaSim, idcliente, idtipocompra, dniCliente, correo,
        paqueteCl, paquete_peru, txtTipoCompra;
    row = $(this).parent().parent()[0];
    dataRow = tabla.fnGetData(row);
    id = dataRow[0];
    nombre = dataRow[3];
    estable = dataRow[7];
    fechaSim = dataRow[1];
    idcliente = dataRow[15];
    idtipocompra = dataRow[16];
    dniCliente = dataRow[17];
    correo = dataRow[18];
    paqueteCl = dataRow[10];
    paquete_peru = dataRow[19];
    txtTipoCompra = dataRow[20];

    valDataAjax(id, nombre, estable, fechaSim, idcliente, idtipocompra, dniCliente, correo, paqueteCl, paquete_peru, txtTipoCompra);
    sendDataAjax();
});

function valDataAjax(dataid, nombre, estable, fechaSim, idcliente, idtipocompra, dniCliente, correo, paqueteCl, paquete_peru, txtTipoCompra) {
    var obj = JSON.stringify({
        id: dataid, nombreSend: nombre, estabSend: estable, fechaSimpl: fechaSim,
        idCliente: idcliente, idTipoCom: idtipocompra, documento: dniCliente, correo: correo,
        paqueteCl: paqueteCl, paquete_peru: paquete_peru, txtTipoCompra: txtTipoCompra
    });

    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/EnviarPeruShop",
        data: obj,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (response) {
            console.log(response);
            Swal.fire({
                title: 'Perfecto!',
                text: 'Compra Validada',
                type: "success"
            });
        }
    });
}

//detalle

$(document).on('click', '.btn-deta', function (e) {
    e.preventDefault();
    var row2 = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row2);
    console.log(datax);
    id2 = datax[0];

    sendDetalleAjax(id2);
});

$(document).on('click', '.btn-delet', function (e) {
    e.preventDefault();
    var row2 = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row2);
    console.log(datax);
    var id1 = datax[0], id2 = datax[2], id3 = datax[3], id4 = datax[5], id5 = datax[8];

    eliminarCompra(id1, id2, id3, id4, id5);
});

function sendDetalleAjax(dataid2) {

    var obj2 = JSON.stringify({ id: dataid2 });

    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/ListaDetalleComprasGeneral",
        data: obj2,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data2) {
            console.log(data2.d);
            addRowDetalle(data2.d);
            datosDetalleCompra(data2.d);
        }
    });
}

function addRowDetalle(obj2) {
    tabla2 = $("#tbl_detalle").DataTable();
    tabla2.fnClearTable();
    for (var i = 0; i < obj2.length; i++) {
        var imagenProducto = obj2[i].Foto;
        tabla2.fnAddData([
            obj2[i].Nombre,
            '<img src="products/' + imagenProducto + '" style="height: 50px">',
            obj2[i].Cantidad,
            obj2[i].PrecioPS
        ]);
    }
}

function datosDetalleCompra(obj3) {
    var fechaSub = obj3[0].FechaCOM;

    $("#txtTicket").val(obj3[0].TicketCOM);
    $("#txtMontoPagar").val(obj3[0].MontoPagarCOM);
    $("#txtMontoComision").val(obj3[0].MontoComisionCOM);
    $("#txtPuntosTotal").val(obj3[0].PuntosTotalCOM);
    $("#txtMontoTotal").val(obj3[0].MontoTotalCOM);
    $("#txtfechaCompra").val(fechaSub.substr(0, 10));
    $("#txtCanProductos").val(obj3[0].CantidadCOM);
    $("#txtIDOP").val(obj3[0].IdopCOM);
    $("#cboTipoCompra").change(function () {
    });
    CargarTipoCompra();
    $("#cboTipoCompra").val(obj3[0].TipoCompraCOM);
    $("#cboTipoPago").change(function () {
    });
    CargarTipoPago();
    $("#cboTipoPago").val(obj3[0].TipoPagoCOM);
    $("#cboEstado").change(function () {
    });
    CargarEstadoCompra();
    $("#cboEstado").val(obj3[0].EstadoCOM);
    $("#cboDespacho").change(function () {
    });
    CargarDespacho();
    $("#cboDespacho").val(obj3[0].descDespacho);
}

function eliminarCompra(ticketE, idop, nombres, monto, tipopago) {

    var ticketSend = ticketE;
    var obj = JSON.stringify({ ticketS: ticketSend, idopS: idop, nombreS: nombres, montoS: monto, tipoPagoS: tipopago });
    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/EliminarCompra",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            console.log(data.d);
            Swal.fire({
                title: 'Perfecto!',
                text: 'Compra Eliminada',
                type: "success"
            }).then(function () {
                window.location = "AprobarTransferenciaEEUU.aspx";
            });
        }
    });
}

function CargarTipoCompra() {
    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/ListaTipoCompraDetalle",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboTipoCompra").empty();
            $("#cboTipoCompra").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboTipoCompra").append($("<option></option>").val(value.idTipoCompra).html(value.nombreTipoCom));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarTipoPago() {
    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/ListaTipoPagoDetalle",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboTipoPago").empty();
            $("#cboTipoPago").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboTipoPago").append($("<option></option>").val(value.idTipoPago).html(value.nombreTipoPago));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarEstadoCompra() {
    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/ListaEstadoDetalle",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboEstado").empty();
            $.each(result.d, function (key, value) {
                $("#cboEstado").append($("<option></option>").val(value.idEstado).html(value.descEstado));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarDespacho() {
    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/ListaDespachoDetalle",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDespacho").empty();
            $.each(result.d, function (key, value) {
                $("#cboDespacho").append($("<option></option>").val(value.idDespacho).html(value.descDespacho));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function updateDataAjax() {

    var ticket, montoPagar, montoComision, puntosTotal, montoTotal, fechaCompra, cantidadProd, idop, idtipocompra, tipoCompra, tipoPagoDet, estado, despacho;

    ticket = $("#txtTicket").val();
    montoPagar = $("#txtMontoPagar").val();
    montoComision = $("#txtMontoComision").val();
    puntosTotal = $("#txtPuntosTotal").val();
    montoTotal = $("#txtMontoTotal").val();
    fechaCompra = $("#txtfechaCompra").val();
    cantidadProd = $("#txtCanProductos").val();
    idop = $("#txtIDOP").val();
    idtipocompra = $("#cboTipoCompra").val();
    tipoCompra = $("#cboTipoCompra option:selected").text();
    tipoPagoDet = $("#cboTipoPago").val();
    estado = $("#cboEstado").val();
    despacho = $("#cboDespacho").val();

    var obj9 = JSON.stringify({
        ticketCom1: ticket, montoPagarCom1: montoPagar, montoComisionCom1: montoComision, puntosTotalCom1: puntosTotal,
        montoTotalCom1: montoTotal, fechaCom1: fechaCompra, cantidadProdCom1: cantidadProd, idopCom1: idop,
        idtipoCom1: idtipocompra, tipoCom1: tipoCompra, tipoPagoCom1: tipoPagoDet, estadoCom1: estado, despachoCom1: despacho
    });


    $.ajax({
        type: "POST",
        url: "AprobarTransferenciaEEUU.aspx/ActualizarCompraDetalle",
        data: obj9,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (response) {
            console.log(response);
            Swal.fire({
                title: 'Perfecto!',
                text: 'Compra Actualizada',
                type: "success"
            }).then(function () {
                window.location = "AprobarTransferenciaEEUU.aspx";
            });
        }
    });
}

$("#btnActualizar").click(function (e) {
    e.preventDefault();
    updateDataAjax();

});

var day = moment().format("DD/MM/YYYY");
$('#fecha').val(day);
$('.daterange').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: { format: "DD/MM/YYYY" },
    autoApply: false
});

$("#btnGenerar").click(function (e) {
    e.preventDefault();
    var fecha1 = $("#fecha").val();
    var fecha2 = $("#fechaFin").val();
    sendDataAjaxFiltro(fecha1, fecha2);
});

if (filtroaplicado == 0) {
    sendDataAjax();
} else { filtroaplicado = 0; }
