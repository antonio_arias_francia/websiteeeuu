﻿var tabla, idcliente;

tabla = $('#table_id').dataTable({
    responsive: true,
    language: {
        "decimal": "",
        "emptyTable": "There is no information",
        "info": "Show _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Show 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Show _MENU_ ",
        "loadingRecords": "Charging...",
        "processing": "Processing...",
        "search": "Search:",
        "zeroRecords": "No results found",
        "paginate": {
            "first": "First",
            "last": "Last",
            "next": "Next",
            "previous": "Previous"
        }
    }
});

sendDataAjax();

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "CompletePreRegistration.aspx/ListarPreRegistro",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            addRowDT(data.d);
            $('#ddlUpline').val();
        }
    });
}

function DatosCliente(idcliente) {
    var obj = JSON.stringify({ idcliente: idcliente });
    $.ajax({
        type: "POST",
        url: "CompletePreRegistration.aspx/ListarActualizacionDatosPreregistro",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            var Dd = data.d;
            $("#ddlTC").val(Dd[0].tipoCliente);

            if (Dd[0].tipoCliente == "01") {
                $('#ddlUpline').attr("disabled", false);
                $("#ddlUpline").val(Dd[0].upline).trigger('change');
            } else {
                $("#ddlUpline").val("0").trigger('change');;
                $('#ddlUpline').attr("disabled", true);
            }

        }
    });
}

function addRowDT(obj) {
    tabla.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        tabla.fnAddData([
            obj[i].FechaRegistro,
            obj[i].Documento,
            obj[i].Nombres,
            obj[i].Apellidos,
            obj[i].Nombres_Upline,
            obj[i].Paquete,
            '<button title="Actualizar" type="button" class="btn btn-primary btn-comp" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-users"></i></button>',
            obj[i].IdPaquete,
            obj[i].IdCliente
        ]);
    }
}

ddlTC.onchange = function () {

    if (this.value == "01") {
        $('#ddlUpline').attr("disabled", false);
    } else if (this.value == "03") {
        $("#ddlUpline").val("0").trigger('change');;
        $('#ddlUpline').attr("disabled", true);

    } else if (this.value == "05") {
        $("#ddlUpline").val("0").trigger('change');;
        $('#ddlUpline').attr("disabled", true);
    }
}

$(document).on('click', '.btn-comp', function (e) {
    e.preventDefault();
    var row2 = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row2);
    idcliente = datax[8];
    $('#txtNombre').val(datax[2] + " " + datax[3]);
    DatosCliente(datax[8]);
    //console.log(datax);
    //id2 = datax[8];
    //localStorage['STipoCompra'] = datax[7];
    //GuardarDatosPreregis(id2);
});

$("#btnActualizar").click(function (e) {
    Actualizar(idcliente);
});

function Actualizar(idcliente) {
    var upline = $('#ddlUpline').val();
    var tipocliente = $('#ddlTC').val();
    var obj = JSON.stringify({ idCliente: idcliente, upline: upline, tipoCliente: tipocliente });
    $.ajax({
        type: "POST",
        url: "CompletePreRegistration.aspx/ActualizarDatosPreregistro",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            alertme();
        }
    });
}

function alertme() {
    Swal.fire({
        title: '¡EXCELENTE!',
        text: 'Se actualizaron los datos correctamente',
        type: "success"
    }).then(function () {
        $('#btnCancelar').click();
        $('.modal-backdrop').remove();
        sendDataAjax();
    });
}

//function GuardarDatosPreregis(idcliente) {
//    var obj = JSON.stringify({
//        idCliente: idcliente
//    });
//    $.ajax({
//        type: "POST",
//        url: "CompletarPregistro.aspx/GuardarDatosPreRegis",
//        data: obj,
//        contentType: 'application/json; charset=utf-8',
//        error: function (xhr, ajaxOptions, throwError) {
//            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
//        },
//        success: function (dataS) {
//            location.href = "TiendaSN.aspx";
//        }
//    });
//}