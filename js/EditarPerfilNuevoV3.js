﻿var img;

$.ajax({
    type: "POST",
    url: "EditarPerfil.aspx/GetPais",
    data: "{}",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: false,
    success: function (result) {
        $("#cboPais").empty();
        $("#cboPais").append("<option value='0'>--Select--</option>");
        $.each(result.d, function (key, value) {
            $("#cboPais").append($("<option></option>").val(value.Codigo).html(value.Nombre));
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus + ": " + XMLHttpRequest.responseText);
    }
});

$.ajax({
    type: "POST",
    url: "EditarPerfil.aspx/ListaDatosClienteByDocumento",
    data: {},
    contentType: 'application/json; charset=utf-8',
    error: function (xhr, ajaxOptions, throwError) {
        console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
    },
    success: function (data) {
        console.log(data.d);
        Guardar(data.d);
    }
});

$("#cboPais").change(function () {
    var params = new Object();
    params.pais = $("#cboPais").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDepartamentosByPais",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDepartamento").empty();
            $("#cboDepartamento").append("<option value='0'>--Select--</option>");
            CargarProvincia();
            CargarDistrito();
            $.each(result.d, function (key, value) {
                $("#cboDepartamento").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

$("#cboDepartamento").change(function () {
    var params = new Object();
    params.departamento = $("#cboDepartamento").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetZipCodeByDepartamento",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboZipCode").empty();
            $("#cboZipCode").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboZipCode").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

$("#cboProvincia").change(function () {
    var params = new Object();
    params.provincia = $("#cboProvincia").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDistritoByProvincia",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDistrito").empty();
            $("#cboDistrito").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboDistrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

function CargarPaises() {

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetPais",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {

            $("#cboPais").empty();
            $("#cboPais").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboPais").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });

}

function CargarDepartamentos() {
    var params = new Object();
    params.pais = $("#cboPais").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDepartamentosByPais",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDepartamento").empty();
            $("#cboDepartamento").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboDepartamento").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarZipCode() {
    var params = new Object();
    params.departamento = $("#cboDepartamento").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetZipCodeByDepartamento",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboZipCode").empty();
            $("#cboZipCode").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboZipCode").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarProvincia() {
    var params = new Object();
    params.departamento = $("#cboDepartamento").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetProvinciaByDepartamento",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboProvincia").empty();
            $("#cboProvincia").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboProvincia").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarDistrito() {
    var params = new Object();
    params.provincia = $("#cboProvincia").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDistritoByProvincia",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDistrito").empty();
            $("#cboDistrito").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboDistrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function Guardar(obj) {

    var tdoc, sexo, estable, imagen, x, establePremio;
    x = obj[0].imagen;

    if (obj[0].tipoDoc == "SSN (Social Security Number)") {
        tdoc = "1";
    } else if (obj[0].tipoDoc = "PASSPORT") {
        tdoc = "2";
    }
    else if (obj[0].tipoDoc = "ITIN") {
        tdoc = "3";
    }
    if (obj[0].sexo == "MALE") {
        sexo = "1";
    } else if (obj[0].sexo == "FEMALE") {
        sexo = "2";
    }
    if (obj[0].idtipoEstablecimiento == "" | obj[0].idtipoEstablecimientoPremio == null) {
        estable = "0";
    } else { estable = obj[0].idtipoEstablecimiento; }

    if (obj[0].idtipoEstablecimientoPremio == "" | obj[0].idtipoEstablecimientoPremio == null) {
        establePremio = "0";
    } else { establePremio = obj[0].idtipoEstablecimientoPremio; }

    if (x.trim() == "") {
        imagen = "";
        img = "";
    } else {
        imagen = obj[0].imagen.trim();
        img = obj[0].imagen.trim();
    }


    $("#txtUs").val(obj[0].usuario);
    
    $("#txtCla").val(obj[0].clave);
    $("#txtNombre").val(obj[0].nombre);
    $("#txtApePat").val(obj[0].apellidoPat);
    $("#txtApeMat").val(obj[0].apellidoMat);
    $("#txtDocumento").val(obj[0].numeroDoc);
    $("#datepicker").val(obj[0].fechaNac);
    $("#txtDireccion").val(obj[0].direccion);
    $("#txtReferencia").val(obj[0].referencia);
    $("#txtTelefono").val(obj[0].telefono);
    $("#txtCelular").val(obj[0].celular);
    $("#txtCorreo").val(obj[0].correo);
    $("#txtRuc").val(obj[0].ruc);
    $("#txtCuenta").val(obj[0].nroCtaInterbancaria);
    $("#txtBanco").val(obj[0].nombreBanco);
    $("#txtDeposito").val(obj[0].nroCtaDeposito);
    $("#txtDetracciones").val(obj[0].nroCtaDetraccion);
    $("#ComboSexo").val(sexo);
    $("#cbo_idioma").val(obj[0].idioma);
    $("#cboTipoDoc").val(tdoc);
    $("#cboPais").change(function () {
        CargarDepartamentos();
    });
    CargarPaises();
    $("#cboPais").val(obj[0].idpais);
    CargarDepartamentos();
    $("#cboDepartamento").val(obj[0].iddepartamento);
    CargarZipCode();
    $("#cboZipCode").val(obj[0].CodigoPostal);
    //CargarProvincia();
    //$("#cboProvincia").val(obj[0].idprovincia);
    //CargarDistrito();
    $("#cboDistrito").val(obj[0].idditrito);
    $("#cboTipoCliente").val(obj[0].idtipoCliente);
    $("#cboTipoEstablecimiento").val(estable);
    $("#ddlPremio").val(establePremio);
    $("#NumeroEditar").text(obj[0].celular);
    $("#CorreoEditar").text(obj[0].correo);
    if (imagen == "") {
        document.getElementById("FotoCliente").src = "img/Preview-icon1.png";
    } else {
        document.getElementById("FotoCliente").src = imagen;
    }
}

$("#btnSave").click(function (e) {
    e.preventDefault();
    var correo = $("#txtCorreo").val();
    var direccion = $("#txtDireccion").val();
    var pais = $("#cboPais").val();
    var celular = $("#txtCelular").val();
    var premio = $("#ddlPremio").val();
    var zipcode = $("#cboZipCode").val();
    var idioma = $("#cbo_idioma").val();

    if (correo == "") {
        FaltaCorreo();
    } else if (pais == "0") {
        FaltaPais();
    } else if (zipcode == "0" || zipcode == null) {
        FaltaZipCode();
    } else if (idioma == "0") {
        FaltaIdioma();
    } else if (celular == "") {
        FaltaCelular();
    } else if (premio == "0") {
        FaltaPremio();
    } else if (direccion == "") {
        FaltaDireccion();
    } else {
        var valida = document.getElementById('fileUpload').files.length;
        var obj = JSON.stringify({ correoS: correo });
        $.ajax({
            type: "POST",
            url: "EditarPerfil.aspx/ValidarCorreo",
            data: obj,
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOptions, throwError) {
                console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (response) {
                console.log(response);
                if (response.d == true) {
                    if (valida == 0) {
                        updateDataAjax();
                    } else {
                        DeleteFile(img);
                        UpdateFile();
                    }
                } else {
                    FaltaCorreo();
                }
            }
        });
    }
});

function UpdateFile() {
    var fileUpload = $("#fileUpload").get(0);
    var files = fileUpload.files;

    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    data.append("archivo", "socios");

    $.ajax({
        url: "FileUpload.ashx",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            var r = result;
            console.log(result.d);
            if (r == "") {
                updateDataAjax();
            } else {
                alert(result);
            }
        },
        error: function (err) {
            alert(err.statusText)
        }
    });
}

function DeleteFile(image) {

    var obj = JSON.stringify({ imagen: image });

    $.ajax({
        url: "EditarPerfil.aspx/EliminarImagen",
        type: "POST",
        data: obj,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (response) {
            console.log(response);
        }
    });
}

function updateDataAjax() {

    var tdoc, sexo, imagenCliente, zipcode, idioma;
    tdoc = $("#cboTipoDoc").val();
    sexo = $("#ComboSexo").val();
    zipcode = $("#cboZipCode").val();
    idioma = $("#cbo_idioma").val();
    
    var valida = document.getElementById('fileUpload').files.length;
    if (valida == 0) {
        imagenCliente = img;
    } else {
        imagenCliente = "uploads/" +  document.getElementById('fileUpload').files[0].name;
    }

    if (tdoc == "1") {
        tdoc = "SSN (Social Security Number)";
    } else if (tdoc == "2") {
        tdoc = "PASSPORT";
    } else if (tdoc == "2") {
        tdoc = "ITIN";
    }

    if (sexo == "1") {
        sexo = "MALE";
    } else if (sexo == "2") {
        sexo = "FEMALE";
    }

    var obj = JSON.stringify({
        usuarioUd: $("#txtUs").val(), claveUd: $("#txtCla").val(), nombresUd: $("#txtNombre").val(), apellidoPatUd: $("#txtApePat").val(), apellidoMatUd: $("#txtApeMat").val(),
        fechaNacUd: $("#datepicker").val(), sexoUd: sexo, tipoDocUd: tdoc, numeroDocUd: $("#txtDocumento").val(), direccionUd: $("#txtDireccion").val(), 
        referenciaUd: $("#txtReferencia").val(), telefonoUd: $("#txtTelefono").val(), celularUd: $("#txtCelular").val(), paisUd: $("#cboPais").val(), departamentoUd: $("#cboDepartamento").val(),
        provinciaUd: $("#cboProvincia").val(), distritoUd: $("#cboDistrito").val(), correoUd: $("#txtCorreo").val(), rucUd: $("#txtRuc").val(), interbancariaUd: $("#txtCuenta").val(),
        bancoUd: $("#txtBanco").val(), depositoUd: $("#txtDeposito").val(), detraccionUd: $("#txtDetracciones").val(), imagenUd: imagenCliente, establecimientoUd: $("#cboTipoEstablecimiento option:selected").text(),
        establecimientoUdPremio: $("#ddlPremio option:selected").text(), ZipCode: zipcode, Idioma: idioma
    });

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/ActualizarCliente",
        data: obj,
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (response) {
            console.log(response);
            Swal.fire({
                title: 'Perfect!',
                text: 'Updated profile',
                type: "success"
            }).then(function () {
               // window.location = "EditarPerfil.aspx";
            });
        }
    });
}

function FaltaCorreo() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Wrong email format: example name@domain.xxx',
        type: "error"
    });
}
function FaltaPais() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not define the Country',
        type: "error"
    });
}
function FaltaPremio() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not define the Yachay Wasi of Gifts',
        type: "error"
    });
}
function FaltaZipCode() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'You must select the Zip Code',
        type: "error"
    });
}
function FaltaDireccion() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not register the Address',
        type: "error"
    });
}
function FaltaCelular() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'Did not register the Mobile',
        type: "error"
    });
}
function FaltaIdioma() {
    Swal.fire({
        title: 'Ooops...!',
        text: 'You must choose the language',
        type: "error"
    });
}