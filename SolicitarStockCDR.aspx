﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="SolicitarStockCDR.aspx.cs" Inherits="SantaNaturaNetworkV3.SolicitarStockCDR" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" />
    <style>
        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td,
        .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            vertical-align: middle;
        }

        .ui-front {
            z-index: 2000 !important;
        }

        .modal-body {
            overflow: hidden;
        }

            .modal-body:hover {
                overflow-y: auto;
            }
    </style>
    <section class="content-header">
        <h1 style="text-align: center">Generar Pedido</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box box-header">
                        <h3 class="box-title">Lista de Pedidos</h3>
                    </div>
                    <div style="padding-left: 11px; margin-bottom: 40px; display: flex; justify-content: center" class="col-md-12">
                        <div class="col-md-4">
                            <p style="margin-top: auto; margin-bottom: auto; margin-right: 10px; margin-left:10px; font-family: 'Dancing Script', cursive; font-size: 25px;">
                                Saldo Comisiones:
                        <asp:Label runat="server" ID="txtSaldoComision">0</asp:Label>
                            </p>
                        </div>
                        <div class="col-md-4">
                            <p style="margin-top: auto; margin-bottom: auto; margin-right: 20px; font-family: 'Dancing Script', cursive; font-size: 25px;">
                                Linea de Credito:
                        <asp:Label runat="server" ID="txtLineaCredito">0</asp:Label>
                            </p>
                        </div>
                    </div>
                    <div style="padding-left: 11px; margin-bottom: 30px" class="col-md-12">
                        <div class="col-md-2" style="margin-top: 15px; margin-bottom: 15px;">
                            <button runat="server" type="button" class="btn btn-success" style="font-weight: bold" id="btnModals">Crear Pedido</button>
                        </div>
                        <div class="col-md-2">
                            <label>Tipo Pago</label>
                            <asp:DropDownList runat="server" ID="cboTipoPago" CssClass="form-control" />
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="container modal-dialog" id="modalTamano2" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 style="text-align: center; font-weight: bold" class="modal-title" id="exampleModalLabel2">Registrar Stock</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="modal-body1">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Despacho</label>
                                                <asp:TextBox runat="server" ID="txtCDR" CssClass="form-control" BackColor="LightGreen" ReadOnly="true" />
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="box-body table-responsive">
                                                <table id="tbl_registro" class="table table-bordered table-hover text-center">
                                                    <thead>
                                                        <tr>
                                                            <th>Orden</th>
                                                            <th>Producto</th>
                                                            <th>Imagen</th>
                                                            <th>ID PS</th>
                                                            <th>Pack</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbe2">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="btnCancelar2" type="reset" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                    <button id="btnRegistrar" type="button" class="btn btn-success">Registrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="container modal-dialog" id="modalTamano" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 style="text-align: center; font-weight: bold" class="modal-title" id="exampleModalLabel">Detalle de Solicitud de Stock</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="modal-body1">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>CDR</label>
                                                <asp:TextBox runat="server" ID="txtCDRActualizar" CssClass="form-control" BackColor="LightGreen" ReadOnly="true" />
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="box-body table-responsive">
                                                <table id="tbl_detalle" class="table table-bordered table-hover text-center">
                                                    <thead>
                                                        <tr>
                                                            <th>Orden</th>
                                                            <th>Producto</th>
                                                            <th>Imagen</th>
                                                            <th>ID PS</th>
                                                            <th>Unidades</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbActualiza">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="btnCancelar" type="reset" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="box-body table-responsive">
                        <table id="tbl_cdr" class="table table-bordered table-hover text-center">
                            <thead>
                                <tr>
                                    <th>Fila</th>
                                    <th>Fecha Solicitud</th>
                                    <th>Id CDR</th>
                                    <th>Ver Detalle</th>
                                </tr>
                            </thead>
                            <tbody id="tbl_body_table">
                                <!--Data por medio de Ajax-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/sweetAlert.js" type="text/javascript"> </script>
        <script src="js/plugins/datatables/jquery.dataTables.js"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="js/SolicitarProductosCDRV2.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
            });
        </script>

    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
