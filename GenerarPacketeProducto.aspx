﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="GenerarPacketeProducto.aspx.cs" Inherits="SantaNaturaNetwork.GenerarPacketeProducto" ClientIDMode="Static" %>

<%@ OutputCache Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />
    <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <section class="content-header">
        <h1 style="text-align: center">Gestion de Paquetes</h1>
    </section>
    <style>
        #modalTamano {
            width: 62% !important;
        }

        .ui-front {
            z-index: 2000 !important;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td,
        .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            vertical-align: middle;
        }

        .switchTamano {
            width: 25rem !important;
            top: -1.1rem;
        }

        .modal-body {
            height: 470px;
            overflow: hidden;
        }

            .modal-body:hover {
                overflow-y: auto;
            }

        
    </style>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box box-header">
                        <h3 class="box-title">Lista de Paquetes</h3>
                    </div>
                    <div style="padding-bottom: 13px; padding-left: 11px">
                        <button type="button" class="btn btn-success" style="font-weight: bold" data-toggle="modal" data-target="#exampleModal" id="btnNuevoPaquete">
                            Nuevo Paquete
                        </button>
                    </div>
                    <div class="box-body table-responsive">
                        <div class="col-md-4">
                            <div class="box-body">
                                <div class="form-group">
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="container modal-dialog" id="modalTamano" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 style="text-align: center; font-weight: bold" class="modal-title" id="exampleModalLabel">Nuevo Paquete</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="modal-body1">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label>Nombre de Paquete</label>
                                                                <asp:TextBox ID="txtNombrePaquete" BackColor="LightGreen" placeholder="Nombre" CssClass="form-control" runat="server" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>Observacion</label>
                                                                <asp:TextBox runat="server" ID="txtObservacion" TextMode="MultiLine" Rows="2" CssClass="form-control" BackColor="LightGreen"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="box-body table-responsive">
                                                                <table id="tblRegistroProducto" class="table table-bordered table-hover text-center">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="width: 30%">Pais</th>
                                                                            <th style="width: 30%">Dato1</th>
                                                                            <th style="width: 30%">Estado y Cantidad</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="tbl_body_table2">
                                                                        <tr>
                                                                            <th>Perú</th>
                                                                            <th>Datos del Paquete para Perú
                                                                            </th>
                                                                            <th>
                                                                                <div class="checkbox switchTamano" id="switchTamano1">
                                                                                    <input id="toogleEEUU" type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger">
                                                                                </div>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-1</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU1"  BackColor="LightGreen" placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU1" MaxLength="6" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-2</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU2" BackColor="LightGreen"  placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU2" MaxLength="6" placeholder="Precio" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-3</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU3" BackColor="LightGreen"  placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU3" MaxLength="6" placeholder="Precio" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-4</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU4" BackColor="LightGreen"  placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU4" MaxLength="6" placeholder="Precio" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-5</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU5" BackColor="LightGreen"  placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU5" MaxLength="6" placeholder="Precio" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-6</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU6" BackColor="LightGreen"  placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU6" MaxLength="6" placeholder="Precio" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-7</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU7" BackColor="LightGreen"  placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU7" MaxLength="6" placeholder="Precio" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-8</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU8" BackColor="LightGreen"  placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU8" MaxLength="6" placeholder="Precio" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-9</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU9" BackColor="LightGreen"  placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU9" MaxLength="6" placeholder="Precio" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>P-10</th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtIDEEUU10" BackColor="LightGreen"  placeholder="IdPerushop" class="form-control" runat="server" />
                                                                            </th>
                                                                            <th>
                                                                                <asp:TextBox runat="server" ID="txtPrecioEEUU10" MaxLength="6" placeholder="Precio" CssClass="form-control solo-numero" BackColor="LightGreen"></asp:TextBox>
                                                                            </th>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button id="btnId" type="reset" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                                    <button id="btnRegistrar" type="button" class="btn btn-success">Aceptar</button>
                                                    <button id="btnActualizar" type="button" class="btn btn-success">Actualizar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-body">
                                <div class="form-group"></div>
                            </div>
                        </div>
                        <table id="tbl_paquetes" class="table table-bordered table-hover text-center">
                            <thead>
                                <tr>
                                    <th>ID Paquete</th>
                                    <th>Nombre</th>
                                    <th>Estado</th>
                                    <th>Actualizar</th>
                                </tr>
                            </thead>
                            <tbody id="tbl_body_table">
                                <!--Data por medio de Ajax-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <script src="js/sweetAlert.js" type="text/javascript"> </script>
        <script src="js/plugins/datatables/jquery.dataTables.js"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="js/Paquete.js?v3" type="text/javascript"></script>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
