﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SantaNaturaNetwork.Login" %>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Inicio de Sesión</title>

    <link rel="icon" href="https://tienda.mundosantanatura.com/img/Natural_Food_icon.png" type="image/x-icon" />
    <!-- Dışarıdan Çağırılan Dosyalar Font we Materyal İkonlar -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <!-- #Dışarıdan Çağırılan Dosyalar Font we Materyal İkonlar Bitiş -->
    <link href="css/estilos-login5.css" rel="stylesheet" type='text/css' />
    <%--<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />--%>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <link href="css/bootstrap-sweetalert-master/sweetalert.css" rel="stylesheet" />
    <script src="js/bootstrap-sweetalert-master/sweetalert.min.js" type="text/javascript"></script>    

    <link rel="stylesheet" href="assets/Estilos/alertify.core.css" />
    <link rel="stylesheet" href="assets/Estilos/alertify.default.css" id="toggleCSS" />
    <script src="assets/Estilos/alertify.min.js" type="text/javascript"></script>

     <style>
    .form-controlEPP2{
        font-size: 0.8rem;
        line-height: 1.5;
        display: block;
        width: 100%;
        height: calc(2.75rem + 2px);
        padding: .625rem .75rem;
        transition: all .2s cubic-bezier(.68, -.55, .265, 1.55);
        color: #8898aa;
        border: 1px solid #cad1d7;
        border-radius: .375rem;
        background-color: #fff;
        background-clip: padding-box;
        box-shadow: none;
    }
      </style>
      <style>
          .alertify {
            background: #FFF;
            border: 10px solid #f8f9fa;
           border: 10px solid #f8f9fa;
        }
        .alertify-button-ok, .alertify-button-ok:hover, .alertify-button-ok:focus {
            background-color: #1B1464;
            border: 1px solid #3B7808;
        }
        .btn-success {
            color: #fff;
            background-color: #1B1464;
            border-color: #4cae4c;
        }
    </style>
    <style>
        .passwordLogin {
            width: 88%
        }

        #password_visibility {
            width: 12%
        }

        .claseFuente1 {
            font-size: 60px;
            font-family: 'HurmeGeometricSans1 BlackOblique';
            color: #1B1464;
        }

        .claseFuente2 {
            font-size: 30px;
            font-family: 'HurmeGeometricSans1';
            color: #1B1464;
            font-weight: bold;
        }

        .claseFuente3 {
            font-size: 15px;
            font-family: 'HurmeGeometricSans1 Oblique';
            color: #1B1464 !important;
            font-weight: bold !important;
        }

        .claseFuente4 {
            font-size: 10px;
            font-family: 'HurmeGeometricSans1 Oblique';
            color: #1B1464;
            font-weight: bold;
            margin-left: -95px;
        }

        .claseFuente5 {
            font-size: 20px !important;
            font-family: 'HurmeGeometricSans1 Black';
            color: #1B1464 !important;
            font-weight: bold;
        }
    </style>
    <link href="css/proyecto2/fonts-v2.css" rel="stylesheet" type="text/css" />
</head>
<body>
     <input id="hdnContador" name="hdnContador" type="hidden">
    <input id="hdn_clave_actual" name="hdn_clave_actual" type="hidden">
     <input type="hidden" class="form-control" id="hdnRespuesta" /> 

    <div id="fondoFormularioLogin">
        <div class=" login-card">
            <form id="form1" runat="server" class="col-lg-12" style="margin-bottom: auto; margin-top: auto;">
                <asp:Login ID="LoginUser" runat="server" EnableViewState="false" OnAuthenticate="LoginUser_Authenticate" Width="100%">
                    <LayoutTemplate>
                        <!-- Logo -->
                        <div class="col-lg-12 logo-kapsul">
                            <img id="imgLogoParaLogin" width="250" class="logo" src="https://tienda.mundosantanatura.com/img/LOGO-PARA-LOGIN-NEW.png" alt="Logo" style="padding-top: -40px; padding-bottom: -40px" />
                            <br />
                            <%--<img width="110" src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png" style="top: 20px; padding-bottom: 50px; padding-top: -30px" />--%>
                            <%--<img id="bienvenidosLogin" width="250" src="img/bienvenidos-login.png" style="top: 20px; padding-bottom: 50px" />--%>
                            <div>
                                <p class="claseFuente1">¡Welcome!</p>
                            </div>
                            <br />
                            <h4 class="claseFuente2" id="ingreseDatos">Enter your details to continue</h4>
                            <br />
                            <div id="bloqueNoEresEmpresario" style="display: none; justify-content: space-evenly; align-items: center;">
                                <div>
                                    <h6 class="claseFuente3" id="noEresEmpresario">¿NO ERES EMPRESARIO SNN? &nbsp;&nbsp;</h6>
                                </div>
                                <div>
                                    <a class="claseFuente4" id="ClickAqui" href="#">HAGA CLICK AQUI</a>
                                </div>
                            </div>
                        </div>
                        <!-- #Logo Bitiş -->

                        <div style="clear: both"></div>

                        <!-- Kullanıcı Adı Giriş İnput -->
                        <div class="group" id="campoUsuario" style="margin-top: 30px">
                            <%--<input type="text" required="required" runat="server" name="usuario" id="usuario"/>--%>
                            <div style="width: 99px; margin-right: auto">
                                <label style="position: inherit"><%--<i id="iconoLabelUsuario" class="material-icons input-ikon">person_outline</i>--%><span id="labelUsuario" class="span-input claseFuente5">User</span></label>
                            </div>

                            <asp:TextBox ID="UserName" runat="server" placeholder="Enter User..." MaxLength="8"></asp:TextBox>
                        </div>
                        <!-- #Kullanıcı Adı Giriş İnput Bitiş -->

                        <!-- Şifre İnput Giriş-->
                        <div class="group" id="campoClave">
                            <%--<input type="password" required="required" runat="server" name="clave" id="clave"/>--%>
                            <div style="width: 99px; margin-right: auto">
                                <label style="position: inherit"><%--<i id="iconoLabelClave" class="material-icons input-sifre-ikon">lock</i>--%><span id="labelClave" class="span-input text-center claseFuente5">Password</span></label>
                            </div>
                            <div class="pass_show" style="display: flex; align-items: center">
                                <asp:TextBox CssClass="passwordLogin" TextMode="Password" ID="Password" runat="server" placeholder="Enter Password..." MaxLength="12"></asp:TextBox>
                            </div>
                        </div>
                        <!-- Şifre İnput Giriş Bitiş-->

                        <!-- Giriş Yap Buton -->
                        <%--<a href="Index.aspx" class="giris-yap-buton">Iniciar Sesión</a>--%>
                        <div id="iniciarSesion" class="group">
                            <asp:Button ID="Button1" CommandName="Login" runat="server" Text="Login" CssClass="giris-yap-buton claseFuente5" />
                        </div>
                        <!-- #Giriş Yap Buton Bitiş -->

                        <!-- Şifremi Unuttum ve Kaydol Linkleri -->
                        <div class="forgot-and-create tab-menu" style="display:block;">
                            <a id="signoInterrogacion1" class="claseFuente3" style="font-size: 15px; margin-right: -1px"></a><a class="sifre-hatirlat-link claseFuente3" href="javascript:void('sifre-hatirlat-link');">Did you forget your password</a><a id="signoInterrogacion2" class="claseFuente3" style="font-size: 15px; margin-left: -10px">?</a>
                            <%--<a class="hesap-olustur-link" href="javascript:void('hesap-olustur-link');" style="font-size: 15px; float: right; color: aquamarine">Crear cuenta</a>--%>
                        </div>
                        <!-- Şifremi Unuttum ve Kaydol Linkleri Bitiş -->
                    </LayoutTemplate>
                </asp:Login>
            </form>

            <!-- Sifre Hatirlat Form Sayfası -->
          <%--  <form id="sifre-hatirlat-form" class="col-lg-12">

                <div class="col-lg-12 logo-kapsul">
                    <img width="110" class="logo" src="https://tienda.mundosantanatura.com/img/contraseña.png" alt="Logo" />
                </div>

                <div style="clear: both"></div>

                <!-- Şifre Hatırlat Email İnput -->
                <div class="group">
                    <input type="text" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-ikon">mail_outline</i><span class="span-input">E-Mail</span></label>
                </div>
                <!-- #Şifre Hatırlat Email İnput Bitiş -->

                <!-- Şifremi Hatırlat Buton -->

                <div id="btnEnviarCorreoLogin" class="group">
                    <a href="javascript:void(0);" class="giris-yap-buton claseFuente5" style="text-decoration: none !important">Enviar</a>
                </div>
                <!-- #Şifremi Hatırlat Buton Bitiş -->

                <!-- Zaten Hesap Var Link -->
                <a class="zaten-hesap-var-link claseFuente3" href="javascript:void('zaten-hesap-var-link');">¿Ya tienes una cuenta? Inicia sesión.</a>
                <!-- #Zaten Hesap Var Link Bitiş -->

            </form>--%>

            <!-- Sifre Hatirlat Form Sayfası -->
            <form id="sifre-hatirlat-form" class="col-lg-12" style="margin-bottom: auto; margin-top: auto;">

                <div class="col-lg-12 logo-kapsul">
                            <img id="imgLogoParaLogin" width="250" class="logo" src="https://tienda.mundosantanatura.com/img/LOGO-PARA-LOGIN-NEW.png" alt="Logo" style="padding-top: -40px; padding-bottom: -40px" />
                            <br />
                            <%--<img width="110" src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png" style="top: 20px; padding-bottom: 50px; padding-top: -30px" />--%>
                            <%--<img id="bienvenidosLogin" width="250" src="img/bienvenidos-login.png" style="top: 20px; padding-bottom: 50px" />--%>
                            <div>
                                <p class="claseFuente1">Recover my Account</p>
                            </div>
                            <br />
                            <h4 class="claseFuente2" id="ingreseDatos">Enter your details to continue</h4>
                            <br />
                           
                        </div>

             
                <div class="col-lg-12 logo-kapsul">
                    <img width="110" class="logo" src="https://tienda.mundosantanatura.com/img/contraseña.png" alt="Logo" />
                </div>

                <div style="clear: both"></div>

                <!-- Şifre Hatırlat Email İnput -->
                <div class="group">
                    <input type="text" required="required" id="id_usuario" style="padding: 10px 10px 10px 10px;" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label style="top:-27px"><i class="material-icons input-ikon">person</i><span class="span-input" style="color:#1B1464; font-weight:bold;">User</span></label>
               
                </div>
               
                 
                <!-- #Şifre Hatırlat Email İnput Bitiş -->

                <!-- Şifremi Hatırlat Buton -->

                <div id="btnEnviarCorreoLogin" class="group">
                    <a class="giris-yap-buton claseFuente5" style="text-decoration: none !important;cursor:pointer" onclick="open_restablecer_clave();">Check</a>
                </div>
                <!-- #Şifremi Hatırlat Buton Bitiş -->

                <!-- Zaten Hesap Var Link -->
                <a class="zaten-hesap-var-link claseFuente3" href="javascript:void('zaten-hesap-var-link');">Do you already have an account? Log in.</a>
                <!-- #Zaten Hesap Var Link Bitiş -->

            </form>

            <%--<form id="kayit-form" class="col-lg-12">
                <div class="col-lg-12 logo-kapsul">
                    <img width="100" class="logo" src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png" alt="Logo" />
                </div>

                <div style="clear: both;"></div>

                <!-- Kayıt Form Kullanıcı Adı İnput -->
                <div class="group">
                    <input type="text" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-ikon">person_outline</i><span class="span-input">Su nombre de usuario</span></label>
                </div>
                <!-- #Kayıt Form Kullanıcı Adı İnput Bitiş -->

                <!-- Kayıt Form Email İnput -->
                <div class="group">
                    <input type="text" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-ikon">mail_outline</i><span class="span-input">E-Mail</span></label>
                </div>
                <!-- #Kayıt Form Email İnput Bitiş -->

                <!-- Kayıt Form Şifre İnput -->
                <div class="group">
                    <input type="password" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-sifre-ikon">lock</i><span class="span-input">Contraseña</span></label>
                </div>
                <!-- #Kayıt Form Şifre İnput Bitiş -->

                <!-- Kayıt Form Şifre-Tekrarı İnput -->
                <div class="group">
                    <input type="password" required="required" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label><i class="material-icons input-sifre-ikon">lock</i><span class="span-input">Repetir contraseña</span></label>
                </div>
                <!-- #Kayıt Form Şifre-Tekrarı İnput Bitiş -->

                <!-- Kayıt Ol Buton -->
                <a href="javascript:void(0);" class="giris-yap-buton">Crear una cuenta</a>
                <!-- #Kayıt Ol Buton Bitiş -->

                <!-- Zaten Hesap Var Link -->
                <a class="zaten-hesap-var-link" href="javascript:void('zaten-hesap-var-link');">¿Ya tienes una cuenta? Inicia sesión.</a>
                <!--# Zaten Hesap Var Link Bitiş -->
            </form>--%>
            <!-- ##Kayıt Form  Sayfası Bitis -->
        </div>
         <div class="modal" id="ModalCambioPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" data-backdrop="static" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                 <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold">Change Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="form-group row">    
                                    <div class="col-lg-12">
                                         <div class="form-group focused">
                                              <label class="col-form-label form-control-label" style="font-size:.875rem">Current Password</label>
                                              <input class="form-controlEPP2 text-uppercase form-control-alternative" id="id_clave_actual" placeholder="Enter Current Password" type="password" oncopy="return false" onpaste="return false"/>
                                             <label class="col-form-label form-control-label" id="mensaje_clave_actual" style="display:none;font-size:.875rem">Correcto</label>
                                          </div>
                                    </div>
                                 </div>
                               <div class="form-group row">
                                    <div class="col-lg-12">
                                         <div class="form-group focused">
                                              <label class="col-form-label form-control-label" style="font-size:.875rem">New password</label>
                                              <input class="form-controlEPP2 text-uppercase form-control-alternative" id="id_nueva_clave" placeholder="Enter New Password " type="password" oncopy="return false" onpaste="return false"/>
                                              <label class="col-form-label form-control-label" id="mensaje_nueva_clave" style="display:none;font-size:.875rem"></label>           
                                          </div>
                                    </div>
                               </div>
                                 <div class="form-group row">
                                        <div class="col-lg-12">
                                             <div class="form-group focused">
                                                  <label class="col-form-label form-control-label" style="font-size:.875rem">Confirm Password</label>
                                                  <input class="form-controlEPP2 text-uppercase form-control-alternative" id="id_confirmar_clave" placeholder="Confirm New Password" type="password" oncopy="return false" onpaste="return false"/>
                                                    <label class="col-form-label form-control-label" id="mensaje_confirmar_clave" style="display:none;font-size:.875rem"></label>            
                                             </div>
                                        </div>
                                        
                                   </div>
                                <%-- <div class="form-group row">
                                        <div class="col-lg-12">
                                             <div class="form-group focused">
                                                <div class="alert alert-danger" role="alert">
                                                 <i class="glyphicon glyphicon-info-sign"></i>&nbsp;Alerta: Tu Clave debe contener una letra mayúscula, minúscula,numero,caracter especial y debe tener mínimo 8 dígitos. Ejemplo: Peru321.
                                                </div>
                                             </div>
                                        </div>
                                        
                                   </div>--%>
                            </div>
                        </div>
                    </div>                 
                   <%-- <div class="modal-footer ">
                        <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-lg btn-success" data-dismiss="modal" id="btn_guardar_clave" style="display:none" onclick="validar_cambio_clave();">Guardar</button>
                    </div>--%>
                     <div class="modal-footer ">
                            <div class="form-group row" style="text-align:center">
                               
                               
                                <div class="col-sm-4" style="margin: 0px 0px 5px 0px;"></div>
                                <div class="col-sm-2" style="margin: 00px 0px 5px 0px;">
                                    <button type="button" class="btn btn-danger" style="width: auto;" onclick="close_cambiar_clave();">
                                        <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>&nbsp;Cancel
                                    </button>
                                </div>

                                 <div class="col-sm-5" style="margin: 0px 0px 5px 0px;">
                                     <div class="form-group row" style="text-align:center">
                                           <div class="col-sm-10" style="margin: 0px 0px 5px 0px;">
                                               <button type="button" class="btn btn-success" id="btn_guardar_clave" style="display:none" onclick="validar_cambio_clave();">
                                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;Save&nbsp;
                                                </button>
                                           </div>
                                           <div class="col-sm-2" style="margin: 0px 0px 0px 0px;">
                                                <center><img src="Imagenes/Spin.gif" id="id_spiner_save" style="display:none" class="rounded float-center" alt="..."></center>
                                           </div>
                                      </div>
                                </div>
                                
                            </div>
                           
                            
                        </div>
                     </div>
                </div>
            </div>
        <div class="modal" id="ModalElegirValidacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title font-weight-bold">Verification code</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <div class="form-group row">    
                                        <div class="col-lg-12">
                                             <div class="form-group focused">
                                                  <label class="col-form-label form-control-label">How do you want us to send the verification code to change your password?</label>
                                             
                                              </div>
                                        </div>
                                     </div>
                                     <div class="form-group row">    
                                        <div class="col-lg-12">
                                             <div class="form-group focused">
                                                 <%-- <input type="radio" class="form-controlEPP text-uppercase form-control-alternative" style="display:none" id="id_sms" name="id_sms" value="1" disabled>
                                                  <label for="male" class="col-form-label form-control-label" style="color:black;display:none" id="lbl_sms"><i class="glyphicon glyphicon-phone"></i>&nbsp;</label><br>--%>
                                                  <input type="radio"  class="form-controlEPP text-uppercase form-control-alternative" id="id_correo" name="id_correo" value="2">
                                                  <label for="female" class="col-form-label form-control-label" style="color:black" id="lbl_correo"><i class="glyphicon glyphicon-envelope"></i>&nbsp;</label><br>
                                                  <input type="radio" class="form-controlEPP text-uppercase form-control-alternative" id="id_preguntas_seguridad" name="id_preguntas_seguridad" value="3">
                                                  <label for="other" class="col-form-label form-control-label" style="color:black" id="lbl_preguntas_seguridad"><i class="glyphicon glyphicon-list"></i>&nbsp; Answer Secret Questions</label>
                                              </div>
                                        </div>
                                     </div>
                              
                                </div>
                            </div>
                                </div>
                        </div>                 
                        <div class="modal-footer ">
                            <div class="form-group row" style="text-align:center">
                               
                               
                                <div class="col-sm-4" style="margin: 0px 0px 5px 0px;"></div>
                                <div class="col-sm-2" style="margin: 00px 0px 5px 0px;">
                                    <button type="button" class="btn btn-danger" style="width: auto;" onclick="close_correo_sms();">
                                        <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>&nbsp;Cancel
                                    </button>
                                </div>

                                 <div class="col-sm-5" style="margin: 0px 0px 5px 0px;">
                                     <div class="form-group row" style="text-align:center">
                                           <div class="col-sm-10" style="margin: 0px 0px 5px 0px;">
                                               <button type="button" class="btn btn-success" id="btn_enviar_codigo_verificacion" onclick="open_correo_sms();">
                                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;Continue&nbsp;
                                                </button>
                                           </div>
                                           <div class="col-sm-2" style="margin: 0px 0px 0px 0px;">
                                                <center><img src="Imagenes/Spin.gif" id="id_spiner" style="display:none" class="rounded float-center" alt="..."></center>
                                           </div>
                                      </div>
                                </div>
                                
                            </div>
                           
                            
                        </div>
                      </div>
                  </div>
            </div>
        <div class="modal" id="ModalSMS" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title font-weight-bold">Enter the security code</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <div class="form-group row">    
                                        <div class="col-lg-12">
                                             <div class="form-group focused">
                                                  <label class="col-form-label form-control-label" style="margin-bottom:-15px">Search your cell phone for the security code sent.</label>
                                              </div>
                                        </div>
                                     </div>
                                     <div class="form-group row">    
                                        <div class="col-lg-12">
                                             <div class="form-group focused">
                                                 <div class="col-sm-2" style="margin: 0px 0px 0px 0px;">
                                                     <input type="text" class="form-control " required="" id="txt_codigo" title="Security code" autocomplete="off" placeholder="Enter Code">
                                                 </div>
                                                <%-- <div class="col-lg-5"><span><b>Hemos enviado el código a:</b></span></div>--%>
                                                <div class="col-sm-5" style="margin: 0px 0px 0px 15px;">
                                                    <div class="row">
                                                        <span><b>We have sent the code to:</b></span>
                                                    </div>
                                                    <div class="row">
                                                        <span id="span_fono_correo">al*****@entel.pe</span>
                                                    </div>
                                                </div>
                                                 <%-- <input type="radio" class="form-controlEPP text-uppercase form-control-alternative" id="male" name="gender" value="male">
                                                  <label for="male" class="col-form-label form-control-label" style="color:black"><i class="glyphicon glyphicon-phone"></i>&nbsp; Enviar Código por SMS: ******712</label><br>
                                                  <input type="radio"  class="form-controlEPP text-uppercase form-control-alternative"id="female" name="gender" value="female">
                                                  <label for="female" class="col-form-label form-control-label" style="color:black"><i class="glyphicon glyphicon-envelope"></i>&nbsp; Enviar Código por correo: al*****@entel.pe</label><br>
                                                  <input type="radio" class="form-controlEPP text-uppercase form-control-alternative" id="other" name="gender" value="other">
                                                  <label for="other" class="col-form-label form-control-label" style="color:black"><i class="glyphicon glyphicon-list"></i>&nbsp; Responder Preguntas secretas</label>--%>
                                              </div>
                                        </div>
                                     </div>
                              
                                </div>
                            </div>
                                </div>
                        </div>                 
                        <div class="modal-footer ">
                            <div class="form-group row" style="text-align:center">
                                  <div class="col-lg-2" style="margin: 0px 0px 5px 0px;">
                                    <button type="button" class="btn btn-success" style="width: auto;" id="btn_enviar_reenviar" onclick="open_correo_sms();">
                                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;Resend
                                    </button>
                                </div>
                                <div class="col-lg-4" style="margin: 0px 0px 5px 0px;">
                                </div>
                                <div class="col-lg-2" style="margin: 0px 0px 5px 0px;">
                                    <button type="button" class="btn btn-success" style="width: auto;" id="btn_enviar_continuar" onclick="open_correo_sms();">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;Continue
                                    </button>
                                </div>
                                <div class="col-lg-1" style="margin: 0px 0px 5px 0px;">
                                </div>
                                <div class="col-lg-2" style="margin: 00px 0px 5px 0px;">
                                    <button type="button" class="btn btn-danger" style="width: auto;" onclick="Cerrar_Modal('div_Identificar_Cuenta')">
                                        <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>&nbsp;Cancel
                                    </button>
                                </div>
                            </div>
                            <%--<div class="col-sm-2" style="margin: 0px 0px 5px 0px;">
                                    <button type="button" class="btn btn-lg btn-success" style="width: auto;" data-dismiss="modal" id="btn_enviar_reenviar" onclick="open_correo_sms();">
                                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;Reenviar
                                    </button>
                                </div>--%>
                            <%--<button type="button" style="text-align:left" class="btn btn-lg btn-success" data-dismiss="modal" id="btn_enviar_reenviar" onclick="open_correo_sms();">Reenviar</button>--%>
                            <%--<button type="button" class="btn btn-lg btn-success" data-dismiss="modal" id="btn_enviar_validacion" onclick="open_correo_sms();">Continuar</button>
                            <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cancelar</button>--%>
                        
                        </div>
                         </div>
                    </div>
            </div>
        <div class="modal" id="ModalCorreo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title font-weight-bold">Enter the security code</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <div class="form-group row">    
                                        <div class="col-lg-12">
                                             <div class="form-group focused">
                                                  <label class="col-form-label form-control-label" style="margin-bottom:-15px">Search your cell phone for the security code sent.</label>
                                              </div>
                                        </div>
                                     </div>
                                     <div class="form-group row">    
                                        <div class="col-lg-12">
                                             <div class="form-group focused">
                                                 <div class="col-sm-2" style="margin: 0px 0px 0px 0px;">
                                                     <input type="text" class="form-control " required="" id="txt_codigo_correo" title="Security code" onkeypress="return valideKey(event);" autocomplete="off" placeholder="Enter Code">
                                                 </div>
                                                <%-- <div class="col-lg-5"><span><b>Hemos enviado el código a:</b></span></div>--%>
                                                <div class="col-sm-5" style="margin: 0px 0px 0px 15px;">
                                                    <div class="row">
                                                        <span><b>We have sent the code to:</b></span>
                                                    </div>
                                                    <div class="row">
                                                       <%-- <span id="span_correo">al*****@entel.pe</span>--%>
                                                        <label for="male" class="col-form-label form-control-label" style="color:black" id="span_correo">&nbsp;</label><br>
                                                    </div>
                                                </div>
                                                 <%-- <input type="radio" class="form-controlEPP text-uppercase form-control-alternative" id="male" name="gender" value="male">
                                                  <label for="male" class="col-form-label form-control-label" style="color:black"><i class="glyphicon glyphicon-phone"></i>&nbsp; Enviar Código por SMS: ******712</label><br>
                                                  <input type="radio"  class="form-controlEPP text-uppercase form-control-alternative"id="female" name="gender" value="female">
                                                  <label for="female" class="col-form-label form-control-label" style="color:black"><i class="glyphicon glyphicon-envelope"></i>&nbsp; Enviar Código por correo: al*****@entel.pe</label><br>
                                                  <input type="radio" class="form-controlEPP text-uppercase form-control-alternative" id="other" name="gender" value="other">
                                                  <label for="other" class="col-form-label form-control-label" style="color:black"><i class="glyphicon glyphicon-list"></i>&nbsp; Responder Preguntas secretas</label>--%>
                                              </div>
                                        </div>
                                     </div>
                              
                                </div>
                            </div>
                                </div>
                        </div>                 
                        <div class="modal-footer ">
                            <div class="form-group row" style="text-align:center">
                                  <div class="col-lg-4" style="margin: 0px 0px 5px 0px;">
                                    

                                      <div class="form-group row" style="text-align:center">
                                           <div class="col-sm-10" style="margin: 0px 0px 5px 0px;">
                                               <button type="button" class="btn btn-success" style="width: auto;" id="btn_enviar_reenviar_correo" onclick="open_reenvio_codigo_correo();">
                                                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;Resend
                                                </button>
                                           </div>
                                           <div class="col-sm-2" style="margin: 0px 0px 0px 0px;">
                                                <center><img src="Imagenes/Spin.gif" id="id_spiner_correo" style="display:none" class="rounded float-center" alt="..."></center>
                                           </div>
                                      </div>

                                </div>
                                <div class="col-lg-1" style="margin: 0px 0px 5px 0px;">
                                </div>

                                  <div class="col-lg-2" style="margin: 00px 0px 5px 0px;">
                                    <button type="button" class="btn btn-danger" style="width: auto;" onclick="close_elegir_codigo_seguridad();">
                                        <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>&nbsp;Cancel
                                    </button>
                                </div>
                               <%-- <div class="col-lg-1" style="margin: 0px 0px 5px 0px;">
                                </div> --%>      
                                
                               <%-- <div class="col-lg-2" style="margin: 0px 0px 5px 0px;">
                                    <button type="button" class="btn btn-success" style="width: auto;" id="btn_enviar_continuarxs" onclick="validar_codigo_seguridad();">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;Continuar
                                    </button>
                                </div>--%>
                                <div class="col-lg-4" style="margin: 0px 0px 5px 0px;">
                                      <div class="form-group row" style="text-align:center">
                                           <div class="col-sm-10" style="margin: 0px 0px 5px 0px;">
                                               <button type="button" class="btn btn-success" style="width: auto;" id="btn_validar_codigo_correo" onclick="validar_codigo_seguridad();">
                                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;Continue
                                                </button>
                                           </div>
                                           <div class="col-sm-2" style="margin: 0px 0px 0px 0px;">
                                                <center><img src="Imagenes/Spin.gif" id="id_spiner_validar_codigo" style="display:none" class="rounded float-center" alt="..."></center>
                                           </div>
                                      </div>

                                </div>


                            </div>
                            <%--<div class="col-sm-2" style="margin: 0px 0px 5px 0px;">
                                    <button type="button" class="btn btn-lg btn-success" style="width: auto;" data-dismiss="modal" id="btn_enviar_reenviar" onclick="open_correo_sms();">
                                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;Reenviar
                                    </button>
                                </div>--%>
                            <%--<button type="button" style="text-align:left" class="btn btn-lg btn-success" data-dismiss="modal" id="btn_enviar_reenviar" onclick="open_correo_sms();">Reenviar</button>--%>
                            <%--<button type="button" class="btn btn-lg btn-success" data-dismiss="modal" id="btn_enviar_validacion" onclick="open_correo_sms();">Continuar</button>
                            <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cancelar</button>--%>
                        
                        </div>
                         </div>
                    </div>
            </div>
        <div class="modal" id="ModalPreguntasSeguridad" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title font-weight-bold">Security questions</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <fieldset id="ContenidoVerificarPreguntas" class="scheduler-border" style="margin-top: 10px;">
                            <div class="row">
                                <div class="col-lg-11" style="margin: 0px 0px 5px 20px;">
                                    <span class="lblMedio-sm">Answer the following questions:</span>
                                </div>
                            </div>
                            <br>
                             <div id="idpreguntas">

                             
                            <div class="row">
                                <div class="col-lg-11" style="margin: 0px 0px 5px 10px;">
                                    <span class="lblMedio input-sm" id="span_pregunta"><b></b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11" style="margin: 0px 0px 5px 10px;">
                                    <textarea id="txt_Respuesta_1" name="txt_Respuesta" class="form-control input-sm" title="Repuesta 1" placeholder="Enter Answer" maxlength="200"></textarea>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11" style="margin: 0px 0px 0px 15px;">
                                    <div id="Leyenda4" class="alert alert-danger" style="display: none">
                                        <p><strong>¡Alerta!</strong></p>
                                        <ul style="list-style-type: circle" id="ListaDescripcionValidacion4"></ul>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        </div>                 
                        <div class="modal-footer ">
                            <div class="form-group row" style="text-align:center">
                                <div class="col-lg-2" style="margin: 0px 0px 5px 0px;">
                                   
                                </div>
                                <div class="col-lg-4" style="margin: 0px 0px 5px 0px;">
                                </div>
                                <div class="col-lg-2" style="margin: 0px 0px 5px 0px;">
                                    <button type="button" class="btn btn-success" style="width: auto;" id="btn_validar_preguntas" onclick="validar_preguntas_seguridad();">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;Continue
                                    </button>
                                </div>
                                <div class="col-lg-1" style="margin: 0px 0px 5px 0px;">
                                </div>
                                <div class="col-lg-2" style="margin: 00px 0px 5px 0px;">
                                    <button type="button" class="btn btn-danger" style="width: auto;" onclick="close_preguntas_seguridad();">
                                        <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>&nbsp;Cancel
                                    </button>
                                </div>
                            </div>
                            <%--<div class="col-sm-2" style="margin: 0px 0px 5px 0px;">
                                    <button type="button" class="btn btn-lg btn-success" style="width: auto;" data-dismiss="modal" id="btn_enviar_reenviar" onclick="open_correo_sms();">
                                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;Reenviar
                                    </button>
                                </div>--%>
                            <%--<button type="button" style="text-align:left" class="btn btn-lg btn-success" data-dismiss="modal" id="btn_enviar_reenviar" onclick="open_correo_sms();">Reenviar</button>--%>
                            <%--<button type="button" class="btn btn-lg btn-success" data-dismiss="modal" id="btn_enviar_validacion" onclick="open_correo_sms();">Continuar</button>
                            <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cancelar</button>--%>
                        
                        </div>
                      </div>
                  </div>
            </div>


    </div>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="js/estilos-login3.js?v2"></script>

    <script>

        $(document).ready(function () {

            var obtenerAnchoPagina = $("#fondoFormularioLogin").width();
            console.log("obtenerAnchoPagina: " + obtenerAnchoPagina);

            // $("#id_sms").on("change", function () {
            //     document.querySelector('#id_correo').checked = false;
            //     document.querySelector('#id_preguntas_seguridad').checked = false;
            //});
            $("#id_correo").on("change", function () {
                //  document.querySelector('#id_sms').checked = false;
                 document.querySelector('#id_preguntas_seguridad').checked = false;
            });
            $("#id_preguntas_seguridad").on("change", function () {
                 // document.querySelector('#id_sms').checked = false;
                 document.querySelector('#id_correo').checked = false;
            });
            $("#id_clave_actual").on("change", function () {
                
                ValidarClaveActual();               
                
             });
            $("#id_nueva_clave").on("change", function () {
                 ValidarNuevaClave();
             });
            $("#id_confirmar_clave").on("change", function () {
                 ValidarConfirmarClave();
            });

        });
        function ValidarClaveActual() {
            
            var clave_actual = $("#id_clave_actual").val();
            var nueva_clave = $("#id_nueva_clave").val();
            var confirmar_clave = $("#id_confirmar_clave").val();
            var element_btn_guardar_clave = document.getElementById("btn_guardar_clave");

          //  var IdCliente = document.getElementById('hf_IdCliente').value;
             var usuario = $("#id_usuario").val();
            //var usuario=IdCliente;
            var clave = $("#id_clave_actual").val();
            var tipo = "Actual";
             
                $.ajax({
                   type: 'POST',
                   url: 'Login.aspx/ValidarClaveUsuario2',
                   contentType: 'application/json; charset=utf-8',
                   dataType: 'json',
                   cache: false,
                   data: '{usuario: "' + usuario + '",clave: "' + clave + '",tipo: "' + tipo + '"}',
                   success: function (data) {
                       ndata = data.d;
                       var element_mensaje_clave_actual = document.getElementById("mensaje_clave_actual");                      
                       if (ndata == 'Clave Correcta') {
                           element_mensaje_clave_actual.style.display = "block";
                           //element_mensaje_clave_actual.style.backgroundColor = "Green";
                           element_mensaje_clave_actual.style.color = "Green";
                           document.getElementById('mensaje_clave_actual').innerHTML = "Correct Password";
                           document.getElementById("id_nueva_clave").focus();
                           // alert(ndata);

                           if (nueva_clave == "" || confirmar_clave == "" || clave_actual == "") {
                                element_btn_guardar_clave.style.display = "none";
                                return;
                            }

                           element_btn_guardar_clave.style.display = "block";
                           //$('#hdn_clave_actual').val('clave correcto');
                            hdn_clave_actual.value = "clave correcto";
                       } else {
                           element_mensaje_clave_actual.style.display = "block";
                          // element_mensaje_clave_actual.style.backgroundColor = "Red";
                            element_mensaje_clave_actual.style.color = "Red";
                           document.getElementById('mensaje_clave_actual').innerHTML = "Incorrect password.";
                           document.getElementById("id_clave_actual").focus();
                           element_btn_guardar_clave.style.display = "none";
                           //$('#hdn_clave_actual').val('clave incorrecto');
                           hdn_clave_actual.value = "clave incorrecto";
                       }
                   }
               })
        }

        function ValidarNuevaClave() {
            

             var clave_actual = $("#id_clave_actual").val();
            var nueva_clave = $("#id_nueva_clave").val();
            var confirmar_clave = $("#id_confirmar_clave").val();
            var element_btn_guardar_clave = document.getElementById("btn_guardar_clave");


             var tipo = "Nueva";
            // var IdCliente = document.getElementById('hf_IdCliente').value;
             var usuario = $("#id_usuario").val();
            var clave=$("#id_nueva_clave").val();          
            
             //Debe contener 8 o mas caracteres
            var WeakPass = /(?=.{8,}).*/;
            var validar_WeakPass = WeakPass.test(nueva_clave);

            //Debe contener letras minusculas y al menos un numero
            var MediumPass = /^(?=\S*?[a-z])(?=\S*?[0-9])\S{5,}$/;

             //Debe contener una letra Mayuscula, letras minusculas y al menos un numero
            var StrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])\S{5,}$/;
            //Debe contener una Mayuscula, letras minusculas, un numero, y un caracter especial

            var VryStrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?=\S*?[^\w\*])\S{8,}$/;
            var validar_VryStrongPass = VryStrongPass.test(nueva_clave);


            var validar_MediumPass = MediumPass.test(nueva_clave);
            var element_mensaje_nueva_clave = document.getElementById("mensaje_nueva_clave");

            if (nueva_clave == '') {
               // document.getElementById('mensaje_nueva_clave').innerHTML = "Debe elegir una clave diferente a: Peru321.";
                element_mensaje_nueva_clave.style.display = "none";
                document.getElementById("id_nueva_clave").focus();
                element_btn_guardar_clave.style.display = "none";
                //element_mensaje_nueva_clave.style.color = "Red";
                document.getElementById('id_confirmar_clave').innerHTML = "";

                return;
            }
            //if (nueva_clave == 'Peru321.') {
            //    document.getElementById('mensaje_nueva_clave').innerHTML = "Debe elegir una clave diferente a: Peru321.";
            //    element_mensaje_nueva_clave.style.display = "block";
            //    element_mensaje_nueva_clave.style.color = "Red";
            //    document.getElementById("id_nueva_clave").focus();
            //    element_btn_guardar_clave.style.display = "none";
            //    document.getElementById('id_confirmar_clave').innerHTML = "";
            //    return;
            //}
            //if (validar_WeakPass == false)
            //{
            //    document.getElementById('mensaje_nueva_clave').innerHTML = "Debe contener 8 o mas caracteres";
            //    element_mensaje_nueva_clave.style.display = "block";
            //    element_mensaje_nueva_clave.style.color = "Red";
            //    document.getElementById("id_nueva_clave").focus();
            //    element_btn_guardar_clave.style.display = "none";
            //    document.getElementById('id_confirmar_clave').innerHTML = "";
            //    return;
            //}
            //if (validar_VryStrongPass == false)
            //{
            //    document.getElementById('mensaje_nueva_clave').innerHTML = "Debe contener letra mayúscula, minúscula, número, caracter especial y debe tener mínimo 8 dígitos <br> Eje: Peru321.";
            //    element_mensaje_nueva_clave.style.display = "block";
            //    element_mensaje_nueva_clave.style.color = "Red";
            //    document.getElementById("id_nueva_clave").focus();
            //    element_btn_guardar_clave.style.display = "none";
            //    document.getElementById('id_confirmar_clave').innerHTML = "";
            //    return;
            //}
            $.ajax({
                   type: 'POST',
                   url: 'Login.aspx/ValidarClaveUsuario2',
                   contentType: 'application/json; charset=utf-8',
                   dataType: 'json',
                   cache: false,
                   data: '{usuario: "' + usuario + '",clave: "' + clave + '",tipo: "' + tipo + '"}',
                success: function (data) {
                    
                       ndata = data.d;
                       var element_mensaje_nueva_clave = document.getElementById("mensaje_nueva_clave");                      
                       if (ndata == 'Clave Correcta') {
                           
                           element_mensaje_nueva_clave.style.display = "block";
                           //element_mensaje_clave_actual.style.backgroundColor = "Green";
                           element_mensaje_nueva_clave.style.color = "Green";
                           document.getElementById('mensaje_nueva_clave').innerHTML = "Valid Password";
                           document.getElementById("id_confirmar_clave").focus();

                           if (confirmar_clave != "" && clave_actual != "") {
                               element_btn_guardar_clave.style.display = "block";
                           }
                           $("#id_confirmar_clave").val('');
                           document.getElementById('mensaje_confirmar_clave').innerHTML = "";

                           var clave_actual = hdn_clave_actual.value;//$('#hdn_clave_actual').val('');
                            if (clave_actual == "clave incorrecto") {
                                error("Clave Actual Incorrecto");   
                                element_btn_guardar_clave.style.display = "none";
                                return;
                           }
                            element_btn_guardar_clave.style.display = "none";
                           // alert(ndata);
                       } else if (ndata == 'Clave Existe') {
                            element_mensaje_nueva_clave.style.display = "block";
                          // element_mensaje_clave_actual.style.backgroundColor = "Red";
                            element_mensaje_nueva_clave.style.color = "Red";
                           document.getElementById('mensaje_nueva_clave').innerHTML = "The Clave has already been used before. It must be a new key";
                           $("#id_nueva_clave").val('');
                           $("#id_confirmar_clave").val('');
                           document.getElementById("id_nueva_clave").focus();
                           element_btn_guardar_clave.style.display = "none";
                           //document.getElementById('id_confirmar_clave').innerHTML = "";

                           $("#id_confirmar_clave").val('');
                           document.getElementById('mensaje_confirmar_clave').innerHTML = "";

                       }
                       else {
                           element_mensaje_nueva_clave.style.display = "block";
                          // element_mensaje_clave_actual.style.backgroundColor = "Red";
                            element_mensaje_nueva_clave.style.color = "Red";
                           document.getElementById('mensaje_nueva_clave').innerHTML = "Incorrect password";
                           document.getElementById("id_nueva_clave").focus();
                           element_btn_guardar_clave.style.display = "none";
                           //document.getElementById('id_confirmar_clave').innerHTML = "";

                           $("#id_confirmar_clave").val('');
                           document.getElementById('mensaje_confirmar_clave').innerHTML = "";

                       }
                       
                   }
               })
            
        }

        function ValidarConfirmarClave() {
            

            var clave_actual = $("#id_clave_actual").val();
            var nueva_clave = $("#id_nueva_clave").val();
            var confirmar_clave = $("#id_confirmar_clave").val();
            var element_mensaje_confirmar_clave = document.getElementById("mensaje_confirmar_clave");
            var element_btn_guardar_clave = document.getElementById("btn_guardar_clave");

            
            if (nueva_clave != confirmar_clave) {
                document.getElementById('mensaje_confirmar_clave').innerHTML = "Keys do not match. It must be equal to New Password.";
                element_mensaje_confirmar_clave.style.display = "block";
                element_mensaje_confirmar_clave.style.color = "Red";
                document.getElementById('id_confirmar_clave').innerHTML = "";
                document.getElementById("id_confirmar_clave").focus();
                element_btn_guardar_clave.style.display = "none";
               // document.getElementById('id_spiner_save').style.display = 'none';

                return;
            }           
             document.getElementById('mensaje_confirmar_clave').innerHTML = "Correct confirmation.";
             element_mensaje_confirmar_clave.style.display = "block";
            element_mensaje_confirmar_clave.style.color = "Green";
            element_mensaje_confirmar_clave.style.display = "block";
           // document.getElementById('id_spiner_save').style.display = 'block';


            if (nueva_clave == "" || confirmar_clave == "" || clave_actual == "") {
                element_btn_guardar_clave.style.display = "none";
              //  document.getElementById('id_spiner_save').style.display = 'none';
                return;
            }

            element_btn_guardar_clave.style.display = "block";     
           // document.getElementById('id_spiner_save').style.display = 'block';

            var clave_actual2 = hdn_clave_actual.value;//$('#hdn_clave_actual').val('');
            if (clave_actual2 == "clave incorrecto") {
                error("Clave Actual Incorrecto");     
                element_btn_guardar_clave.style.display = "none";
                return;
            }


        }

        function validar_cambio_clave() {

            var clave_actual = $("#id_clave_actual").val();
            var nueva_clave = $("#id_nueva_clave").val();
            var confirmar_clave = $("#id_confirmar_clave").val();

            document.getElementById('id_spiner_save').style.display = 'block';

            if (nueva_clave != confirmar_clave) {
                error("Clave No Coinciden");
                document.getElementById('id_spiner_save').style.display = 'none';
                return;
            }
            if (clave_actual == "" || nueva_clave == "" || confirmar_clave == "") {
                error("Clave Actual Vacio");
                document.getElementById('id_spiner_save').style.display = 'none';
                return;
            }

            var clave_actual = hdn_clave_actual.value;//$('#hdn_clave_actual').val('');
            if (clave_actual == "clave incorrecto") {
                document.getElementById('id_spiner_save').style.display = 'none';
                  error("Clave Actual Incorrecto");
                 return;
            }
           
           // var IdCliente = document.getElementById('hf_IdCliente').value;
            var usuario = $("#id_usuario").val();
           // var usuario=IdCliente;
            var clave = nueva_clave;


            $.ajax({
                   type: 'POST',
                   url: 'Login.aspx/ActualizarClaveUsuario2',
                   contentType: 'application/json; charset=utf-8',
                   dataType: 'json',
                   cache: false,
                   data: '{usuario: "' + usuario + '",clave: "' + clave + '"}',
                   success: function (data) {
                       ndata = data.d;
                       var element_mensaje_nueva_clave = document.getElementById("mensaje_nueva_clave");                      
                       if (ndata == 'Correcto') {
                           limpiar();
                           alerta("Actualizacion Correcta");
                           document.getElementById('id_spiner_save').style.display = 'none';
                       } else {
                           error("Error al Actualizar");
                           document.getElementById('id_spiner_save').style.display = 'none';
                       }
                       
                   }
               })


            
        }
        function open_restablecer_clave() {
           //alert("hola");
            var usuario = $("#id_usuario").val();
            //$('#ModalCambioPassword').modal('show');
            //return;

            $.ajax({
                type: 'POST',
                url: 'Login.aspx/ValidarClaveUsuario',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                data: '{usuario: "' + usuario + '"}',
                success: function (data) {
                    ndata = data.d;
                    // var element_mensaje_clave_actual = document.getElementById("mensaje_clave_actual");                      
                    if (ndata == 'Usuario Existe') {
                        alerta("Correcto");
                        // document.querySelector('#id_sms').checked = false;
                        document.querySelector('#id_correo').checked = false;
                        document.querySelector('#id_preguntas_seguridad').checked = false;
                        document.getElementById('id_spiner').style.display = 'none';
                        obtener_datos_cliente(usuario);
                        //$('#ModalElegirValidacion').modal('show');
                    } else {
                        alert("Usuario No Existe");
                    }
                }
            });
        }
        
        function obtener_datos_cliente(usuario) {
           //alert("hola");
           // var usuario = usuario;// $("#id_usuario").val();

            $.ajax({
                type: 'POST',
                url: 'Login.aspx/ListaDatosClienteUsuario',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                data: '{usuario: "' + usuario + '"}',
                success: function (data) {
                    var Datos = data.d;
                    var Correo;
                    var Celular;
                    $.each(Datos, function (i) {
                        
                        Correo = Datos[i].correo_encrip;
                        Celular = Datos[i].celular_encrip;

                        //document.getElementById('lbl_sms').innerHTML = "<i class='glyphicon glyphicon-phone'></i>&nbsp Enviar Código por SMS:" + "<label class='col-form-label form-control-label' style='color:#1B1464'>" + Celular + "</label>";
                        document.getElementById('lbl_correo').innerHTML = "<i class='glyphicon glyphicon-envelope'></i>&nbsp; Send Code by mail to: " + "<label class='col-form-label form-control-label' style='color:#1B1464'>" + Correo;

                        document.getElementById('span_correo').innerHTML = "<label for='male' class='col-form-label form-control-label' style='color:#1B1464' id='span_correo'>&nbsp;" + Correo + "</label><br>";

                        $('#ModalElegirValidacion').modal('show');

                         <%--$("#TxtMonto").val(MontoPagar);
                         $("#cmbTienda option[value='" + Despacho + "']").attr("selected", true);
                         $('#<%=HiddenTienda.ClientID%>').val(Despacho);
                         $('#<%=HiddenMondoPagar.ClientID%>').val(MontoPagar);--%>
                    });
                }
            });  
        }
         function open_correo_sms() {
            //var id_sms = document.querySelector('#id_sms').checked;
            var id_correo = document.querySelector('#id_correo').checked;
            var id_preguntas_seguridad = document.querySelector('#id_preguntas_seguridad').checked;

            if (id_correo == false & id_preguntas_seguridad == false) {
                error("Codigo Vacio");
            }
            else {
                //if (id_sms == true) {
                //    $('#ModalElegirValidacion').modal('hide');
                //    $('#ModalSMS').modal('show');
                //}
                if (id_correo == true) {
                    open_envio_codigo();
                }
                if (id_preguntas_seguridad == true) {
                    ListadoPreguntas();
                    $('#ModalElegirValidacion').modal('hide');
                    $('#ModalPreguntasSeguridad').modal('show');
                }
            }
            
        }
         function open_envio_codigo() {
            var usuario = $("#id_usuario").val();
            document.getElementById('id_spiner').style.display = 'block';
             $.ajax({
                 type: 'POST',
                 url: 'Login.aspx/EnviarCorreo',
                 contentType: 'application/json; charset=utf-8',
                 dataType: 'json',
                 cache: false,
                 data: '{usuario: "' + usuario + '"}',
                 success: function (data) {
                     var resultado = data.d;

                     if (resultado == "0") {
                         error("Intentos Superados");
                         document.getElementById('id_spiner').style.display = 'none';
                     }
                     else {

                         $('#txt_codigo_correo').val('');
                         $('#ModalElegirValidacion').modal('hide');
                         alerta("Enviado");
                         $('#ModalCorreo').modal('show');
                         document.getElementById('id_spiner').style.display = 'none';
                     }


                 }
             });
        }

        function open_reenvio_codigo_correo() {
            var usuario = $("#id_usuario").val();
            document.getElementById('id_spiner_correo').style.display = 'block';
            $.ajax({
                type: 'POST',
                url: 'Login.aspx/EnviarCorreo',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                data: '{usuario: "' + usuario + '"}',
                success: function (data) {
                    var resultado = data.d;
                    // alert(resultado);
                    if (resultado == "0") {
                        error("Intentos Superados");
                        document.getElementById('id_spiner_correo').style.display = 'none';
                    }
                    else {
                        alerta("Enviado");
                        document.getElementById('id_spiner_correo').style.display = 'none';
                    }


                }
            });
        }
         function ListadoPreguntas() {
            var usuario = $("#id_usuario").val();
             
             $.ajax({
                 type: 'POST',
                 url: 'Login.aspx/ListadoPreguntasUsuario',
                 contentType: 'application/json; charset=utf-8',
                 dataType: 'json',
                 async: true,
                 cache: false,
                 data: '{usuario: "' + usuario + '"}',
                 success: function (data) {
                     
                     var Datos = data.d;
                     var pregunta;
                     var respuesta;
                     var contador = 0;
                     var html;
                     
                     $.each(Datos, function (i) {
                         pregunta = Datos[i].Pregunta;
                         respuesta = Datos[i].Respuesta;
                         contador = contador + 1;
                         if (contador == 1) {
                             html = `<div class="row">
                                <div class="col-lg-11" style="margin: 0px 0px 5px 10px;">
                                    <span class="lblMedio input-sm" id="span_pregunta${contador}" style="font-weight: bold;">${pregunta}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11" style="margin: 0px 0px 5px 10px;">
                                    <textarea id="txt_Respuesta${contador}" name="txt_Respuesta${contador}" class="form-control input-sm" title="Ingresar Repuesta" placeholder="Ingresar Respuesta" maxlength="200"></textarea>
                                    <textarea id="txt_Respuesta_oculto${contador}" name="txt_Respuesta_oculto${contador}" style="display:none;" class="form-control input-sm" title="Ingresar Repuesta" placeholder="Ingresar Respuesta" maxlength="200">${respuesta}</textarea>
                                </div>
                            </div>`;
                            
                         }
                         else {
                              html = html + `<div class="row">
                                <div class="col-lg-11" style="margin: 0px 0px 5px 10px;">
                                    <span class="lblMedio input-sm" id="span_pregunta${contador}" style="font-weight: bold;">${pregunta}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11" style="margin: 0px 0px 5px 10px;">
                                    <textarea id="txt_Respuesta${contador}" name="txt_Respuesta${contador}" class="form-control input-sm" title="Ingresar Repuesta" placeholder="Ingresar Respuesta" maxlength="200"></textarea>
                                    <textarea id="txt_Respuesta_oculto${contador}" name="txt_Respuesta_oculto${contador}" style="display:none;" class="form-control input-sm" title="Ingresar Repuesta" placeholder="Ingresar Respuesta" maxlength="200">${respuesta}</textarea>
                                </div>
                            </div>`;
                         }
                     });
                     hdnContador.value = contador;
                     $("#idpreguntas").html(html);

                 },
                 error: function (xhr, status, error) {
                     var error_especifico = eval("(" + xhr.responseText + ")");
                     var error_mostrar = '';
                     if (xhr.status === 0) {
                         error_mostrar = ('Verificar conexión a internet - ' + error_especifico.Message);
                     } else if (xhr.status == 404) {
                         error_mostrar = ('URL solicitada no encontrada [Error 404] - ' + error_especifico.Message);
                     } else if (xhr.status == 500) {
                         error_mostrar = ('Error interno del servidor [Error 500] - ' + error_especifico.Message);
                     } else if (status === 'parsererror') {
                         error_mostrar = ('JSON solicitado falló - ' + error_especifico.Message);
                     } else if (status === 'timeout') {
                         error_mostrar = ('Error en tiempo de espera - ' + error_especifico.Message);
                     } else if (status === 'abort') {
                         error_mostrar = ('Solicitud Ajax abortada - ' + error_especifico.Message);
                     } else {
                         error_mostrar = ('Error desconocido: - ' + error_especifico.Message);
                     }
                 }
             });
        }
        
        function validar_preguntas_seguridad() {
            var usuario = $("#id_usuario").val();
            var contador = hdnContador.value;
            var variable_id_respuesta= 'txt_Respuesta';
            var respuesta;
            var variable_id_pregunta = 'span_pregunta';
            var pregunta;
            var result_respuesta;
            var contador_item;
            var result;

            var variable_id_respuesta_oculta = 'txt_Respuesta_oculto';
            var respuesta_oculta;

            for (let item = 1; item <= contador; item++) {
                
              // Se ejecuta 5 veces, con valores del paso 0 al 4.
                contador_item = item;
                pregunta = document.getElementById(variable_id_pregunta + item).innerHTML;//document.getElementById(variable_id_pregunta + item).value;
                respuesta = document.getElementById(variable_id_respuesta + item).value;
                respuesta_oculta = document.getElementById(variable_id_respuesta_oculta + item).value;
                
                //variable_respuesta = variable_respuesta + item;
                if (respuesta == "") {                    
                    document.getElementById(variable_id_respuesta + item).focus();
                    result_respuesta ="vacio";
                    break;
                }    
                if (respuesta != respuesta_oculta) {                    
                    document.getElementById(variable_id_respuesta + item).focus();
                    //result_respuesta ="vacio";
                    result = "No Coincide";
                    break;
                }                 
            }
            if (result_respuesta == "vacio") {
                error("Preguntas Vacias");
                return;
            }
            else {
                    if (result == "No Coincide") {
                    error("No Coincide");
                    return;
                }
                    else {
                        limpiar();
                        alerta("Respuesta Correcta");
                        $('#ModalPreguntasSeguridad').modal('hide');
                        $('#ModalCambioPassword').modal('show');
                    return;
                }
            }              
            
        }
      
      function valideKey(evt){    
        // code is the decimal ASCII representation of the pressed key.
        var code = (evt.which) ? evt.which : evt.keyCode;
    
        if(code==8) { // backspace.
          return true;
        } else if(code>=48 && code<=57) { // is a number.
          return true;
        } else{ // other keys.
          return false;
        }
    }
        function validar_codigo_seguridad() {
            var codigo_correo = $("#txt_codigo_correo").val();
            var usuario = $("#id_usuario").val();
            document.getElementById('id_spiner_validar_codigo').style.display = 'block';

            if (codigo_correo == "") {
                error("Codigo Correo Vacio");
                document.getElementById('id_spiner_validar_codigo').style.display = 'none';
                return;
            }

            $.ajax({
                type: 'POST',
                url: 'Login.aspx/ValidarCodigoCorreo',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                cache: false,
                data: '{usuario: "' + usuario + '",codigo_correo: "' + codigo_correo + '"}',
                success: function (data) {
                    var resultado = data.d;

                    if (resultado == "CODIGO INCORRECTO") {
                        error("CODIGO INCORRECTO");
                        document.getElementById('id_spiner_validar_codigo').style.display = 'none';
                    }
                    else {
                        //$('#ModalElegirValidacion').modal('hide');     
                        limpiar();
                        alerta("Codigo Correcto");
                        //$('#ModalCorreo').modal('show');
                        //document.getElementById('id_spiner').style.display = 'none';
                        $('#ModalCorreo').modal('hide');
                        $('#ModalCambioPassword').modal('show');
                        document.getElementById('id_spiner_validar_codigo').style.display = 'none';
                    }


                }
            });
            
            
        }
        function limpiar() {
            $('#id_clave_actual').val('');
            $("#id_nueva_clave").val('');
            $("#id_confirmar_clave").val('');

            document.getElementById('mensaje_clave_actual').innerHTML = "";
            document.getElementById('mensaje_nueva_clave').innerHTML = "";
             document.getElementById('mensaje_confirmar_clave').innerHTML = "";
             var element_btn_guardar_clave = document.getElementById("btn_guardar_clave");
            element_btn_guardar_clave.style.display = "none";

        }
        function close_correo_sms() {
          $('#ModalElegirValidacion').modal('hide');            
        }
        function alerta(mensaje) {           

            if (mensaje == 'Actualizacion Correcta') {
                 alertify.alert("<b>Correct Update.</b>", function () {
                    //aqui introducimos lo que haremos tras cerrar la alerta.
                    //por ejemplo -->  location.href = 'http://www.google.es/';  <-- Redireccionamos a GOOGLE.
              });
            }  

              //un alert
             if (mensaje == 'Correcto') {
                 alertify.alert("<b>Validated User. Continue.</b>", function () {
                    //aqui introducimos lo que haremos tras cerrar la alerta.
                    //por ejemplo -->  location.href = 'http://www.google.es/';  <-- Redireccionamos a GOOGLE.
              });
            }  
            if (mensaje == 'Enviado') {
                 alertify.alert("<b>The Code was sent successfully. Check your Inbox.</b>", function () {
                    //aqui introducimos lo que haremos tras cerrar la alerta.
                    //por ejemplo -->  location.href = 'http://www.google.es/';  <-- Redireccionamos a GOOGLE.
              });
            } 
            if (mensaje == 'Codigo Correcto') {
                 alertify.alert("<b>Correct Code. Continue with the Password Change.</b>", function () {
                    //aqui introducimos lo que haremos tras cerrar la alerta.
                    //por ejemplo -->  location.href = 'http://www.google.es/';  <-- Redireccionamos a GOOGLE.
              });
            } 
            if (mensaje == 'Respuesta Correcta') {
                 alertify.alert("<b>Your Answers are correct. Continue with the Password Change.</b>", function () {
                    //aqui introducimos lo que haremos tras cerrar la alerta.
                    //por ejemplo -->  location.href = 'http://www.google.es/';  <-- Redireccionamos a GOOGLE.
              });
            }

           
            
        }                   
         function confirmar(){
              //un confirm
              alertify.confirm("<p>Aquí confirmamos algo.<br><br><b>ENTER</b> y <b>ESC</b> corresponden a <b>Aceptar</b> o <b>Cancelar</b></p>", function (e) {
                    if (e) {
                          alertify.success("Has pulsado '" + alertify.labels.ok + "'");
                    } else { 
                                alertify.error("Has pulsado '" + alertify.labels.cancel + "'");
                    }
              }); 
              return false
         }
         function datos(){
                  //un prompt
                  alertify.prompt("Esto es un <b>prompt</b>, introduce un valor:", function (e, str) { 
                        if (e){
                              alertify.success("Has pulsado '" + alertify.labels.ok + "'' e introducido: " + str);
                        }else{
                              alertify.error("Has pulsado '" + alertify.labels.cancel + "'");
                        }
                  });
                  return false;
            }                   
         function notificacion(){
                    //una notificación normal
                  alertify.log("Esto es una notificación cualquiera."); 
                  return false;
         }                   
         function ok(){
                    //una notificación correcta
                  alertify.success("Visita nuestro <a href=\"https://blog.reaccionestudio.com/\" style=\"color:white;\" target=\"_blank\"><b>BLOG.</b></a>"); 
                  return false;
         }                   
        function error(mensaje) {
             if (mensaje == "Clave No Coinciden") {
                 alertify.error("The keys do not match. Validate"); 
            }  
            if (mensaje == "Clave Actual Vacio") {
                 alertify.error("All fields are required."); 
            } 
             if (mensaje == "Error") {
                 alertify.error("Update failed. Notify the System Administrator."); 
                  return false; 
            }  
             if (mensaje == "Codigo Vacio") {
                 alertify.error("You must choose an option."); 
                  return false; 
            }  
            if (mensaje == "Intentos Superados") {
                 alertify.error("Exceeded the sending limit per day. Please try within 24 hours."); 
                  return false; 
            }  
            if (mensaje == "Codigo Correo Vacio") {
                 alertify.error("You must Enter the Security Code."); 
                  return false; 
            } 
            if (mensaje == "CODIGO INCORRECTO") {
                 alertify.error("Incorrect code. Please try again."); 
                  return false; 
            } 
            if (mensaje == "Error al Actualizar") {
                 alertify.error("Update failed. Please try again."); 
                  return false; 
            }
            if (mensaje == "Preguntas Vacias") {
                 alertify.error("You must enter your answers."); 
                  return false; 
            }
            if (mensaje == "No Coincide") {
                 alertify.error("Your answers do not match what is recorded."); 
                  return false; 
            }
            if (mensaje == "Clave Actual Incorrecto") {
                 alertify.error("Your current password is wrong."); 
                  return false; 
            }
            
        }

        function close_cambiar_clave() {
            $('#ModalCambioPassword').modal('hide');
        }
        function close_preguntas_seguridad() {
            $('#ModalPreguntasSeguridad').modal('hide');
        }
        function close_elegir_codigo_seguridad() {
            $('#ModalCorreo').modal('hide');
        }
    </script>

</body>
</html>
