﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="MapaRedSocios.aspx.cs" Inherits="SantaNaturaNetworkV3.MapaRedSocios" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/proyecto2/tienda.css" rel="stylesheet" />
    <link href="css/proyecto2/vendors/elegant-icon/style.css" rel="stylesheet" />
    <link href="css/AdminLTE-v1.css" rel="stylesheet" type="text/css" />
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="css/tree-table/jquery.treegrid.css?v2" rel="stylesheet" type="text/css" />
    <style>
        .content-table {
            border-collapse: collapse;
            margin: 25px 0;
            font-size: 0.9em;
            min-width: 400px;
            border-radius: 5px 5px 0 0;
            overflow: hidden;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
        }

            .content-table thead tr {
                background-color: #009897;
                color: #ffffff;
                text-align: left;
                font-weight: bold;
            }

            .content-table th, .content-table td {
                padding: 12px 15px !important;
            }

            .content-table tbody tr {
                border-bottom: 1px solid #dddddd;
            }

                .content-table tbody tr:nth-of-type(even) {
                }

                .content-table tbody tr:last-of-type {
                    border-bottom: 2px solid #009897;
                }

                .content-table tbody tr.active-row {
                    font-weight: bold;
                    color: #009897;
                }

        .ddlMapaDeRed {
            border-radius: 4px 5px;
        }

        .lblMedioDePago {
            margin-left: auto;
        }

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(img/loadingPageSantanatura.gif) center no-repeat #fff;
        }

        .style-button-red {
            background-color: transparent !important;
            color: black !important;
            border-color: transparent !important;
        }

        div.centerTable {
            text-align: center;
        }

            div.centerTable table {
                margin: 0 auto;
                text-align: left;
                margin-bottom: 70px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="text-align: center; margin-top: 100px" id="Mp_h2_1" runat="server">Network Map</h2>
    <br />
    <div id="page_loader" style="display: none" class="se-pre-con"></div>
    <div class="row" style="margin-top: 50px; padding-bottom: 40px">
        <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-xs-12">
            <div id="Div1" runat="server" style="display: block">

                <div class="col-xs-3 col-sm-3">
                    <asp:Label ID="Label1" runat="server" Text="Period:" CssClass="lblPeriodo"></asp:Label>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <asp:DropDownList Style="height: 35px; box-shadow: 0 0 12px 2px #c2b917; transition: all 1s" ID="cboPeriodo" BackColor="White" CssClass="ddlMapaDeRed" Width="100%" ForeColor="#7d6754" Font-Names="Andalus" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="col-xs-3 col-sm-3">
                    <button class="btn btn-success botonFiltrar" type="button" style="width: 100%; transition: all 1s" id="btnFiltro" runat="server">FILTER</button>
                </div>

            </div>
        </div>
    </div>

    <div style="border: 1px solid">
        <br />
        <div class="box-body ">
            <div class="row">
                <div class="col-md-12 centerTable">
                    <div class="box box-success table-responsive">
                        <div class="box box-header">
                        </div>
                        <div style="margin-left: auto; margin-right: auto; width: 95%;">
                            <table id="tbl_red" class="tree content-table table-bordered table-hover text-center table">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;"><label style="width:100px;" id="Label2" runat="server">Level</label></th>
                                        <th style="text-align: center;" id="Mp_th1_13" runat="server">Names</th>
                                        <th style="text-align: center; display:none;" id="Mp_th1_1" runat="server">Hearts</th>
                                        <th style="text-align: center;" id="Mp_th1_2" runat="server">PP</th>
                                        <th style="text-align: center;" id="Mp_th1_3" runat="server">VIP</th>
                                        <th style="text-align: center;" id="Mp_th1_4" runat="server">VP</th>
                                        <th style="text-align: center;" id="Mp_th1_5" runat="server">VR</th>
                                        <th style="text-align: center;" id="Mp_th1_6" runat="server">VG</th>
                                        <th style="text-align: center;" id="Mp_th1_7" runat="server">VQ</th>
                                        <th style="text-align: center;" id="Mp_th1_8" runat="server">Current Rank</th>
                                        <th style="text-align: center;" id="Mp_th1_9" runat="server">Maximum Range</th>
                                        <th style="text-align: center;" id="Mp_th1_10" runat="server">Inscription</th>
                                        <th style="text-align: center;" id="Mp_th1_11" runat="server">Phone</th>
                                        <th style="text-align: center;" id="Mp_th1_12" runat="server">Country</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br />
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/tree-table/jquery.cookie.js?v2"></script>
    <script src="js/proyecto2/jqueryDataTablesPremioSocios.js"></script>
    <script src="js/proyecto2/estiloTablasPremioSocios.js"></script>
    <script src="js/tree-table/jquery.treegrid.js"></script>
    <script src="js/tree-table/jquery.treegrid.bootstrap3.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="js/sweetAlert.js" type="text/javascript"> </script>
    <script src="js/EstructuraRed.js?v9" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tree').treegrid({
                expanderExpandedClass: 'glyphicon glyphicon-minus',
                    expanderCollapsedClass: 'glyphicon glyphicon-plus',
                'saveState': true,
                'saveStateMethod': 'cookie',
                'saveStateName': 'tree-grid-state'
            })
        });
            window.onload = function () {
                document.getElementById("idMenuRed").style.color = 'white';
                document.getElementById("idMenuRed").style.borderBottom = '3px solid white';

                document.getElementById("idSubMenuMapaDeRed").style.color = 'white';
                document.getElementById("idSubMenuMapaDeRed").style.borderBottom = '3px solid white';
            };
    </script>
</asp:Content>
